# pylint: disable=too-many-instance-attributes
"""Tests for common functions"""

import logging
import os
import unittest

import numpy
from astropy import units as u
from astropy.coordinates import SkyCoord
from rascil.processing_components import find_pb_width_null
from ska_sdp_datamodels.image.image_create import create_image
from ska_sdp_datamodels.science_data_model.polarisation_model import PolarisationFrame

from src.common import create_mid_simulation_components, create_pb_radec

log = logging.getLogger("ska-sim-mid-logger")
log.setLevel(logging.WARNING)


class TestCommonFunctions(unittest.TestCase):
    """Test class"""

    def setUp(self):
        self.nchan = 9
        self.frequency = numpy.linspace(1.30e9, 1.46e9, self.nchan)
        self.phasecentre = SkyCoord(
            ra=+30.0 * u.deg, dec=-60.0 * u.deg, frame="icrs", equinox="J2000"
        )
        self.flux_limit = 0.001
        hwhm_deg, _, _ = find_pb_width_null(
            pbtype="MID",
            frequency=numpy.array([self.frequency[self.nchan // 2]]),
        )

        d2r = numpy.pi / 180.0
        hwhm = hwhm_deg * d2r
        self.pbradius = 1.5 * hwhm
        self.pb_npixel = 256
        fov_deg = 10.0 * 1.36e9 / self.frequency[self.nchan // 2]
        self.pb_cellsize = d2r * fov_deg / self.pb_npixel

        self.pbmodel = create_image(
            npixel=self.pb_npixel,
            cellsize=self.pb_cellsize,
            phasecentre=self.phasecentre,
            frequency=self.frequency[0],
            nchan=self.nchan,
            polarisation_frame=PolarisationFrame("stokesIQUV"),
        )

        ssmresources = os.getenv("SSMRESOURCES", None)
        if ssmresources is None:
            err_msg = (
                "test_common: environment variable SSMRESOURCES must "
                "set to directory containing simulation resources"
            )
            log.error(err_msg)
            raise ValueError(err_msg)

        self.vp_directory = ssmresources + "/test_resources/beam_models/"
        self.vp_support = 256

    def test_create_pb_radec(self):
        """create_pb_radec happy path test"""
        beam = create_pb_radec(
            self.pbmodel,
            self.vp_directory,
            frequency=self.frequency,
            telescope="MID_B2",
            stokes=True,
        )

        assert beam  # Does not return None, also not empty.
        assert beam["pixels"].data.shape == (9, 4, 256, 256)

    def test_create_pb_radec_wrong_scale(self):
        """create_pb_radec returns None when scale is wrong"""
        new_model = create_image(
            npixel=self.pb_npixel,
            cellsize=self.pb_cellsize,
            phasecentre=self.phasecentre,
            frequency=self.frequency[0],
            nchan=self.nchan // 2,
            polarisation_frame=PolarisationFrame("stokesIQUV"),
        )

        beam = create_pb_radec(
            new_model,
            self.vp_directory,
            frequency=self.frequency,
            telescope="MID_B2",
            stokes=True,
        )

        assert beam is None

    def test_create_mid_components(self):
        """Test creating components"""
        for telescope, flux_diff in [
            ("MID_B2", -0.0041755324590090825),
            ("MID", -0.004197463022691205),
            ("NONEXISTENT", 0.0),
        ]:
            pb_components = create_mid_simulation_components(
                self.phasecentre,
                self.frequency,
                self.flux_limit,
                self.pbradius,
                self.pb_npixel,
                self.pb_cellsize,
                self.vp_directory,
                pb_type=telescope,
                polarisation_frame=PolarisationFrame("stokesIQUV"),
                apply_pb=True,
            )

            original_components = create_mid_simulation_components(
                self.phasecentre,
                self.frequency,
                self.flux_limit,
                self.pbradius,
                self.pb_npixel,
                self.pb_cellsize,
                self.vp_directory,
                pb_type=telescope,
                polarisation_frame=PolarisationFrame("stokesIQUV"),
                apply_pb=False,
            )

            assert len(pb_components) == len(original_components)
            assert (
                pb_components[0].frequency.all()
                == original_components[0].frequency.all()
            )

            flux_diff_test = (
                pb_components[0].flux[self.nchan // 2][0]
                - original_components[0].flux[self.nchan // 2][0]
            )
            numpy.testing.assert_almost_equal(flux_diff, flux_diff_test, decimal=7)


if __name__ == "__main__":
    unittest.main()
