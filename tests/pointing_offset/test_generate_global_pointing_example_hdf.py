"""
Tests for functions that generate example HDF for global
pointing model fitting
"""

import os

import katpoint
import numpy
import xarray
import yaml
from astropy import units
from astropy.coordinates import SkyCoord
from ska_sdp_datamodels.calibration import import_pointingtable_from_hdf5

from src.pointing_offset.generate_global_pointing_example_hdf import (
    _azel_to_radec,
    _construct_antennas,
    _mjd_to_unix,
    _update_antenna_location,
)

T0 = 5122137600.0
HEADER = ["2021-03-11", "-30 43 4.7172", ""]
PARTIAL_PATH = (
    "/product/eb-globalpointing-20210311-00000/ska-sdp/"
    "pb-globalpointingpo-20210311-00000"
)


def test_construct_antennas():
    """
    Unit test for function used to construct katpoint Antenna
    """
    xyz = numpy.array(
        [
            [5109271.497354163, 2006808.8930278125, -3239130.7361407224],
            [5109284.8540775385, 2006824.2217235335, -3239100.126460417],
            [5109272.199343496, 2006783.5460499495, -3239145.330041681],
        ]
    )
    diameter = numpy.array([13.5, 13.5, 13.5])
    antenna_names = [
        "M001",
        "M002",
        "M003",
    ]

    ants = _construct_antennas(xyz=xyz, diameter=diameter, station=antenna_names)
    numpy.testing.assert_allclose(
        numpy.array(
            (
                list(ants[0].position_ecef),
                list(ants[1].position_ecef),
                list(ants[2].position_ecef),
            )
        ),
        xyz,
        rtol=1e-6,
    )

    assert numpy.array_equal(
        numpy.array((ants[0].diameter, ants[1].diameter, ants[2].diameter)), diameter
    )
    assert [ants[0].name, ants[1].name, ants[2].name] == antenna_names


def test_mjd_to_unix():
    """Unit test for converting Modified Julian Dates to Unix timestamps"""
    assert _mjd_to_unix(5179444764.627388) == 1672727964.627388


def test_azel_to_radec():
    """Unit test for converting Az/El to RA/Dec"""
    ant = katpoint.Antenna(
        name="M000",
        latitude=-0.5361298382018913,
        longitude=0.37426496005672905,
        altitude=1095.1998870689422,
        diameter=13.5,
        delay_model=None,
        pointing_model=None,
        beamwidth=1.22,
    )
    pointing_centre = _azel_to_radec(
        azimuth=3.9902672691187475,
        elevation=0.6394291806044812,
        time_unix=1615437600.0,
        antenna=ant,
    )

    numpy.testing.assert_almost_equal(pointing_centre.ra.deg, 192.10834647, decimal=8)
    numpy.testing.assert_almost_equal(pointing_centre.dec.deg, -49.42084027, decimal=8)


def test_update_antenna_location():
    """Unit test for function used to update antenna location"""
    x, y, z = _update_antenna_location(
        old_x=5109271.497354163,
        old_y=2006808.8930278125,
        old_z=-3239130.7361407224,
        new_lat="-30 43 4.7172",
    )
    numpy.testing.assert_almost_equal(x, 5109005.17563564, decimal=8)
    numpy.testing.assert_almost_equal(y, 2006704.28774438, decimal=8)
    numpy.testing.assert_almost_equal(z, -3239612.33557800, decimal=8)


def test_read_pointingtable(pointing_table, main_class):
    """Unit test for method read_pointingtable()"""
    xarray.testing.assert_equal(
        main_class.read_pointingtable, pointing_table, check_dim_order=True
    )


def test_read_tpoint_file(main_class, tpoint_file):
    """Unit test for method read_tpoint_file()"""
    flines = main_class.read_tpoint_file(tpoint_file)

    assert len(flines) == 9


def test_get_pointings_and_offsets(main_class, tpoint_file):
    """Unit test for method get_pointings_and_offsets()"""

    header, actual_coord, commanded_coord, pointing_offsets = (
        main_class.get_pointings_and_offsets(main_class.read_tpoint_file(tpoint_file))
    )
    assert header == HEADER

    assert actual_coord[0].az.deg, actual_coord[0].alt.deg == [
        308.66543844,
        35.30129944,
    ]
    assert actual_coord[1].az.deg, actual_coord[1].alt.deg == [
        307.74401573,
        34.59614866,
    ]
    assert actual_coord[2].az.deg, actual_coord[2].alt.deg == [
        332.69591152,
        36.51411379,
    ]

    assert commanded_coord[0].az.deg, commanded_coord[0].alt.deg == [
        308.51570409,
        34.88444160,
    ]
    assert commanded_coord[1].az.deg, commanded_coord[1].alt.deg == [
        307.59409492,
        34.17894297,
    ]
    assert commanded_coord[2].az.deg, commanded_coord[2].alt.deg == [
        332.54579978,
        36.09913661,
    ]

    numpy.testing.assert_almost_equal(
        pointing_offsets[0], numpy.array([[0.00213282, 0.00727554]]), decimal=8
    )

    numpy.testing.assert_almost_equal(
        pointing_offsets[1], numpy.array([[0.00215393, 0.00728161]]), decimal=8
    )
    numpy.testing.assert_almost_equal(
        pointing_offsets[2], numpy.array([[0.00210567, 0.00724272]]), decimal=8
    )


def test_get_starttime(main_class):
    """Unit test for method get_starttime()"""
    header = ["2021-03-11", "-30 43 4.7172"]
    assert main_class.get_starttime(header) == T0


def test_get_partial_path(main_class):
    """Unit test for method get_partial_path()"""
    assert main_class.get_partial_path(T0) == PARTIAL_PATH


def test_update_pointingtable_attrs(main_class):
    """Unit test for method update_pointingtable_attrs()"""
    result = main_class.update_pointingtable_attrs(HEADER)
    assert result.configuration.attrs["name"] == "SKA"
    assert result.attrs["scan_mode"] == "5-point"

    numpy.testing.assert_almost_equal(
        numpy.array([-2465431.31644119, -2465431.31644119, -2465431.31644119]),
        result.configuration.xyz.data[:, 0],
        decimal=8,
    )
    numpy.testing.assert_almost_equal(
        numpy.array([4903360.07830227, 4903360.07830227, 4903360.07830227]),
        result.configuration.xyz.data[:, 1],
        decimal=8,
    )

    numpy.testing.assert_almost_equal(
        numpy.array([-3239206.13739401, -3239206.13739401, -3239206.13739401]),
        result.configuration.xyz.data[:, 2],
        decimal=8,
    )


def test_update_pointingtable_with_source_list(main_class, pointing_table, tmp_path):
    """Unit test for method update_pointingtable_with_source_list()"""
    pointing_offsets_list = [
        numpy.array([[0.00213282, 0.00727554]]),
        numpy.array([[0.00215393, 0.00728161]]),
        numpy.array([[0.00210567, 0.00724272]]),
    ]

    actual_coord_list = [
        SkyCoord(
            az=308.66543844 * units.deg, alt=35.30129944 * units.deg, frame="altaz"
        ),
        SkyCoord(
            az=307.74401573 * units.deg, alt=34.59614866 * units.deg, frame="altaz"
        ),
        SkyCoord(
            az=332.69591152 * units.deg, alt=36.51411379 * units.deg, frame="altaz"
        ),
    ]

    commanded_coord_list = [
        SkyCoord(
            az=308.51570409 * units.deg, alt=34.88444160 * units.deg, frame="altaz"
        ),
        SkyCoord(
            az=307.59409492 * units.deg, alt=34.17894297 * units.deg, frame="altaz"
        ),
        SkyCoord(
            az=332.54579978 * units.deg, alt=36.09913661 * units.deg, frame="altaz"
        ),
    ]

    main_class.get_starttime(HEADER)

    # Use temp path just for this test
    main_class.hdf_filename = f"{tmp_path}/temp.hdf5"

    main_class.update_pointingtable_with_source_list(
        pointing_table,
        pointing_offsets_list,
        actual_coord_list,
        commanded_coord_list,
    )

    assert os.path.exists(
        f"{tmp_path}/{PARTIAL_PATH}/scan1-5/pointing_offsets_scans_1-5.hdf5"
    )
    assert os.path.exists(f"{tmp_path}/{PARTIAL_PATH}/scan1-5/ska-data-product.yaml")
    assert os.path.exists(
        f"{tmp_path}/{PARTIAL_PATH}/scan6-10/pointing_offsets_scans_6-10.hdf5"
    )
    assert os.path.exists(f"{tmp_path}/{PARTIAL_PATH}/scan6-10/ska-data-product.yaml")
    assert os.path.exists(
        f"{tmp_path}/{PARTIAL_PATH}/scan11-15/pointing_offsets_scans_11-15.hdf5"
    )
    assert os.path.exists(f"{tmp_path}/{PARTIAL_PATH}/scan11-15/ska-data-product.yaml")


def test_time_calculation(main_class, pointing_table, tmp_path):
    """Unit test to verify time in metadata and pointing_table are consistent"""
    pointing_offsets_list = [
        numpy.array([[0.00213282, 0.00727554]]),
    ]

    actual_coord_list = [
        SkyCoord(
            az=308.66543844 * units.deg, alt=35.30129944 * units.deg, frame="altaz"
        ),
    ]

    commanded_coord_list = [
        SkyCoord(
            az=308.51570409 * units.deg, alt=34.88444160 * units.deg, frame="altaz"
        ),
    ]

    main_class.get_starttime(HEADER)
    # Use temp path just for this test
    main_class.hdf_filename = f"{tmp_path}/temp.hdf5"

    main_class.update_pointingtable_with_source_list(
        pointing_table,
        pointing_offsets_list,
        actual_coord_list,
        commanded_coord_list,
    )

    metadata_path = f"{tmp_path}/{PARTIAL_PATH}/scan1-5/ska-data-product.yaml"
    with open(metadata_path, "r", encoding="utf8") as file:
        metadata = yaml.safe_load(file)

    output_pointing_table = import_pointingtable_from_hdf5(
        f"{tmp_path}/{PARTIAL_PATH}/scan1-5/pointing_offsets_scans_1-5.hdf5"
    )

    n_scans = int(output_pointing_table.scan_mode.split("-")[0])
    # Find central scan
    central_scan = int(n_scans / 2)
    for scan_idx, coord in enumerate(output_pointing_table.attrs["discrete_offset"]):
        if numpy.array_equal(coord, numpy.zeros(2)):
            central_scan = scan_idx + 1
            break

    track_duration = output_pointing_table.attrs["track_duration"]

    assert (
        metadata["obscore"]["t_max"] - metadata["obscore"]["t_min"]
        == n_scans * track_duration
    )
    assert (
        output_pointing_table.time.data[0]
        == metadata["obscore"]["t_min"]
        + central_scan * track_duration
        - track_duration / 2
    )


def test_create_metadata(main_class, tmp_path):
    """Test generating an initial metadata file with data"""

    partial_path = main_class.get_partial_path(T0)

    expected_metadata = {
        "config": {
            "cmdline": None,
            "commit": None,
            "image": "artefact.skao.int/ska-sdp-script-pointing-offset",
            "processing_block": "pb-globalpointingpo-20210311-00000",
            "processing_script": "pointing-offset",
            "version": "0.7.0",
        },
        "context": {},
        "interface": "http://schema.skao.int/ska-data-product-meta/0.1",
        "execution_block": "eb-globalpointing-20210311-00000",
        "obscore": {
            "access_format": "application/x-hdf5",
            "calib_level": 0,
            "dataproduct_type": "POINTING-OFFSETS",
            "facility_name": "SKA-Observatory",
            "instrument_name": "SKA-MID",
            "obs_collection": "SKA-Observatory/SKA-MID/POINTING-OFFSETS",
        },
        "files": [
            {
                "crc": None,
                "description": "Pointing offsets for scans: [1, 2, 3, 4, 5]",
                "path": f"{PARTIAL_PATH}/scan1-5/pointing_offsets_scans_1-5.hdf5",
                "status": "done",
            }
        ],
    }

    result = main_class.create_metadata(
        f"{tmp_path}/{partial_path}/scan1-5/pointing_offsets_scans_1-5.hdf5"
    )

    assert expected_metadata == result.get_data()
