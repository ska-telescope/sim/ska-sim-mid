""" Pytest fixtures """

import numpy
import pytest
from astropy import units
from astropy.coordinates import EarthLocation, SkyCoord
from ska_sdp_datamodels.calibration import PointingTable, export_pointingtable_to_hdf5
from ska_sdp_datamodels.configuration import Configuration
from ska_sdp_datamodels.configuration.config_coordinate_support import lla_to_ecef
from ska_sdp_datamodels.science_data_model.polarisation_model import ReceptorFrame

from src.pointing_offset.generate_global_pointing_example_hdf import (
    DATA_COMMENT,
    PointingOffsetHDF,
)


@pytest.fixture(name="low_aa05_config")
def low_aa05_config_fixture():
    """LOW AA0.5 configuration fixture"""
    x_coord, y_coord, z_coord = lla_to_ecef(
        numpy.array([-26.86371635, -26.86334071, -26.85615287]) * units.deg,
        numpy.array([116.6934539, 116.6936577, 116.7296391]) * units.deg,
        300.0,
    )
    ant_xyz = numpy.stack((x_coord, y_coord, z_coord), axis=1)
    return Configuration.constructor(
        name="LOW-AA0.5",
        location=EarthLocation(
            lon=116.69345390 * units.deg, lat=-26.86371635 * units.deg, height=300.0
        ),
        names=["S008‐1", "S008‐2", "S009‐1"],
        mount=numpy.repeat("XY", 3),
        xyz=ant_xyz,
        vp_type=numpy.repeat("LOW", 3),
        diameter=38.0 * numpy.ones(3),
    )


@pytest.fixture(name="pointing_table")
def pointingtable_fixture(low_aa05_config):
    """PointingTable fixture"""
    # Generate a simple PointingTable similar to what the pointing offset
    # calibration pipeline writes out
    return PointingTable.constructor(
        pointing=numpy.array([[[[[1, 1]]]]]).repeat(3, axis=1),
        time=numpy.ones(1),
        interval=numpy.ones(1),
        weight=numpy.array([[[[[1, 1]]]]]).repeat(3, axis=1),
        residual=numpy.array([[[[1, 1]]]]),
        frequency=numpy.array([1350e6]),
        expected_width=numpy.array([[[[[1, 1]]]]]).repeat(3, axis=1),
        fitted_width=numpy.array([[[[[1, 1]]]]]).repeat(3, axis=1),
        fitted_width_std=numpy.array([[[[[0.01, 0.01]]]]]).repeat(3, axis=1),
        fitted_height=numpy.array([[[[1]]]]).repeat(3, axis=1),
        fitted_height_std=numpy.array([[[[0.02]]]]).repeat(3, axis=1),
        receptor_frame=ReceptorFrame("stokesI"),
        pointing_frame="xel-el",
        band_type="Band 2",
        scan_mode="5-point",
        track_duration=60.0,
        discrete_offset=numpy.array([-1.0, -0.33, 0.0, 0.33, 1.0]),
        commanded_pointing=numpy.array(
            [
                [
                    [[[3.67609153, 1.23569492]]],
                    [[[3.67609153, 1.23569492]]],
                    [[[3.67609153, 1.23569492]]],
                ]
            ]
        ),
        pointingcentre=SkyCoord(
            ra=192.10834647286165 * units.deg,
            dec=-49.42084027450528 * units.deg,
            frame="icrs",
        ),
        configuration=low_aa05_config,
    )


@pytest.fixture(name="main_class")
def main_class_fixture(pointing_table, tmp_path_factory):
    """The initialised class that handles the TPOINT and sample HDF files"""

    # Set the name of HDF5 file to be written to disc
    hdf_filename = (
        f"{tmp_path_factory.mktemp('data')}/"
        "1672727931_sdp_l0_scan_pointing_offsets.hdf5"
    )

    # Write out the HDF5 to disc
    export_pointingtable_to_hdf5(pointing_table, hdf_filename, **DATA_COMMENT)

    return PointingOffsetHDF(hdf_filename)


@pytest.fixture(name="tpoint_file")
def create_tpoint_file_fixture(tmp_path_factory):
    """Create a TPOINT file fixture"""
    # Set the name of the TPOINT file to be written to disc
    tpoint_filename = f"{tmp_path_factory.mktemp('tpoint')}/tpoint.ral.dat"
    numpy.savetxt(
        tpoint_filename,
        numpy.stack(
            (
                numpy.array([-51.33456156, 35.30129944, -51.48429591, 34.8844416]),
                numpy.array([-52.25598427, 34.59614866, -52.40590508, 34.17894297]),
                numpy.array([-27.30408848, 36.51411379, -27.45420022, 36.09913661]),
            )
        ),
        header="2021-03-11\n:NODA\n:ALLSKY\n:ALTAZ\n-30 43 4.7172\n",
        comments="",
    )

    return tpoint_filename
