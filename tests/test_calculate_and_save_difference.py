"""Tests for function to save difference data"""

import logging
from unittest.mock import patch

import numpy
import pytest
from astropy import units as u
from astropy.coordinates import SkyCoord
from rascil.workflows.rsexecute.execution_support import rsexecute
from ska_sdp_datamodels.configuration import create_named_configuration
from ska_sdp_datamodels.science_data_model.polarisation_model import PolarisationFrame
from ska_sdp_datamodels.visibility import create_visibility

from src.calculate_and_save_difference import calculate_and_save_difference, cli_parser

log = logging.getLogger("ska-sim-mid-logger")
log.setLevel(logging.WARNING)


@pytest.fixture(scope="module", name="mock_bvis")
def mock_bvis_fxt():
    """Fixture for visibility"""
    frequency = numpy.linspace(1e9, 1.5e9, 3)
    channelwidth = numpy.array([2.5e7, 2.5e7, 2.5e7])
    vis_pol = PolarisationFrame("stokesI")
    times = numpy.linspace(-300.0, 300.0, 3) * numpy.pi / 43200.0

    phasecentre = SkyCoord(
        ra=+180.0 * u.deg, dec=-60.0 * u.deg, frame="icrs", equinox="J2000"
    )

    config = create_named_configuration("MID", rmax=500.0)

    nominal_vis = create_visibility(
        config,
        times,
        frequency,
        channel_bandwidth=channelwidth,
        phasecentre=phasecentre,
        weight=1.0,
        polarisation_frame=vis_pol,
    )
    nominal_vis["vis"].data += 10.0

    actual_vis = create_visibility(
        config,
        times,
        frequency,
        channel_bandwidth=channelwidth,
        phasecentre=phasecentre,
        weight=1.0,
        polarisation_frame=vis_pol,
    )
    actual_vis["vis"].data += 10.0
    # First time and channel are 3 above nominal.
    # pylint: disable-next=unsupported-assignment-operation
    actual_vis["vis"][0, :, 0, :] += 3.0
    # Second time and channel are 5 above nominal.
    # pylint: disable-next=unsupported-assignment-operation
    actual_vis["vis"][1, :, 1, :] += 5.0

    return actual_vis, nominal_vis


@patch("src.calculate_and_save_difference.create_visibility_from_ms")
@patch("src.calculate_and_save_difference.export_visibility_to_ms")
def test_calculate_and_save_difference(mock_export_bvis, mock_create_bvis, mock_bvis):
    """
    Test that difference images are correctly calculated and saved
    """
    mock_create_bvis.side_effect = [[mock_bvis[0]], [mock_bvis[1]]]
    args = cli_parser().parse_args(
        [
            "--actual_ms",
            "actual.ms",
            "--nominal_ms",
            "nominal.ms",
            "--use_dask",
            "False",
        ]
    )

    calculate_and_save_difference(args)

    # export_bvis_to_ms was called with two arguments:
    #   output name and the bvis list to be saved.
    # bvis list (difference bvis) contains one element, which is saved in result.
    result_bvis = mock_export_bvis.call_args_list[0].args[1][0]
    result_output_name = mock_export_bvis.call_args_list[0].args[0]

    assert result_output_name == ".//difference.ms"
    assert result_bvis["vis"].shape == mock_bvis[0]["vis"].shape
    # The difference is non-zero where we updated the actual bvis (see fixture).
    assert (result_bvis["vis"].data[0, :, 0, :] == 3.0 + 0.0j).all()
    assert (result_bvis["vis"].data[1, :, 1, :] == 5.0 + 0.0j).all()
    assert (result_bvis["vis"].data[0, :, 1, :] == 0.0 + 0.0j).all()
    assert (result_bvis["vis"].data[2, :, 2, :] == 0.0 + 0.0j).all()

    rsexecute.close()
