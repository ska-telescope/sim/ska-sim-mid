# pylint: disable=too-many-instance-attributes,duplicate-code

"""Tests for converting MS"""

import glob
import logging
import os
import shutil
import unittest

import numpy
from astropy import units as u
from astropy.coordinates import SkyCoord
from ska_sdp_datamodels.configuration import create_named_configuration
from ska_sdp_datamodels.science_data_model.polarisation_model import PolarisationFrame
from ska_sdp_datamodels.visibility import create_visibility, export_visibility_to_hdf5

from src.convert_to_ms import cli_parser, do_merge, plot_vis

log = logging.getLogger("ska-sim-mid-logger")
log.setLevel(logging.WARNING)


class TestConvertMS(unittest.TestCase):
    """Tests for converting MS data"""

    def setUp(self):
        self.persist = os.environ.get("RASCIL_PERSIST", False)

        self.frequency = numpy.linspace(1e8, 1.5e8, 3)
        self.channelwidth = numpy.array([2.5e7, 2.5e7, 2.5e7])
        self.vis_pol = PolarisationFrame("stokesI")

        self.times = numpy.linspace(-300.0, 300.0, 3) * numpy.pi / 43200.0

        self.phasecentre = SkyCoord(
            ra=+180.0 * u.deg, dec=-60.0 * u.deg, frame="icrs", equinox="J2000"
        )

        self.config = create_named_configuration("MID", rmax=500.0)

        self.vis = create_visibility(
            self.config,
            self.times,
            self.frequency,
            channel_bandwidth=self.channelwidth,
            phasecentre=self.phasecentre,
            weight=1.0,
            polarisation_frame=self.vis_pol,
        )

        self.filename = "results/SKA_MID_SIM_test_0.hdf"
        parser = cli_parser()
        self.args = parser.parse_args(
            [
                "--hdffile",
                self.filename,
                "--plot",
                "True",
            ]
        )

    def test_do_merge(self):
        """
        Test merging HDF files
        """
        export_visibility_to_hdf5(self.vis, self.filename)
        do_merge(self.args)

        msname = self.filename.replace("_0.hdf", ".ms")
        visname = self.filename.replace("_0.hdf", "_vis.jpg")
        uvname = self.filename.replace("_0.hdf", "_uvcoverage.png")

        assert os.path.exists(msname)
        assert os.path.exists(visname)
        assert os.path.exists(uvname)

        if self.persist is False:
            os.remove(visname)
            os.remove(uvname)

    def test_plot_vis(self):
        """
        Test plotting visibilities
        """
        msname = self.filename.replace("_0.hdf", ".ms")
        plot_vis(msname)

        assert os.path.exists(self.filename.replace("0.hdf", "vis.jpg"))

        if self.persist is False:
            shutil.rmtree(msname)
            imglist = glob.glob("results/SKA_MID_SIM_test*")
            for f in imglist:
                os.remove(f)


if __name__ == "__main__":
    unittest.main()
