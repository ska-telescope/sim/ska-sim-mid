# pylint: disable=too-many-arguments,too-many-locals
# pylint: disable=too-many-positional-arguments

"""Regression test for RFI simulation code"""

import re
import tempfile

import numpy as np
import pytest
from rascil.apps.rascil_imager import cli_parser as imager_cli_parser
from rascil.apps.rascil_imager import imager
from rascil.apps.rascil_vis_ms import cli_parser as vis_cli_parser
from rascil.apps.rascil_vis_ms import visualise
from ska_sdp_datamodels.image.image_io_and_convert import import_image_from_fits

from rfi.mid_rfi_simulation import cli_parser, rfi_simulation


@pytest.mark.parametrize(
    "averaged, msout, apply_primary_beam, flux_max, flux_min, flux_rms, declination",
    [
        (
            "False",
            "simulate_rfi_PBTrue.ms",
            "True",
            1.149594429145324e-12,
            -8.132705185751368e-13,
            1.3629536389668253e-13,
            30.0,
        ),
        (
            "False",
            "simulate_rfi_PBFalse.ms",
            "False",
            0.00015724,
            -0.0001035,
            1.82726156e-05,
            -45.95,
        ),
    ],
)
def test_rfi_simulation(
    averaged,
    msout,
    apply_primary_beam,
    flux_max,
    flux_min,
    flux_rms,
    declination,
):
    """
    These tests use RASCIL imager to calculate the dirty images
    after a measurement set is created using the rfi_simulation code,
    from which we calculate the statistics, and check those.

    averaged: User averaged bvis from RFI simulation
    apply_primary_beam: Apply the primary beam to rfi
    flux_max, flux_min, flux_rms: test values in output dirty image
    declination: declination for phase centre

    Note: the declination for the case when primary beam is applied is for
    a source close to the horizon at the SKA Mid site. This is necessary
    because we do not have information about the far sidelobes of the beam
    and hence we need to point towards potential sources to get some signal.
    The aircraft data contains sources up to ~20 deg altitude,
    but more closer to the horizon.

    The declination value for no primary beam is for a source closer to zenith.
    """
    file_path = __file__
    # The test data set is a truncated version of
    # aeronautical_sim_datacube_20190313.h5
    # containing two sources, five stations,
    # ten time samples, and one frequency channel.
    input_file = re.sub(
        "test_mid_rfi_simulation_regression.py",
        "test_data/aeronautical_sim_datacube_20190313_truncated.h5",
        file_path,
    )
    nchan = 4

    with tempfile.TemporaryDirectory() as tempdirname:
        parser = cli_parser()
        args = parser.parse_args(
            [
                "--input_file",
                input_file,
                "--ra",
                "0.05",
                "--dec",
                f"{declination}",
                "--msout",
                msout,
                "--output_dir",
                tempdirname,
                "--nchannels",
                f"{nchan}",
                "--return_average",
                averaged,
                "--channel_average",
                "1",
                "--frequency_range",
                "1.0e9",
                "1.2e9",
                "--apply_primary_beam",
                apply_primary_beam,
            ]
        )

        rfi_simulation(args)

        parser = vis_cli_parser()
        args = parser.parse_args(["--ingest_msname", f"{tempdirname}/{msout}"])
        visualise(args)

        parser = imager_cli_parser()
        args = parser.parse_args(
            [
                "--mode",
                "invert",
                "--use_dask",
                "False",
                "--ingest_msname",
                f"{tempdirname}/{msout}",
                "--ingest_vis_nchan",
                f"{nchan}",
                "--ingest_dd",
                "0",
                "--ingest_chan_per_vis",
                f"{nchan}",
                "--imaging_npixel",
                "1024",
                "--imaging_cellsize",
                "0.0022",
                "--imaging_nchan",
                f"{nchan}",
            ]
        )

        # Make an all sky image and delete any invalid pixels due to the imaging.
        dirty_name = imager(args)
        dirty = import_image_from_fits(dirty_name)
        np.nan_to_num(dirty["pixels"].data, copy=False, nan=0.0, posinf=0.0, neginf=0.0)
        qa = dirty.image_acc.qa_image()

        assert dirty["pixels"].shape == (
            nchan,
            1,
            args.imaging_npixel,
            args.imaging_npixel,
        )

        np.testing.assert_allclose(
            qa.data["max"],
            flux_max,
            atol=1e-7,
            err_msg=str(qa),
        )
        np.testing.assert_allclose(
            qa.data["min"],
            flux_min,
            atol=1e-7,
            err_msg=str(qa),
        )
        np.testing.assert_allclose(
            qa.data["rms"],
            flux_rms,
            atol=1e-7,
            err_msg=str(qa),
        )
