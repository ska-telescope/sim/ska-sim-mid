# pylint: disable=too-many-arguments,too-many-locals
# pylint: disable=inconsistent-return-statements
# pylint: disable=too-many-positional-arguments

"""
Regression test for mid_simulation.py
"""
import glob
import logging
import os
import shutil
import tempfile

import numpy
import pytest
from ska_sdp_datamodels.image.image_io_and_convert import import_image_from_fits

from src.mid_simulation import cli_parser as simulation_cli_parser
from src.mid_simulation import simulation

log = logging.getLogger("ska-sim-mid-logger")
log.setLevel(logging.WARNING)
rascillog = logging.getLogger("rascil-logger")
rascillog.setLevel(logging.WARNING)

DEFAULT_RUN = True
USE_DASK = True


@pytest.mark.parametrize(
    "enabled, mode, flux_limit, npixel, cellsize, rmax, "
    "flux_max, flux_min, source, only_actual, iso",
    [
        (
            DEFAULT_RUN,
            "wind_pointing",
            0.3,
            256,
            1e-4,
            5e2,
            0.0043734948255960285,
            -0.039700451391196614,
            "s3sky",
            "False",
            "False",
        ),
        (
            DEFAULT_RUN,
            "random_pointing",
            0.3,
            256,
            1e-4,
            5e2,
            0.004373824982199548,
            -0.03970004744504036,
            "s3sky",
            "False",
            "False",
        ),
        (
            DEFAULT_RUN,
            "ionosphere",
            0.03,
            256,
            1e-4,
            5e2,
            0.1151758675831728,
            -0.011030493982954576,
            "s3sky",
            "False",
            "False",
        ),
        (
            DEFAULT_RUN,
            "ionosphere",
            0.03,
            256,
            1e-4,
            5e2,
            0.11519143119768473,
            -0.011037544386892483,
            "s3sky",
            "False",
            "True",
        ),
        (
            DEFAULT_RUN,
            "polarisation",
            0.3,
            256,
            1e-4,
            5e2,
            0.04314280010590244,
            -0.004633469610291666,
            "s3sky",
            "False",
            "False",
        ),
        (
            DEFAULT_RUN,
            "heterogeneous",
            0.3,
            256,
            1e-4,
            5e2,
            0.04314280010590244,
            -0.004633469610291666,
            "s3sky",
            "False",
            "False",
        ),
        (
            DEFAULT_RUN,
            "polarisation",
            0.3,
            256,
            1e-4,
            5e2,
            1.0062191223043198,
            -0.10592820958026977,
            "point",
            "True",
            "False",
        ),
    ],
)
def test_mid_simulation(
    enabled,
    mode,
    flux_limit,
    npixel,
    cellsize,
    rmax,
    flux_max,
    flux_min,
    source,
    only_actual,
    iso,
):
    """Test selected mid_simulations

    This requires downloading of some files from the GCP ska-simulation site
    using the shell script get_test_gcp.sh. The ionosphere tests is disabled
    since the required screen file is 3GB.m You must also set the environment
    variable SSMRESOURCES to the root of the directory where the data is
    downloaded. Optionally you can also set SSMRESULTS to put your data.

    :param enabled: Is this test enabled?
    :param mode: Simulation mode: nominal, wind_pointing, random_pointing, heterogeneous
    :param flux_limit: Weakest flux component to include from S3-SEX
    :param npixel: Number of pixels in test image
    :param cellsize: Cellsize in radian
    :param rmax: Maximum distance of dishes from array centre (m)
    :param flux_max: Maximum flux in residual image
    :param flux_min: Minimum flux in residual image
    :param source: Type of source s3sky or point
    :param only_actual: whether to calculate the actual data only (True),
            or nominal and difference as well
    :param iso: Should the screen be forced to be isoplanatic?
            Default is that the screen is non-isoplanatic
    :return:
    """

    if not enabled:
        log.warning(
            "test_mid_simulation: test of %s mode is disabled, "
            "use enabled argument to change",
            mode,
        )
        return True

    # Set true if we want to save the outputs.
    persist = os.environ.get("PERSIST", False)

    ssmresults = os.environ.get("SSMRESULTS", None)
    if ssmresults is None:
        ssmresults = os.getcwd()
    result_dir = ssmresults + "/results/"

    # pylint: disable=consider-using-with
    tempdir_root = tempfile.TemporaryDirectory(dir=result_dir)
    tempdir = tempdir_root.name

    log.info("Putting data into temporary %s and permanent %s", tempdir, result_dir)

    parser = simulation_cli_parser()

    ssmresources = os.getenv("SSMRESOURCES", None)
    if ssmresources is None:
        err_msg = (
            "test_mid_simulation: environment variable SSMRESOURCES must "
            "set to directory containing simulation resources"
        )
        log.error(err_msg)
        raise ValueError(err_msg)

    args = parser.parse_args(
        [
            "--mode",
            mode,
            "--rmax",
            f"{rmax}",
            "--band",
            "B2",
            "--flux_limit",
            f"{flux_limit}",
            "--duration",
            "custom",
            "--time_range",
            "-0.01",
            "0.01",
            "--vp_directory",
            f"{ssmresources}/test_resources/beam_models",
            "--use_dask",
            f"{USE_DASK}",
            "--npixel",
            f"{npixel}",
            "--cellsize",
            f"{cellsize}",
            "--nchan",
            "9",
            "--results",
            tempdir,
            "--imaging_dft_kernel",
            "cpu_looped",
            "--vp_support",
            "256",
            "--source",
            f"{source}",
            "--only_actual",
            only_actual,
            "--isoplanatic",
            iso,
            "--make_images",
            "True",
        ]
    )
    _, dirtyname = simulation(args)

    dirty = import_image_from_fits(dirtyname)
    qa = dirty.image_acc.qa_image()

    # This is a differential test.
    numpy.testing.assert_approx_equal(
        qa.data["max"],
        flux_max,
        8,
        err_msg=str(qa),
        verbose=True,
    )
    numpy.testing.assert_approx_equal(
        qa.data["min"],
        flux_min,
        8,
        err_msg=str(qa),
        verbose=True,
    )

    # Clean up.
    if persist is True:
        try:
            shutil.copyfile(tempdir + "/SKA_MID_SIM_custom*.ms", result_dir)
        except FileNotFoundError:
            pass

        to_copy = tempdir + "/SKA_MID_SIM_custom*"
        for f in glob.glob(to_copy):
            shutil.copy(f, result_dir)
