# pylint: disable=too-many-instance-attributes
# pylint: disable=duplicate-code

"""Tests for Mid simulation functions"""

import logging
import os
import tempfile
import unittest

import numpy
from astropy import units as u
from astropy.coordinates import SkyCoord
from rascil.workflows.rsexecute.execution_support.rsexecute import rsexecute
from ska_sdp_datamodels.configuration import create_named_configuration
from ska_sdp_datamodels.image.image_io_and_convert import import_image_from_fits
from ska_sdp_datamodels.science_data_model.polarisation_model import PolarisationFrame
from ska_sdp_datamodels.visibility import create_visibility

from src.mid_simulation import cli_parser as simulation_cli_parser
from src.mid_simulation import make_images_workflow

log = logging.getLogger("ska-sim-mid-logger")
log.setLevel(logging.WARNING)


class TestMidSimulation(unittest.TestCase):
    """Tests for mid simulation functions"""

    def setUp(self):
        rsexecute.set_client(use_dask=False)
        self.persist = os.environ.get("RASCIL_PERSIST", False)

    def tearDown(self):
        rsexecute.close()

    # pylint: disable=missing-function-docstring,invalid-name
    # pylint: disable=attribute-defined-outside-init
    def actualSetUp(self):
        self.frequency = numpy.linspace(1e9, 1.5e9, 3)
        self.channelwidth = numpy.array([2.5e8, 2.5e8, 2.5e8])
        self.vis_pol = PolarisationFrame("stokesI")
        self.image_pol = PolarisationFrame("stokesI")
        self.npixel = 256
        self.cellsize = 0.00005
        self.times = numpy.linspace(-300.0, 300.0, 3) * numpy.pi / 43200.0

        self.phasecentre = SkyCoord(
            ra=+180.0 * u.deg, dec=-60.0 * u.deg, frame="icrs", equinox="J2000"
        )

        self.config = create_named_configuration("MID", rmax=500.0)

        self.vis = create_visibility(
            self.config,
            self.times,
            self.frequency,
            channel_bandwidth=self.channelwidth,
            phasecentre=self.phasecentre,
            weight=1.0,
            polarisation_frame=self.vis_pol,
        )

        parser = simulation_cli_parser()
        self.args = parser.parse_args(
            [
                "--mode",
                "nominal",
                "--rmax",
                "5e2",
                "--duration",
                "custom",
                "--time_range",
                "-0.01",
                "0.01",
                "--use_dask",
                "True",
                "--npixel",
                f"{self.npixel}",
                "--cellsize",
                f"{self.cellsize}",
                "--results",
                "results",
            ]
        )

    def test_make_images_workflow(self):
        """Test make_images_workflow function, happy path"""
        self.actualSetUp()
        with tempfile.TemporaryDirectory() as tempdirname:
            dirtyname = f"{tempdirname}/test_make_images_workflow.fits"
            bvis_list = make_images_workflow(
                dirtyname, self.args, self.image_pol, [self.vis], "nominal"
            )
            rsexecute.compute(bvis_list, sync=True)

            assert os.path.exists(dirtyname)

            dirty = import_image_from_fits(dirtyname)
            qa = dirty.image_acc.qa_image()

            assert qa.data["shape"] == "(3, 1, 256, 256)"

            if self.persist is False:
                os.remove(dirtyname)


if __name__ == "__main__":
    unittest.main()
