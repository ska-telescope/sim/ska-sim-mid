# pylint: disable=too-many-locals,too-many-statements

"""
Simulation of the effect of errors on MID observations

Imaging step
"""
import argparse
import glob
import logging
import os
import pprint
import sys

import matplotlib
import matplotlib.pyplot as plt

matplotlib.use("Agg", force=True)

import numpy as np
from casacore.tables import table
from rascil.processing_components import plot_uvcoverage
from ska_sdp_datamodels.visibility import export_visibility_to_ms
from ska_sdp_datamodels.visibility.vis_io_and_convert import import_visibility_from_hdf5
from ska_sdp_func_python.visibility import concatenate_visibility

pp = pprint.PrettyPrinter()


def init_logging():
    """Initialize logging"""
    logging.basicConfig(
        filename="convert_to_ms.log",
        filemode="a",
        format="%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s",
        datefmt="%H:%M:%S",
        level=logging.INFO,
    )


init_logging()
log = logging.getLogger("logger")
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))


def do_merge(args):
    """Merge hdf files into ms format"""
    log.info("mid_image:\nStarting MID convert to ms\n")

    pp.pprint(vars(args))

    hdffile = args.hdffile
    nfiles = args.nfiles

    if hdffile == "":
        files = glob.glob("SKA_MID_SIM*_0.hdf")
    else:
        files = [hdffile]

    for f in files:
        file = f.replace("_0.hdf", "")
        print(f"Processing {file}")
        if nfiles <= 0:
            files = glob.glob(f"{file}_*.hdf")
            nfiles = len(files)
            print(f"Found {nfiles} files")

        try:
            bvis_list = [
                import_visibility_from_hdf5(f"{file}_{i}.hdf") for i in range(nfiles)
            ]

            entire_bvis = concatenate_visibility(bvis_list)
            msname = f"{file}.ms"
            size = entire_bvis.nbytes / 1024 / 1024 / 1024
            print(f"Size of concatenated BlockVis = {size:.3f} GB")
            export_visibility_to_ms(msname, [entire_bvis])
            if args.plot == "True":
                plot_vis(msname, 0)
                plot_uvcoverage(
                    bvis_list,
                    plot_file=f"{file}_uvcoverage.png",
                    title=file,
                )
        except OSError as e:
            print(e)

    return True


def plot_vis(msname, nch=0):
    """Plotting visibilities."""
    print("Plotting vis plot for the channel", nch, "of the MS", msname)
    t = table(msname, readonly=True)
    # Ignore autocorrelations.
    t = t.query("ANTENNA1 != ANTENNA2")
    ant1 = t.getcol("ANTENNA1")
    ant2 = t.getcol("ANTENNA2")
    timestamps = t.getcol("TIME")

    ms = np.array(t.getcol("DATA"), dtype=np.complex128)  # [:, ch, corr]
    # Flag if one correlation is flagged.
    flags = np.any(
        np.array(t.getcol("FLAG"), bool), axis=2
    )  # [:, ch] - All frequency should be flagged out.
    t.close()
    print(f"# Rows: {ms.shape[0]}")
    print(f"# Channels: {ms.shape[1]}")
    print(f"# Correlations: {ms.shape[2]}")
    print(f"{np.sum(flags) / flags.size * 100} % flagged")

    t = table(os.path.join(msname, "SPECTRAL_WINDOW"), readonly=True)
    freq = t.getcol("CHAN_FREQ")[0]
    if nch < 0:
        nch = 0
    elif nch > (freq.shape[0] - 1):
        nch = freq.shape[0] - 1
    print(
        "MS contains",
        freq.shape[0],
        "frequency channel(s), from 0 to",
        (freq.shape[0] - 1),
    )
    print("Plotting vis for the channel", nch, ", freq =", freq[nch], "Hz")
    title_part2 = str(freq[nch]) + " Hz"
    t.close()

    t = table(os.path.join(msname, "ANTENNA"), readonly=True)
    # pp.pprint(t[0])
    ant_position = np.array(t.getcol("POSITION"))
    t.close()

    baselength = np.zeros(ms.shape[0])
    for i in range(ms.shape[0]):
        iant1 = ant1[i]
        iant2 = ant2[i]
        xyz2 = (ant_position[iant1, :] - ant_position[iant2, :]) ** 2
        baselength[i] = np.sqrt(np.sum(xyz2))

    tmin = np.amin(timestamps)
    timestamps1 = timestamps - tmin
    timestamps1 = timestamps1 / 60.0
    tmax = np.amax(timestamps1)

    baselength_log10 = np.log10(baselength)
    blmax_log10 = np.amax(baselength_log10)
    blmin_log10 = np.amin(baselength_log10)
    print("Log values min, max", blmin_log10, blmax_log10)

    # Plot dimensions.
    nxt = int(np.amax(timestamps1))
    nx = len(np.unique(timestamps1))
    ny = int(nxt)
    # print(nx, nxt, ny, timestamps1.shape)

    visdata_log = np.zeros((nx, ny, 4))
    viscount_log = np.zeros((nx, ny, 4))
    # Loop over visibilities.
    for i in range(baselength.shape[0]):
        ix = int(np.rint(timestamps1[i] / tmax * (nx - 1)))
        iyl = int(
            np.rint(
                (baselength_log10[i] - blmin_log10)
                / (blmax_log10 - blmin_log10)
                * (ny - 1)
            )
        )
        visdata_log[ix, iyl, :] = visdata_log[ix, iyl, :] + np.abs(ms[i, nch, :])
        viscount_log[ix, iyl, :] = viscount_log[ix, iyl, :] + 1

    for i in range(nx):
        for j in range(ny):
            if viscount_log[i, j, 0] > 0.0:
                visdata_log[i, j, :] = visdata_log[i, j, :] / viscount_log[i, j, :]

    title_full = msname + "\n" + title_part2
    plt.clf()
    fig, axs = plt.subplots(2, 2, figsize=(12, 10))
    fig.suptitle(title_full, fontsize=20.0)
    sptitle = ["XX", "XY", "YX", "YY"]

    for col in range(2):
        for row in range(2):
            ax = axs[row, col]
            idx = 2 * col + row
            im = ax.imshow(
                visdata_log[:, :, idx],
                extent=[blmin_log10, blmax_log10, tmax, 0],
                cmap="nipy_spectral",
            )
            ax.set_aspect((blmax_log10 - blmin_log10) / tmax)
            ax.set_title(sptitle[idx], fontsize=14.0)
            if idx in (1, 3):
                ax.set_xlabel("Base length, log10(m)", fontsize=13.0)
            if idx in (0, 1):
                ax.set_ylabel("Time, min", fontsize=13.0)
            cbar = fig.colorbar(im, ax=ax)
            if idx in (2, 3):
                cbar.ax.set_ylabel("Flux, Jy", rotation=270, fontsize=13.0, labelpad=13)

    figure_name = str(os.path.splitext(msname)[0]) + "_vis.jpg"
    plt.savefig(figure_name)
    plt.clf()
    print("Exporting plot to ", figure_name)

    return True


def cli_parser():
    """Get command line inputs."""
    parser = argparse.ArgumentParser(
        description="Merge HDF files and convert them to MeasurementSet"
    )
    parser.add_argument("--hdffile", type=str, default="", help="hdf5 file name")
    parser.add_argument("--nfiles", type=int, default=0, help="Number of files")
    parser.add_argument("--plot", type=str, default="False", help="Make plots?")

    return parser


if __name__ == "__main__":
    parser_func = cli_parser()
    input_args = parser_func.parse_args()
    do_merge(input_args)
