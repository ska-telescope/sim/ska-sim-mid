"""
Script to read in a TPOINT file containing actual and observed Az/El for a
list of Global Pointing targets and generate a set of HDF5 files to
represent the corresponding outputs from the SDP pointing-offset pipeline.
These will be used for an end-to-end test of the global pointing processing
chain.
"""

import argparse
import logging
import os
import re

import katpoint
import numpy
from astropy import time, units
from astropy.coordinates import EarthLocation, Latitude, SkyCoord
from ska_sdp_datamodels.calibration import (
    export_pointingtable_to_hdf5,
    import_pointingtable_from_hdf5,
)
from ska_sdp_dataproduct_metadata import MetaData, ObsCore
from ska_sdp_func_python.util.coordinate_support import ecef_to_lla, lla_to_ecef

log = logging.getLogger("ska-sim-mid-logger")
logging.basicConfig(level=logging.INFO)

# These are the descriptions used in the HDF file
# (as defined in pointing pipeline version 0.7.0) in
# src/ska_sdp_wflow_pointing_offset/pointing_pipeline.py
DATA_COMMENT = {
    "time": "The middle timestamp in MJD of the central scan of "
    "the pointing observation. This corresponds to the time at "
    "which the commanded_pointing is calculated. If the central "
    "scan is not found then commanded_pointing cannot be "
    "calculated and the median of the middle timestamps from "
    "all scans is used. In the two-dish mode scenario, this "
    "timestamp for the two sets of observations are stored",
    "frequency": "The central frequency in Hz if fitting to gains "
    "and frequency at the higher end of the band if fitting to "
    "visibilities",
    "weight": "The inverse square of the standard error in the "
    "fitted pointing values in radians",
    "pointing": "The pointing offsets in cross-elevation and "
    " elevation in radians for all antennas in units of radians",
    "expected_width": "The theoretical voltage beam sizes for all "
    "antennas in radians in the horizontal and "
    "vertical co-polarisations",
    "fitted_width": "The fitted voltage beam sizes for all antennas "
    "in radians in the horizontal and vertical co-polarisations",
    "fitted_width_std": "The standard error on the fitted_width in radians",
    "fitted_height": "The fitted Gaussian height for all antennas "
    "in arbitrary units",
    "fitted_height_std": "The standard error on the "
    "fitted_height in arbitrary units",
    "band_type": "Observing band",
    "scan_mode": "Pointing observation mode",
    "track_duration": "How long each scan position was tracked for in seconds",
    "discrete_offset": "Discrete offset of each scan from the central scan, "
    "in degrees (xel-el)",
    "commanded_pointing": "The commanded pointings at the middle "
    "timestamp of the central scan of the pointing observation in radians. "
    "In the two-dish mode scenario, these commanded pointings for the two "
    "sets of observations are stored",
}
POINTING_METADATA_FILE = "ska-data-product.yaml"


def _construct_antennas(xyz, diameter, station):
    """
    Construct list of katpoint Antenna objects
    based on telescope configuration information.

    :param xyz: xyz coordinates of antenna positions in [nants, 3]
    :param diameter: Diameter of dishes in [nants]
    :param station: List of the antenna names [nants]

    :return: a set of katpoint Antenna objects
    """
    latitude, longitude, altitude = ecef_to_lla(xyz[:, 0], xyz[:, 1], xyz[:, 2])
    ants = []
    for ant_name, diam, lat, long, alt in zip(
        station, diameter, latitude, longitude, altitude
    ):
        # Antenna information
        # The beamwidth is HPBW of an antenna: k * lambda/D
        # We use an estimate of k=1.22 here but not used here
        # at all. The "beamwidth" as used is actually referring
        # to the beamwidth factor, k.
        ant = katpoint.Antenna(
            name=ant_name,
            latitude=lat,
            longitude=long,
            altitude=alt,
            diameter=diam,
            delay_model=None,
            pointing_model=None,
            beamwidth=1.22,
        )
        ants.append(ant)

    return ants


def _mjd_to_unix(mjd_secs):
    """
    Convert MJD seconds to Unix time in seconds

    :param mjd_secs: MJD time in seconds

    :return: Unix time in seconds
    """
    return mjd_secs - (40587.0 * 86400.0)


def _azel_to_radec(azimuth, elevation, time_unix, antenna):
    """
    Converts a target position in Azimuth and Elevation
    to Right Ascension and Declination

    :param azimuth: Azimuth in radians
    :param elevation: Elevation in radians
    :param time_unix: Unix time in seconds
    :param antenna: katpoint Antenna object

    :return: Target position in Right Ascension and Declination
        in degrees in SkyCoord object
    """
    target_azel = katpoint.construct_azel_target(az=azimuth, el=elevation)
    target_ra, target_dec = target_azel.radec(time_unix, antenna)

    return SkyCoord(
        ra=numpy.degrees(target_ra) * units.deg,
        dec=numpy.degrees(target_dec) * units.deg,
        frame="icrs",
    )


def _update_antenna_location(old_x, old_y, old_z, new_lat):
    """
    Updates existing antenna position with a new latitude

    :param old_x: x-coordinate of antenna position in ECEF unit
    :param old_y: y-coordinate of antenna position in ECEF unit
    :param old_z: z-coordinate of antenna position in ECEF unit
    :param new_lat: New latitude of antenna in DMS string

    :return: Updated antenna position in ECEF unit
    """
    old_lat, old_lon, old_alt = ecef_to_lla(old_x, old_y, old_z)

    # Update antenna coordinates using TPOINT file latitude
    # Keep longitude and altitude as the old values
    new_lat = Latitude(new_lat, units.deg)

    log.info(
        "Updating the latitude of the antenna from %s to %s",
        numpy.degrees(old_lat),
        new_lat.deg,
    )
    new_x, new_y, new_z = lla_to_ecef(new_lat.rad, old_lon, old_alt)

    return new_x, new_y, new_z


class PointingOffsetHDF:
    """
    Main class that handles the sample HDF and TPOINT file

    :param hdf_filename: The sample HDF filename
    :param trackDuration: The integration time per scan in seconds
    """

    def __init__(self, hdf_filename, track_duration=60.0):
        self.hdf_filename = hdf_filename
        self._track_duration = track_duration
        self._pointing_table = None
        self._pb_id = None
        self._eb_id = None
        self._t0 = 0.0

    @property
    def read_pointingtable(self):
        """
        Reads the PointingTable stored in the sample HDF file
        """
        self._pointing_table = import_pointingtable_from_hdf5(self.hdf_filename)
        return self._pointing_table

    @classmethod
    def read_tpoint_file(cls, tpoint_filename):
        """
        Extracts all lines from the TPOINT file

        :param tpoint_filename: TPOINT filename

        :return: All lines in the TPOINT file
        """
        with open(tpoint_filename, "r", encoding="utf-8") as f:
            lines = f.readlines()
        return lines

    @classmethod
    def get_pointings_and_offsets(cls, flines, do_header=True):
        """
        Extracts dish pointing positions and computed pointing offsets
        from the TPOINT file

        :param flines: All lines of text in the TPOINT file
        :param do_header: Boolean to handle how the header is handled
            for the extraction of parameters of interest

        :return: The header, actual and commanded pointings in radians, pointing
            offsets in azimuth and elevation in the TPOINT file
        """
        header = []
        actual_coord = []
        commanded_coord = []
        pointing_offsets = []
        for line in flines:
            if do_header is False:
                values = line.split()
                actual = SkyCoord(
                    az=float(values[0]) * units.deg,
                    alt=float(values[1]) * units.deg,
                    frame="altaz",
                )
                commanded = SkyCoord(
                    az=float(values[2]) * units.deg,
                    alt=float(values[3]) * units.deg,
                    frame="altaz",
                )

                dcross_el = (actual.az - commanded.az) * numpy.cos(actual.alt.rad)
                delev = actual.alt - commanded.alt

                actual_coord.append(actual)
                commanded_coord.append(commanded)
                pointing_offsets.append(numpy.column_stack((dcross_el.rad, delev.rad)))
            else:
                if line[0] != ":":
                    header.append(line.replace("\n", ""))

            if line == "\n":
                do_header = False

        return header, actual_coord, commanded_coord, pointing_offsets

    def get_partial_path(self, t0):
        """
        Generates the partial path to the data products which has both the
         eb ID and pb ID based on the format required by SDP

        :param t0: Start time of global pointing observation in MJD seconds

        :return: The partial data directory path which has both the eb ID and PB ID
        """
        # Make execution block ID from time - assume a single EB for all sources.
        eb_time = time.Time(_mjd_to_unix(t0), format="unix").to_datetime()
        generator = "globalpointing"
        self._eb_id = f"eb-{generator}-{eb_time.strftime('%Y%m%d')}-00000"
        self._pb_id = f"pb-{generator}po-{eb_time.strftime('%Y%m%d')}-00000"
        partial_path = f"/product/{self._eb_id}/ska-sdp/{self._pb_id}"

        return partial_path

    def get_starttime(self, header):
        """
        Reads the header in the TPOINT file. The header is the date
        and latitude of the dish and converts this time to MJD seconds

        :param header: Header in the TPOINT file

        :return: Start time of global pointing observation in MJD seconds
        """
        # Start time for global pointing observing run (MJD seconds)
        # Take the date from the input TPOINT file and convert to MJD seconds
        date = time.Time(header[0], format="isot", scale="utc")
        log.info("Setting the date of observation to %s", date.strftime("%Y/%m/%d"))
        t0 = date.mjd * 86400.0
        self._t0 = t0

        return t0

    def update_pointingtable_attrs(self, header):
        """
        Updates the pointing table attributes from TPOINT header

        :param header: Header in the TPOINT file

        :return: Updated pointing table
        """
        # Read the example pointing table
        pointing_table = self.read_pointingtable

        # Get existing longitude and altitude from example file telescope location
        x, y, z = _update_antenna_location(
            pointing_table.configuration.attrs["location"].x.value,
            pointing_table.configuration.attrs["location"].y.value,
            pointing_table.configuration.attrs["location"].z.value,
            Latitude(header[1], units.deg),
        )

        pointing_table.configuration.attrs["name"] = "SKA"
        pointing_table.configuration.attrs["location"] = EarthLocation.from_geocentric(
            x, y, z, units.m
        )
        pointing_table.configuration.xyz.data[:, 0] = x
        pointing_table.configuration.xyz.data[:, 1] = y
        pointing_table.configuration.xyz.data[:, 2] = z

        # Set up scan information
        n_scans = int(pointing_table.scan_mode.split("-")[0])
        # If n_scans is changed here, would also need to update
        # discrete_offset to be consistent
        pointing_table.attrs["scan_mode"] = f"{n_scans}-point"

        pointing_table.attrs["track_duration"] = self._track_duration

        # Get the start time
        self.get_starttime(header)

        return pointing_table

    # pylint: disable=too-many-locals
    def update_pointingtable_with_source_list(
        self,
        pointing_table,
        pointing_offsets,
        actual_coord,
        commanded_coord,
    ):
        """
        Updates the pointing offsets in the sample HDF file with those from
        the TPOINT file

        :param pointing_table: The pointing_table to update
        :param pointing_offsets: A list of Pointing offsets in azimuth and
            elevation extracted from the TPOINT file and converted to
            cross-elevation and elevation
        :param actual_coord: Actual dish pointings in radians
        :param commanded_coord: Commanded dish pointings in radians
        """
        n_scans = int(pointing_table.scan_mode.split("-")[0])
        partial_path = self.get_partial_path(self._t0)

        # Construct antenna information in katpoint (for RA/Dec calculation later)
        antennas = _construct_antennas(
            pointing_table.configuration.xyz.data,
            pointing_table.configuration.diameter.data,
            pointing_table.configuration.names.data,
        )

        central_scan = int(n_scans / 2)
        for scan_idx, coord in enumerate(pointing_table.attrs["discrete_offset"]):
            if numpy.array_equal(coord, numpy.zeros(2)):
                central_scan = scan_idx + 1
                break

        # Loop over source list and update pointing_table + save to HDF
        # for each source
        for i, offset in enumerate(pointing_offsets):
            # First and last scans for this source
            first_scan_id = int(i * n_scans + 1)
            last_scan_id = int(first_scan_id + n_scans - 1)

            common_prefix = (
                f"{os.path.dirname(self.hdf_filename)}/"
                f"{partial_path}/scan{first_scan_id}-{last_scan_id}"
            )

            os.makedirs(common_prefix, exist_ok=True)
            results_file_hdf = (
                common_prefix
                + f"/pointing_offsets_scans_{first_scan_id}-{last_scan_id}.hdf5"
            )

            # Pointing offsets - the same value for all antennas
            pointing_table.pointing.data[0, :, 0, 0, 0] = offset[0][0]
            pointing_table.pointing.data[0, :, 0, 0, 1] = offset[0][1]

            # MJD time in seconds - calculated from trackDuration and n_scans
            # Note that time is the midpoint of the central scan
            # (not the start of observation)
            pointing_table.time.data[:] = (
                self._t0
                + i * n_scans * self._track_duration
                + (central_scan - 1) * self._track_duration
                + self._track_duration / 2
            )

            # Commanded pointing - same value for all antennas
            pointing_table.commanded_pointing[0, :, 0, 0, 0] = commanded_coord[i].az.rad
            pointing_table.commanded_pointing[0, :, 0, 0, 1] = commanded_coord[
                i
            ].alt.rad

            # Convert MJD seconds to UTC seconds since unix epoch
            time_unix = _mjd_to_unix(pointing_table.time.data[0])

            # Convert target position from AZ/EL to RA/Dec from Az/El.
            # Use first antenna to do the calculation and update the pointing_table
            pointing_table.attrs["pointingcentre"] = _azel_to_radec(
                actual_coord[i].az.rad,
                actual_coord[i].alt.rad,
                time_unix,
                antennas[0],
            )

            # Export to HDF
            export_pointingtable_to_hdf5(
                pointing_table, results_file_hdf, **DATA_COMMENT
            )

            # Create a metadata yaml file
            metadata = self.create_metadata(results_file_hdf)

            # Update the observation specific obscore metadata
            metadata = self.update_obscore_metadata(
                metadata, results_file_hdf, pointing_table
            )

            log.info(
                "%s: %s [%s], %s",
                i,
                results_file_hdf,
                actual_coord[i].to_string(),
                offset[0],
            )

    def create_metadata(self, results_file_hdf):
        """
        Create metadata file with basic obscore data and
        dataproduct file information.

        :param results_file_hdf: full path to the HDF5 dataproduct file
        """
        scan_ids = [
            *range(
                int(re.split("[-_.]", results_file_hdf)[-3]),
                int(re.split("[-_.]", results_file_hdf)[-2]) + 1,
            )
        ]

        metadata = MetaData()

        metadata_path = f"{os.path.dirname(results_file_hdf)}/{POINTING_METADATA_FILE}"
        metadata.output_path = metadata_path

        data = metadata.get_data()

        # Add processing parameters
        data.config = {
            "cmdline": None,
            "commit": None,
            "image": "artefact.skao.int/ska-sdp-script-pointing-offset",
            "processing_block": self._pb_id,
            "processing_script": "pointing-offset",
            "version": "0.7.0",
        }
        data.execution_block = self._eb_id
        # data.interface = "http://schema.skao.int/ska-data-product-meta/0.1"

        data.obscore.dataproduct_type = ObsCore.DataProductType.POINTING
        data.obscore.calib_level = ObsCore.CalibrationLevel.LEVEL_0
        data.obscore.obs_collection = (
            f"{ObsCore.SKA}/"
            f"{ObsCore.SKA_MID}/"
            f"{ObsCore.DataProductType.POINTING.value}"
        )
        data.obscore.access_format = ObsCore.AccessFormat.HDF5
        data.obscore.facility_name = ObsCore.SKA
        data.obscore.instrument_name = ObsCore.SKA_MID

        # need to save the file path without the mount directory
        # it needs to start with /product
        file_path_wo_mount = f"/product{results_file_hdf.split('product')[1]}"
        metadata_file = metadata.new_file(
            dp_path=file_path_wo_mount,
            description=f"Pointing offsets for scans: {scan_ids}",
        )

        # Mark HDF file as done
        try:
            metadata_file.update_status("done")
        except MetaData.ValidationError as err:
            log.error("Metadata validation failed with error(s): %s", err.errors)

        try:
            metadata.write()
        except MetaData.ValidationError as err:
            log.error("Validation failed with error(s): %s", err.errors)
            raise err

        return metadata

    def update_obscore_metadata(self, metadata, results_file_hdf, pointing_table):
        """
        Updates the metadata object with obscore values.

        :param metadata: metadata to update
        :param results_file_hdf: path and filename to HDF file
        :param pointing_table: The PointingTable hosting the
           pointing offsets and other fitted parameters and their
           uncertainties.
        """
        n_scans = int(pointing_table.scan_mode.split("-")[0])
        first_scan = int(re.split("[-_.]", results_file_hdf)[-3])
        source_number = int((first_scan - 1) / n_scans + 1)
        # Find central scan
        central_scan = int(n_scans / 2)
        for scan_idx, coord in enumerate(pointing_table.attrs["discrete_offset"]):
            if numpy.array_equal(coord, numpy.zeros(2)):
                central_scan = scan_idx + 1
                break

        if os.path.isfile(results_file_hdf):
            hdf_filesize_kb = round(os.path.getsize(results_file_hdf) / 1024.0)
        else:
            log.warning(
                "Pointing output HDF file, %s, does not exist. Setting the "
                "file size to zero in the metadata yaml file.",
                results_file_hdf,
            )
            hdf_filesize_kb = 0

        obscore_to_fill = {
            "obs_id": "",
            "access_estsize": hdf_filesize_kb,
            "target_name": f"global_pointing_source{source_number}",
            "s_ra": pointing_table.attrs["pointingcentre"].ra.deg,
            "s_dec": pointing_table.attrs["pointingcentre"].dec.deg,
            "t_min": (
                pointing_table.time.data[0]
                - (central_scan - 1) * self._track_duration
                - self._track_duration / 2
            ),
            "t_max": (
                pointing_table.time.data[0]
                + (n_scans - central_scan) * self._track_duration
                + self._track_duration / 2
            ),
            "em_min": katpoint.lightspeed / (pointing_table.frequency.data[0]),
            "em_max": katpoint.lightspeed / (pointing_table.frequency.data[-1]),
            "pol_states": pointing_table.receptor_frame.type,
            "pol_xel": pointing_table.receptor_frame.nrec,
        }

        data = metadata.get_data()
        for obscore_item in obscore_to_fill.items():
            setattr(data.obscore, obscore_item[0], obscore_item[1])

        try:
            metadata.write()
        except MetaData.ValidationError as err:
            log.error("Validation failed with error(s): %s", err.errors)
            raise err

        return metadata


# pylint: disable=too-many-locals
def main():
    """Get command line inputs"""
    parser = argparse.ArgumentParser(
        description="Generates pointing HDF files from a sample pointing HDF file"
    )

    parser.add_argument("--hdf_filename", type=str, help="Sample HDF filename")
    parser.add_argument("--tpoint_filename", type=str, help="Sample TPOINT file")
    args = parser.parse_args()

    # Import an example pointing HDF5 file to use as template as a
    # PointingTable object
    init = PointingOffsetHDF(args.hdf_filename)
    # Read the TPOINT file
    lines = init.read_tpoint_file(args.tpoint_filename)
    # Extract information from the TPOINT file lines
    header, actual_coord, commanded_coord, pointing_offsets = (
        init.get_pointings_and_offsets(lines, do_header=True)
    )

    # Update pointing table attributes from TPOINT header
    pointing_table = init.update_pointingtable_attrs(header)

    # Loop over the source list and update pointing_table data
    # and write out HDF file for each source.
    init.update_pointingtable_with_source_list(
        pointing_table,
        pointing_offsets,
        actual_coord,
        commanded_coord,
    )


if __name__ == "__main__":
    main()
