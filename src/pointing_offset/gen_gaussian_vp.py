# pylint: disable=too-many-locals,too-many-branches,invalid-name

"""
Generating Gaussian Voltage Pattern
file for SKA-SIM-MID
The Voltage Patter is used for pointing offset
simulations
"""

import argparse
import copy
import logging
import os
import sys

import numpy as np
import scipy.constants as CC
from astropy import units as u
from astropy.coordinates import SkyCoord
from ska_sdp_datamodels.image import create_image
from ska_sdp_datamodels.science_data_model import PolarisationFrame

log = logging.getLogger("ska-sim-mid-logger")
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))


def cli_parser():
    """Command line interface parser"""
    parser = argparse.ArgumentParser(
        description="Generate Gaussian Voltage Pattern "
        "for pointing offset simulations."
    )

    parser.add_argument(
        "--vp_directory",
        type=str,
        default=None,
        help="Location of the output of voltage pattern files",
    )

    parser.add_argument(
        "--vp_size",
        type=int,
        default=None,
        help="Number of pixels in voltage pattern image",
    )

    parser.add_argument(
        "--cellsize",
        type=float,
        default=0.0078125,
        help="Cell size (deg)",
    )

    parser.add_argument(
        "--frequency",
        type=float,
        default=1.36e9,
        help="Intermediate Frequency (Hz)",
    )

    parser.add_argument(
        "--channel_bandwidth",
        type=float,
        default=48828.125,
        help="Channel bandwidth (Hz) ",
    )

    parser.add_argument(
        "--band",
        type=str,
        default="B2",
        help="Name of the Band (B2 or B5) ",
    )
    parser.add_argument(
        "--nchan",
        type=int,
        default=8,
        help="The number of channels ",
    )

    return parser


def gaussian_2d(x, y, mean, sigma):
    """
    2D Gaussian function
    """
    return np.exp(-((x - mean[0]) ** 2 + (y - mean[1]) ** 2) / (2 * sigma**2))


def fwhm_to_sigma(fwhm):
    """
    Convert FWHM to sigma
    """
    return fwhm / (2 * np.sqrt(2 * np.log(2)))


def gen_vp(args):
    """Generate Voltage Pattern files.

    :param args: Arguments list
    """

    if args.vp_directory is None:
        vp_directory = "."
    else:
        vp_directory = args.vp_directory
    if args.vp_size is not None:
        vp_size = args.vp_size
    else:
        vp_size = 1024
    if args.frequency is not None:
        frequency = args.frequency
    else:
        frequency = 1.36e9
    if args.channel_bandwidth is not None:
        channel_bandwidth = args.channel_bandwidth
    else:
        channel_bandwidth = 48828.125
    if args.nchan is not None:
        nchan = args.nchan
    else:
        nchan = 8
    if args.cellsize is not None:
        cellsize = args.cellsize
    else:
        cellsize = 0.0078125

    phasecentre = SkyCoord(
        ra=0.0 * u.deg,
        dec=0.0 * u.deg,
        frame="icrs",
        equinox="J2000",
    )

    new_vp = create_image(
        npixel=vp_size,
        cellsize=np.deg2rad(cellsize),
        phasecentre=phasecentre,
        frequency=frequency,
        channel_bandwidth=channel_bandwidth,
        nchan=nchan,
        polarisation_frame=PolarisationFrame("linear"),
    )

    new_vp.attrs["_projection"] = ("AZELGEO long", "AZELGEO lati")
    new_vp.attrs["refpixel"] = np.array([vp_size // 2 + 1, vp_size // 2 + 1, 1.0, 1.0])

    fwhm = 1.22 * (CC.c / new_vp.frequency.data) / 15
    fwhm_pixel = fwhm / np.deg2rad(new_vp.image_acc.wcs.wcs.cdelt[1])

    mean = [vp_size // 2, vp_size // 2]
    sigmas = fwhm_to_sigma(fwhm_pixel)

    X, Y = np.meshgrid(
        np.linspace(0, vp_size - 1, vp_size), np.linspace(0, vp_size - 1, vp_size)
    )

    for idx, sigma in enumerate(sigmas):
        # pylint: disable=logging-fstring-interpolation
        log.info(f"Freq. {new_vp.frequency.data[idx]:.3f}: Sigma : {sigma}")

        gauss_image = gaussian_2d(X, Y, mean, sigma)

        new_vp["pixels"].data[idx, 0][:, :] = gauss_image
        new_vp["pixels"].data[idx, 1][:, :] = np.zeros_like(gauss_image)
        new_vp["pixels"].data[idx, 2][:, :] = np.zeros_like(gauss_image)
        new_vp["pixels"].data[idx, 3][:, :] = gauss_image

    band = args.band
    output_real_file = os.path.join(
        vp_directory,
        f"{band}_real_gaussian_{frequency:.3e}_{nchan}.fits",
    )
    output_imag_file = os.path.join(
        vp_directory,
        f"{band}_imag_gaussian_{frequency:.3e}_{nchan}.fits",
    )
    new_vp.image_acc.export_to_fits(output_real_file)

    new_vp_imag = copy.deepcopy(new_vp)
    new_vp_imag["pixels"].data = np.repeat(
        np.repeat(np.zeros_like(gauss_image)[None, None], nchan, 0), 4, 1
    )
    new_vp_imag.image_acc.export_to_fits(output_imag_file)
    log.info(
        "Done. The files have been successfully saved. \n Real: %s \n Imagine: %s",
        output_real_file,
        output_imag_file,
    )


def main():
    """Get command line inputs."""
    parser = cli_parser()
    args = parser.parse_args()
    gen_vp(args)


if __name__ == "__main__":
    main()
