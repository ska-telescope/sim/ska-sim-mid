"""
Calculate the positions of 5 scans
"""

import argparse
import logging
import os
import sys

from astropy import units
from astropy.coordinates import AltAz, EarthLocation, SkyCoord
from astropy.time import Time

log = logging.getLogger("ska-sim-mid-logger")
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))


def cli_parser():
    """Command line interface parser"""
    parser = argparse.ArgumentParser(
        description="Calculate RA and DEC based on AzEl pointing offset."
    )

    parser.add_argument(
        "--output_dir",
        type=str,
        default=".",
        help="Location of output MS files",
    )

    parser.add_argument(
        "--file",
        type=str,
        default="pos_ha_astropy_band2.dat",
        help="File name",
    )

    parser.add_argument(
        "--offset",
        type=float,
        nargs=2,
        default=[0.333, 0.333],
        help="Azimuth and elevation offset (deg)",
    )

    parser.add_argument(
        "--duration",
        type=float,
        default=5,
        help="Observational duration (seconds)",
    )

    parser.add_argument(
        "--ra",
        type=float,
        default=294.8629130087718,
        help="Source Right Ascension (J2000.0, unit:degrees)",
    )

    parser.add_argument(
        "--dec",
        type=float,
        default=-63.44029403519162,
        help="Source Declination (J2000.0, unit:degrees)",
    )

    parser.add_argument(
        "--time",
        type=str,
        default="2000-01-01T6:45:00",
        help="Observational time (e.g., 2000-01-01T6:45:00 )",
    )

    return parser


# pylint: disable=too-many-locals
def calculate_ra_dec(args):
    "Calculate RA and DEC"
    output_dir = args.output_dir
    output_file = args.file
    offset = args.offset

    # SKA1-Mid Site Parameters
    # Do not modify the following variables
    latitude_degrees = -30.712925  # latitude in degrees; north positive.
    longitude_degrees = 21.443803  # longitude in degrees; east positive.
    elevation_m = 1053.000000  # Height of the observer (meters).

    mid_location = EarthLocation(
        lon=longitude_degrees * units.deg,
        lat=latitude_degrees * units.deg,
        height=elevation_m,
    )

    # Observational time
    time = Time(args.time)

    # Radio source positions
    source = SkyCoord(ra=args.ra * units.deg, dec=args.dec * units.deg, frame="icrs")

    altaz = AltAz(location=mid_location, obstime=time)
    star_altaz = source.transform_to(altaz)

    star_altaz_ha = SkyCoord(180 * units.deg, star_altaz.alt, frame=altaz)

    hour_angle = star_altaz_ha.icrs.ra - star_altaz.icrs.ra

    end_time = hour_angle.hourangle + args.duration / 3600
    start_time = hour_angle.hourangle

    offlist = [
        (-offset[0], 0),
        (offset[0], 0),
        (0, -offset[1]),
        (0, offset[1]),
        (0, 0),
    ]

    output_file_name = os.path.join(output_dir, output_file)
    with open(output_file_name, "w", encoding="utf8") as f:
        off_time = 0
        for off0, off1 in offlist:
            this_time = time + off_time * units.second
            aa_tmp = AltAz(location=mid_location, obstime=this_time)
            star_altaz = source.transform_to(aa_tmp)

            direction = SkyCoord(
                star_altaz.az + off0 * units.deg,
                star_altaz.alt + off1 * units.deg,
                frame=aa_tmp,
            )
            radec = direction.transform_to("icrs")
            this_time.format = "fits"
            f.write(
                f"{radec.ra.deg} {radec.dec.deg} "
                f"{start_time} {end_time} {this_time.value}\n"
            )
            log.info(
                "%s %s %s %s %s",
                radec.ra.deg,
                radec.dec.deg,
                start_time,
                end_time,
                this_time.value,
            )
            start_time += args.duration / 3600
            end_time += args.duration / 3600
            off_time += args.duration


def main():
    """Get command line inputs."""
    parser = cli_parser()
    args = parser.parse_args()
    calculate_ra_dec(args)


if __name__ == "__main__":
    main()
