# pylint: disable=too-many-locals,too-many-arguments
# pylint: disable=too-many-branches,too-many-statements
# pylint: disable=too-many-positional-arguments
"""
Functions that help with SKA MID simulations
"""
__all__ = [
    "get_directory_size",
    "get_vp_frequency",
    "get_vp_elevation",
    "merge_hdf_to_ms",
    "create_pb_radec",
    "create_mid_simulation_components",
]

import logging
import os
import pprint

import numpy
from rascil.processing_components import sub_image
from rascil.processing_components.simulation import create_test_skycomponents_from_s3
from ska_sdp_datamodels.image.image_create import create_image
from ska_sdp_datamodels.image.image_io_and_convert import import_image_from_fits
from ska_sdp_datamodels.science_data_model.polarisation_model import PolarisationFrame
from ska_sdp_datamodels.visibility import export_visibility_to_ms
from ska_sdp_datamodels.visibility.vis_io_and_convert import import_visibility_from_hdf5
from ska_sdp_func_python.image import convert_polimage_to_stokes
from ska_sdp_func_python.imaging import advise_wide_field
from ska_sdp_func_python.imaging.primary_beams import convert_azelvp_to_radec, create_pb
from ska_sdp_func_python.sky_component import (
    apply_beam_to_skycomponent,
    filter_skycomponents_by_flux,
)
from ska_sdp_func_python.visibility import (
    concatenate_visibility,
    convert_visibility_stokesI_to_polframe,
)

log = logging.getLogger("ska-sim-mid-logger")


def get_directory_size(directory):
    """Returns the `directory` size in bytes."""
    total = 0
    try:
        for entry in os.scandir(directory):
            if entry.is_file():
                total += entry.stat().st_size
            elif entry.is_dir():
                total += get_directory_size(entry.path)
    except NotADirectoryError:
        # If `directory` isn't a directory, get the file size then.
        # log.info("Not a directory {}".format(directory))
        return os.path.getsize(directory)
    except PermissionError:
        # log.info("Permission problem {}".format(directory))
        # if for whatever reason we can't open the folder, return 0.
        return 0
    return total


def get_vp_frequency(
    telescope,
    vp_directory,
    fixpol=False,
    vp_support=None,
    vp_real_file=None,
    vp_imag_file=None,
):
    """Create an image containing the dish voltage pattern for a number of cases.

    :param telescope: Telescope name plus band e.g. MID_B2
    :param vp_directory: Location of beam models
    :param fixpol: Fix polarisation? (Not often needed)
    :param vp_support: Support in pixels e.g. 256 means 256 x 256 pixel centered on peak
    :param vp_real_file: VP filename (Real part)
    :param vp_imag_file: VP filename (Imagine part)

    :return: Voltage pattern image
    """
    if vp_support is None:
        vp_support = (1024, 1024)
    if vp_directory == "":
        raise ValueError("The voltage pattern directory must be specified.")

    if telescope in ("MID_B1LOW", "MID_B1", "MEERKAT_B1LOW", "MEERKAT_B1"):
        directory = (
            vp_directory
            + "/SKADCBeamPatterns/2019_08_06_SKA_SPFB1/interpolated_frequency/"
        )
        vp_real_file = "B1_45_0465_real_interpolated.fits"
        vp_imag_file = "B1_45_0465_imag_interpolated.fits"

    elif telescope in ("MID_B2", "MID_NOMINAL_B2"):
        directory = (
            vp_directory
            + "/SKADCBeamPatterns/2019_08_06_SKA_SPFB2/interpolated_frequency/"
        )
        if vp_real_file is None:
            vp_real_file = "B2_45_1360_real_interpolated.fits"
        if vp_imag_file is None:
            vp_imag_file = "B2_45_1360_imag_interpolated.fits"

    elif telescope in ("MID_B2G"):
        # B2 band for pointing simulations
        directory = vp_directory + "SKADCBeamPatterns/TestB2"
        if vp_real_file is None:
            vp_real_file = "B2_real_gaussian_8.fits"
        if vp_imag_file is None:
            vp_imag_file = "B2_imag_gaussian_8.fits"

    elif telescope in ("MID_B5G"):
        # B5 band for pointing simulations
        directory = vp_directory + "SKADCBeamPatterns/TestB5"
        if vp_real_file is None:
            vp_real_file = "B5_real_gaussian_8.fits"
        if vp_imag_file is None:
            vp_imag_file = "B5_imag_gaussian_8.fits"
    elif telescope in ("MID_B5BG"):
        # B5 band for pointing simulations
        directory = vp_directory + "SKADCBeamPatterns/TestB5b"
        if vp_real_file is None:
            vp_real_file = "B5b_real_gaussian_8.fits"
        if vp_imag_file is None:
            vp_imag_file = "B5b_imag_gaussian_8.fits"

    elif telescope == "MEERKAT_B2":
        directory = vp_directory + "/MEERKATBeamPatterns/interpolated_frequency/"
        vp_real_file = "MeerKAT_VP_60_1360_real_interpolated.fits"
        vp_imag_file = "MeerKAT_VP_60_1360_imag_interpolated.fits"

    elif telescope in ("MID_Ku", "MEERKAT_Ku"):
        directory = (
            vp_directory
            + "/SKADCBeamPatterns/2019_08_06_SKA_Ku/interpolated_frequency/"
        )
        vp_real_file = "Ku_45_12000_real_interpolated.fits"
        vp_imag_file = "Ku_45_12000_imag_interpolated.fits"

    else:
        raise NotImplementedError(f"Telescope {telescope} has no voltage pattern model")
    real_vp = import_image_from_fits(
        f"{directory}/{vp_real_file}",
        fixpol=fixpol,
    )
    real_vp = sub_image(real_vp, vp_support)
    imag_vp = import_image_from_fits(
        f"{directory}/{vp_imag_file}",
        fixpol=fixpol,
    )
    imag_vp = sub_image(imag_vp, vp_support)
    real_vp["pixels"].data = real_vp["pixels"].data + 1j * imag_vp["pixels"].data
    actual = (
        telescope
        + " "
        + str(real_vp["pixels"].data[0, :, vp_support[0] // 2, vp_support[1] // 2])
    )
    assert (
        real_vp["pixels"].data[0, 0, vp_support[0] // 2, vp_support[1] // 2] > 0.5
    ), actual
    assert (
        real_vp["pixels"].data[0, 1, vp_support[0] // 2, vp_support[1] // 2] < 0.5
    ), actual
    assert (
        real_vp["pixels"].data[0, 2, vp_support[0] // 2, vp_support[1] // 2] < 0.5
    ), actual
    assert (
        real_vp["pixels"].data[0, 3, vp_support[0] // 2, vp_support[1] // 2] > 0.5
    ), actual
    real_vp["pixels"].data /= numpy.max(numpy.abs(real_vp["pixels"].data))

    return real_vp


def get_vp_elevation(telescope, vp_directory, el, fixpol=False, vp_support=None):
    """Create an image containing the dish voltage pattern for a given elevation

    The elevation dependent files must be available in vp_directory.

    :param telescope: Telescope name plus band e.g. MID_B2
    :param vp_directory: Location of beam models
    :param fixpol: Fix polarisation? (Not often needed)
    :param vp_support: Support in pixels e.g. 256 means 256 x 256 pixel centered on peak
    :param el: Elevation in degrees
    :return: Voltage pattern image
    """

    if vp_support is None:
        vp_support = (1024, 1024)
    if telescope == "MID_B1":
        directory = (
            vp_directory
            + "/SKADCBeamPatterns/2019_08_06_SKA_SPFB1/interpolated_elevation/"
        )
        vp_real_file = f"B1_{int(el)}_0565_real_interpolated.fits"
        vp_imag_file = f"B1_{int(el)}_0565_imag_interpolated.fits"

    elif telescope == "MID_B2":
        directory = (
            vp_directory
            + "/SKADCBeamPatterns/2019_08_06_SKA_SPFB2/interpolated_elevation/"
        )
        vp_real_file = (f"B2_{int(el)}_1360_real_interpolated.fits",)
        vp_imag_file = f"B2_{int(el)}_1360_imag_interpolated.fits"
    elif telescope == "MID_Ku":
        directory = (
            vp_directory
            + "/SKADCBeamPatterns/2019_08_06_SKA_Ku/interpolated_elevation/"
        )
        vp_real_file = (f"Ku_{int(el)}_11780_real_interpolated.fits",)
        vp_imag_file = f"Ku_{int(el)}_11780_imag_interpolated.fits"
    else:
        raise ValueError(f"Unknown telescope {telescope}")

    vpa = import_image_from_fits(
        f"{directory}/{vp_real_file}",
        fixpol=fixpol,
    )
    vpa = sub_image(vpa, vp_support)
    vpa_imag = import_image_from_fits(
        f"{directory}/{vp_imag_file}",
        fixpol=fixpol,
    )
    vpa_imag = sub_image(vpa_imag, vp_support)
    actual = (
        telescope
        + " "
        + str(vpa["pixels"].data[0, :, vp_support[0] // 2, vp_support[1] // 2])
    )
    assert (
        vpa["pixels"].data[0, 0, vp_support[0] // 2, vp_support[1] // 2] > 0.5
    ), actual
    assert (
        vpa["pixels"].data[0, 1, vp_support[0] // 2, vp_support[1] // 2] < 0.5
    ), actual
    assert (
        vpa["pixels"].data[0, 2, vp_support[0] // 2, vp_support[1] // 2] < 0.5
    ), actual
    assert (
        vpa["pixels"].data[0, 3, vp_support[0] // 2, vp_support[1] // 2] > 0.5
    ), actual

    vpa["pixels"].data = vpa["pixels"].data + 1j * vpa_imag["pixels"].data
    return vpa


def merge_hdf_to_ms(files, poldef, advise=False, cleanup=True):
    """Merge a set of Visibility HDF files into a MeasurementSet

    :param files: list of file names
    :param poldef: Desired visibility polarisation e.g. stokesI or linear
    :return: Name of MeasurementSet
    """

    bvis_list = [import_visibility_from_hdf5(file) for file in files]
    entire_bvis = concatenate_visibility(bvis_list)

    msname = files[0].replace("_0.hdf", ".ms")
    size = entire_bvis.nbytes / 1024 / 1024 / 1024
    log.info("Size of concatenated BlockVis = %.3f GB", size)

    if entire_bvis.visibility_acc.polarisation_frame != poldef:
        entire_bvis = convert_visibility_stokesI_to_polframe(entire_bvis, poldef)
        log.info(entire_bvis)

    log.info("Writing combined data to %s", msname)
    export_visibility_to_ms(msname, [entire_bvis])

    if advise:
        advice = advise_wide_field(bvis_list[0], guard_band_image=3.0)
        log.info("Advice on imaging")
        log.info(pprint.pformat(advice))

    if cleanup:
        for file in files:
            os.remove(file)

    return msname


def create_pb_radec(
    model,
    vp_directory,
    frequency,
    telescope="MID_B2",
    fixpol=False,
    vp_support=None,
    stokes=True,
    vp_real_file=None,
    vp_imag_file=None,
):
    """Create an image containing the primary beam for a number of cases
       Using the local coordinates and custom primary beam model

    :param model: Template image
    :param telescope: Telescope model
    :param vp_directory: Location of beam models
    :param frequency: Frequency in Hz
    :param fixpol: Fix polarisation? (Not often needed)
    :param vp_support: Support in pixels e.g. 256 means 256 x 256 pixel centered on peak
    :param stokes: Convert the image to Stokes parameters (i.e. IQUV)
    :param vp_real_file: Real part of VP file
    :param vp_imag_file: Imagine part of VP file
    :return: Primary beam image
    """
    try:
        vp = get_vp_frequency(
            telescope,
            vp_directory,
            fixpol=fixpol,
            vp_support=vp_support,
            vp_real_file=vp_real_file,
            vp_imag_file=vp_imag_file,
        )
        vp["pixels"].data = vp["pixels"].values * numpy.conjugate(vp["pixels"].values)
        # The beam pattern needs to be changed from XX, XY, YX, YY to IQUV. For that
        # the function below requires data to be complex.
        if stokes:
            vp = convert_polimage_to_stokes(vp, complex_image=False)
        # This xarray location based indexing like pandas selects one
        # frequency slice keeping it as a dimension.
        try:
            vp = vp.loc[{"frequency": frequency}]
        except KeyError:
            log.info("Cannot create primary beam with selected frequency.")
        # Convert primary beam image coordinates from horizontal to equatorial
        # (sine) projection. The function expects vp to have only one
        # frequency slice.
        try:
            beam = convert_azelvp_to_radec(vp, model, 0.0)
        except IndexError:
            log.error(
                "Cannot convert primary beam image "
                "coordinates from AZELGEO to equatorial."
            )
            beam = None
    except NotImplementedError:
        log.info("Primary beam type not supported.")
        beam = None

    return beam


def create_mid_simulation_components(
    phasecentre,
    frequency,
    flux_limit,
    pbradius,
    pb_npixel,
    pb_cellsize,
    vp_directory,
    fixpol=False,
    vp_support=None,
    fov=10,
    polarisation_frame=PolarisationFrame("stokesI"),
    flux_max=10.0,
    pb_type="MID",
    apply_pb=True,
    vp_real_file=None,
    vp_imag_file=None,
):
    """Construct components for MID simulation

    :param phasecentre: Centre of components
    :param frequency: Frequency in Hz
    :param flux_limit: Lower limit flux (Jy)
    :param pbradius: Radius of components in radians
    :param pb_npixel: Number of pixels in the primary beam model
    :param pb_cellsize: Cellsize in primary beam model
    :param vp_directory: Location of beam models
    :param fixpol: Fix polarisation? (Not often needed)
    :param vp_support: Support in pixels e.g. 256 means 256 x 256 pixel centered on peak
    :param fov: FOV in degrees (used to select catalog)
    :param flux_max: Maximum flux in model before application of primary beam (Jy)
    :param polarisation_frame: Polarisation frame
    :param pb_type: Type of primary beam (telescope model) e.g. "MID"
    :param apply_pb: Apply the primary beam to the output components
    :param vp_real_file: Real part of VP file
    :param vp_imag_file: Imagine part of VP file

    :return: List of skycomponents with primary beam applied
             If apply_pb = False, return the original list of skycomponents
    """

    # Make a skymodel from S3.
    log.info("create_mid_simulation_components: Constructing s3sky components")
    # Make sure frequency is in the form of numpy array
    frequency = numpy.array(frequency)

    all_components = create_test_skycomponents_from_s3(
        flux_limit=flux_limit,
        phasecentre=phasecentre,
        polarisation_frame=polarisation_frame,
        frequency=frequency,
        radius=pbradius,
        fov=fov,
    )
    original_components = filter_skycomponents_by_flux(
        all_components, flux_max=flux_max
    )
    log.info(
        "create_mid_simulation_components: %d components "
        "before filtering with primary beam",
        len(original_components),
    )

    pbmodel = create_image(
        npixel=pb_npixel,
        cellsize=pb_cellsize,
        phasecentre=phasecentre,
        frequency=frequency[0],
        nchan=len(frequency),
        polarisation_frame=polarisation_frame,
    )

    if pb_type != "MID":
        # Use the beam models from external data.
        pb = create_pb_radec(
            pbmodel,
            vp_directory,
            frequency=frequency,
            telescope=pb_type,
            fixpol=fixpol,
            vp_support=vp_support,
            vp_real_file=vp_real_file,
            vp_imag_file=vp_imag_file,
        )
    else:
        pb = create_pb(pbmodel, pb_type, pointingcentre=phasecentre, use_local=False)

    if pb is not None:
        pb_applied_components = apply_beam_to_skycomponent(original_components, pb)
    else:
        log.info("apply_beam_to_skycomponent failed. No beam has been applied.")
        pb_applied_components = original_components

    filtered_components = []
    filtered_pb_components = []
    total_flux = 0.0

    # Filter components by flux. Also make sure the original
    # and pb-applied components have the same polarisation frame.
    for icomp, comp in enumerate(original_components):
        if pb_applied_components[icomp].flux[0, 0] > flux_limit:
            total_flux += pb_applied_components[icomp].flux[0, 0]
            nchan, npol = comp.flux.shape
            scomp = comp.copy()
            npol = polarisation_frame.npol
            iflux = numpy.zeros([nchan, npol])
            iflux[:, 0] = scomp.flux[:, 0]
            scomp.flux = iflux
            scomp.polarisation_frame = polarisation_frame
            filtered_components.append(scomp)
            filtered_pb_components.append(pb_applied_components[icomp])

    log.info(
        "create_mid_simulation_components: %d components > %.6f "
        "Jy after filtering with primary beam",
        len(filtered_components),
        flux_limit,
    )
    log.info(
        "create_mid_simulation_components: Total flux in components is %g (Jy)",
        total_flux,
    )

    log.info(
        "create_mid_simulation_components: Created %d components",
        len(filtered_components),
    )

    # If applying primary beam, return components after the primary beam.
    # If not, return the original components.
    if apply_pb:
        return filtered_pb_components

    return filtered_components
