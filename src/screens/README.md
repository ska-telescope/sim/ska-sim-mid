## Generate atmospheric and ionospheric screens

This directory with the data is available on the Google Cloud Platform:
[gs://ska1-simulation-data/resources/shared/screens](https://console.cloud.google.com/storage/browser/ska1-simulation-data/resources/shared/screens)

The package [ARatmospy](https://github.com/shrieks/ARatmospy) must be in the path. 
The current version won't work for python3 so you should use the forked version at:

    git clone https://github.com/timcornwell/ARatmospy

e.g.

    PYTHONPATH=$PYTHONPATH:~/Code/ARatmospy
    
The required files can be created using:

    make all
    
Note that the files are around 10 GB in size each.
