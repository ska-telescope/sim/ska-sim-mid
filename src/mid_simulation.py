# pylint: disable=too-many-locals,too-many-lines,duplicate-code
# pylint: disable=too-many-branches,too-many-statements,too-many-arguments
# pylint: disable=logging-fstring-interpolation,too-many-function-args
# pylint: disable=unused-argument
# pylint: disable=too-many-positional-arguments

# this is because of log is defined outside and inside the simulation function
# not sure how to update that
# pylint: disable=redefined-outer-name


"""
Simulation of the effect of errors on MID observations

This measures the change in a dirty image the induced by various errors:
    - The sky can be a point source at the phase centre,
      a double source both off the phase centre,
      or a realistic sky constructed from S3-SEX catalog.
    - The observation is by MID over a range of hour angles
    - Processing can be divided into chunks of time (default 1800s)
    - Dask is used to distribute the processing over a number of workers.
    - The primary output is a csv file containing information about
        the statistics of the residual images.
"""
import argparse
import logging
import pprint
import sys
from functools import partial

import dask
import numpy
import xarray
from astropy import units as u
from astropy.coordinates import SkyCoord
from astropy.time import Time
from rascil.processing_components.parameters import rascil_data_path
from rascil.processing_components.simulation.noise import addnoise_visibility
from rascil.workflows.rsexecute import (
    create_atmospheric_errors_gaintable_rsexecute_workflow,
    create_heterogeneous_gaintable_rsexecute_workflow,
    create_pointing_errors_gaintable_rsexecute_workflow,
    create_polarisation_gaintable_rsexecute_workflow,
    create_surface_errors_gaintable_rsexecute_workflow,
    invert_list_rsexecute_workflow,
    subtract_list_rsexecute_workflow,
    sum_invert_results_rsexecute,
    weight_list_rsexecute_workflow,
    zero_list_rsexecute_workflow,
)
from rascil.workflows.rsexecute.execution_support import get_dask_client, rsexecute
from ska_sdp_datamodels.calibration import (
    export_gaintable_to_hdf5,
    export_pointingtable_to_hdf5,
)
from ska_sdp_datamodels.configuration import (
    create_named_configuration,
    select_configuration,
)
from ska_sdp_datamodels.science_data_model.polarisation_model import PolarisationFrame
from ska_sdp_datamodels.sky_model import SkyComponent, export_skycomponent_to_hdf5
from ska_sdp_datamodels.visibility import create_visibility, export_visibility_to_hdf5
from ska_sdp_func_python.calibration import multiply_gaintables
from ska_sdp_func_python.imaging import advise_wide_field, create_image_from_visibility
from ska_sdp_func_python.visibility import convert_visibility_stokesI_to_polframe

from common import create_mid_simulation_components, get_vp_frequency, merge_hdf_to_ms
from common_rsexecute import predict_gaintable_components_list_rsexecute_workflow

log = logging.getLogger("ska-sim-mid-logger")
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))


def cli_parser():
    """Command line interface parser"""
    parser = argparse.ArgumentParser(
        description="Simulate SKA-MID direction dependent errors"
    )

    parser.add_argument(
        "--rmax",
        type=float,
        default=2e5,
        help="Maximum distance of dish from centre (m)",
    )

    parser.add_argument(
        "--image_sampling",
        type=float,
        default=3.0,
        help="Number of points per synthesized beam",
    )

    parser.add_argument(
        "--configuration",
        type=str,
        default="MID",
        help="MID Configuration: MID | MEERKAT+",
    )

    parser.add_argument(
        "--antennas",
        type=str,
        default=None,
        nargs="*",
        help="Antenna names to include (default is all)",
    )

    parser.add_argument(
        "--source",
        type=str,
        default="s3sky",
        help="Type of source: s3sky or point or double",
    )

    parser.add_argument(
        "--ra",
        type=float,
        default=0.0,
        help="Right ascension of phase centre (degrees)",
    )
    parser.add_argument(
        "--declination",
        type=float,
        default=-40.0,
        help="Declination of phase centre (degrees)",
    )

    parser.add_argument(
        "--band",
        type=str,
        default="B2",
        help="Band B1LOW | B1 | B2 | B5 | Ku",
    )

    parser.add_argument(
        "--nchan",
        type=int,
        default=1,
        help="Number of frequency channels",
    )

    parser.add_argument(
        "--channel_width",
        type=float,
        default=None,
        help="Channel bandwidth (Hz) (default is to calculate from band frequency)",
    )

    parser.add_argument(
        "--integration_time",
        type=float,
        default=180,
        help="Integration time (s)",
    )

    parser.add_argument(
        "--time_range",
        type=float,
        nargs=2,
        default=[-4.0, 4.0],
        help="Time range in hour angle",
    )

    parser.add_argument(
        "--time_step",
        type=float,
        default=None,
        help="Time step (unit: second of time)",
    )

    parser.add_argument(
        "--make_images",
        type=str,
        default="False",
        help="Make the images?",
    )

    parser.add_argument(
        "--only_actual",
        type=str,
        default="False",
        help="Only make the actual data?",
    )

    parser.add_argument(
        "--apply_pb",
        type=str,
        default="True",
        help="Apply the primary beam in modes troposphere and ionosphere?",
    )

    parser.add_argument(
        "--pbtype",
        type=str,
        default="MID_B2",
        help="Primary beam model: MID_B1 MID_B1LOW MID_B2 "
        "MID_Ku MEERKAT_B1 MEERKAT_B2 MEERKAT_Ku",
    )

    parser.add_argument(
        "--seed",
        type=int,
        default=18051955,
        help="Random number seed",
    )

    parser.add_argument(
        "--flux_limit",
        type=float,
        default=0.01,
        help="Lower flux limit (Jy)",
    )

    # Image parameters.
    parser.add_argument(
        "--npixel",
        type=int,
        default=512,
        help="Number of pixels in the resulting image",
    )

    parser.add_argument(
        "--cellsize",
        type=float,
        default=None,
        help="Cellsize in radians (will calculate if set to None)",
    )

    parser.add_argument(
        "--weighting",
        type=str,
        default="uniform",
        help="Type of weighting (natural, uniform or robust)",
    )

    parser.add_argument(
        "--robustness",
        type=float,
        default=0.0,
        help="Robustness for robust weighting",
    )

    # Control parameters.
    parser.add_argument(
        "--results",
        type=str,
        default="./",
        help="Directory for results",
    )

    parser.add_argument(
        "--elevation_sampling",
        type=float,
        default=1.0,
        help="Sampling in elevation for surface (deg)",
    )

    # Noniso parameters.
    parser.add_argument(
        "--r0",
        type=float,
        default=5e3,
        help="R0 (meters)",
    )

    parser.add_argument(
        "--height",
        type=float,
        default=3e5,
        help="Height of layer (meters)",
    )

    parser.add_argument(
        "--screen",
        type=str,
        default=rascil_data_path("models/test_mpc_screen.fits", check=False),
        help="Location of atmospheric phase screen data",
    )

    parser.add_argument(
        "--isoplanatic",
        type=str,
        default="False",
        help="Are the phase screens to be treated as isoplanatic?",
    )

    # Simulation parameters.
    parser.add_argument(
        "--time_chunk",
        type=float,
        default=3600.0,
        help="Time for a chunk (s)",
    )

    parser.add_argument(
        "--channel_start",
        type=int,
        default=None,
        help="First channel of interest if interested in "
        "some frequency range. Use with --nchan",
    )

    parser.add_argument(
        "--mode",
        type=str,
        default="none",
        help="Mode of simulation: wind_pointing|random_pointing"
        "|polarisation|ionosphere|troposphere|heterogeneous|surface",
    )

    parser.add_argument(
        "--duration",
        type=str,
        default="long",
        help="Type of duration: long or medium or short. Set to custom when \
              using options --integration_time and --time_range (or--time_step)",
    )

    parser.add_argument(
        "--wind_conditions",
        type=str,
        default="precision",
        help="SKA definition of wind conditions: precision, standard or degraded",
    )

    parser.add_argument(
        "--global_pe",
        type=float,
        nargs=2,
        default=[0.0, 0.0],
        help="Global pointing error (absolute values)",
    )

    parser.add_argument(
        "--static_pe",
        type=float,
        nargs=2,
        default=[0.0, 0.0],
        help="Multipliers for static errors",
    )

    parser.add_argument(
        "--dynamic_pe",
        type=float,
        default=1.0,
        help="Multiplier for dynamic errors",
    )

    parser.add_argument(
        "--pointing_directory",
        type=str,
        default=rascil_data_path("models", check=False),
        help="Location of wind PSD pointing files",
    )

    parser.add_argument(
        "--vp_directory",
        type=str,
        default=None,
        help="Location of voltage pattern files",
    )

    parser.add_argument(
        "--vp_support",
        type=int,
        default=None,
        help="Number of pixels in voltage pattern images",
    )

    parser.add_argument(
        "--use_dask",
        type=str,
        default="True",
        help="Use dask processing?",
    )

    parser.add_argument(
        "--write_gt",
        type=str,
        default="False",
        help="Write gaintable files as HDF",
    )

    parser.add_argument(
        "--write_pt",
        type=str,
        default="False",
        help="Write pointing table files as HDF",
    )

    parser.add_argument(
        "--imaging_dft_kernel",
        type=str,
        default=None,
        help="DFT kernel: cpu_looped | cpu_numba | gpu_raw ",
    )

    parser.add_argument(
        "--add_noise",
        type=str,
        default="False",
        help="Add noises to visibilities",
    )

    parser.add_argument(
        "--ra_s",
        type=float,
        default=-999.99,
        help="Right ascension of phase centre (degrees)",
    )

    parser.add_argument(
        "--dec_s",
        type=float,
        default=999.99,
        help="Declination of phase centre (degrees)",
    )

    parser.add_argument(
        "--vp_real_file",
        type=str,
        default=None,
        help="Voltage pattern real part file name",
    )

    parser.add_argument(
        "--vp_imag_file",
        type=str,
        default=None,
        help="Voltage pattern imagine part file name",
    )

    parser.add_argument(
        "--obs_time",
        type=str,
        default=None,
        help="Observation start time",
    )

    return parser


def make_images_workflow(dirty_name, args, image_pol, bvis_list, state):
    """
    Functions to make FITS images from Visibility

    :param dirty_name: Name of the dirty image
    :param args: Input list of arguments
    :param image_pol: Polarization of FITS image
    :param bvis_list: Input Visibility for imaging
    :param state: State of the image (nominal, difference or actual)

    :return bvis_list: modified Visibility list
    """
    if args.cellsize is None or args.npixel is None:
        advice = [
            rsexecute.execute(advise_wide_field)(
                bvis,
                verbose=False,
                oversampling_synthesised_beam=args.image_sampling,
            )
            for bvis in bvis_list
        ]
        advice = rsexecute.compute(advice, sync=True)

        if args.cellsize is None:
            args.cellsize = min(advice[0]["cellsize"], advice[-1]["cellsize"])
        if args.npixel is None:
            args.npixel = advice[0]["npixels23"]

    log.info("Using npixel %s and cellsize %f", args.npixel, args.cellsize)
    model_list = [
        rsexecute.execute(create_image_from_visibility, nout=1)(
            bvis,
            npixel=args.npixel,
            cellsize=args.cellsize,
            polarisation_frame=image_pol,
        )
        for bvis in bvis_list
    ]
    model_list = rsexecute.persist(model_list)

    bvis_list = weight_list_rsexecute_workflow(
        bvis_list,
        model_list,
        weighting=args.weighting,
        robustness=args.robustness,
    )
    bvis_list = rsexecute.persist(bvis_list)

    dirty_list = invert_list_rsexecute_workflow(
        bvis_list, template_model_imagelist=model_list, context="ng"
    )
    dirty_list = sum_invert_results_rsexecute(dirty_list)

    dirty, _ = rsexecute.compute(dirty_list, sync=True)

    log.info(dirty.image_acc.qa_image(context=state))
    log.info(f"mid_simulation: Writing dirty image {dirty_name}")

    dirty.image_acc.export_to_fits(dirty_name)

    return bvis_list


def write_gaintable(args, gt_list):
    """
    Write the list of gain tables to a single HDF file

    :param args: Input list of arguments
    :param gt_list: List of GainTables

    :return gt_list: same as input gt_list
    """
    nsources = len(gt_list[0])
    for source in range(nsources):
        chunks = [chunk[source] for chunk in gt_list]
        hdfname = (
            f"{args.results}/SKA_{args.configuration}_SIM_{args.duration}_{args.band}_"
            f"{args.mode}_nchan{args.nchan}_source{source}_gaintable.hdf"
        )
        all_chunks = xarray.concat(chunks, dim="time")
        qa = all_chunks.gaintable_acc.qa_gain_table()
        log.info(f"Source {source}: {qa}")
        export_gaintable_to_hdf5(all_chunks, hdfname)

    return gt_list


def simulation(args):
    """
    Main simulation function

    :param args: user-provided arguments

    :return:
        out_msname: MeasurementSet name where data was written to
        out_dirtyname: Dirty image name which was (optionally) created

    Note: return values refer to "actual" if --only_actual is True, else to "difference"
    """
    configuration_name = (
        args.configuration.split("/")[-1].replace("-layout.json", "").upper()
    )
    logfile = (
        f"{args.results}/SKA_{configuration_name}_SIM_{args.duration}_{args.band}_"
        f"{args.mode}_nchan{args.nchan}.log"
    )

    def init_logging():
        logging.basicConfig(
            filename=logfile,
            filemode="a",
            format="%(asctime)s.%(msecs)d %(name)s %(levelname)s %(message)s",
            datefmt="%H:%M:%S",
            level=logging.INFO,
        )

    init_logging()
    log = logging.getLogger("ska-sim-mid-logger")

    log.info("Starting simulation of %s", args.mode)
    log.info(pprint.pformat(vars(args)))

    if args.use_dask == "True":
        client = get_dask_client()
        rsexecute.set_client(use_dask=True, client=client)
        dask.config.set({"distributed.nanny.environ.MALLOC_TRIM_THRESHOLD_": 0})
    else:
        rsexecute.set_client(use_dask=False)

    rsexecute.run(init_logging)
    rsexecute.init_statistics()

    # Set up details of simulated observation.
    if args.duration == "short":
        integration_time = 10.0
        time_range = [-180.0 / 3600.0, 180.0 / 3600.0]
        time_chunk = 360.0
    elif args.duration == "medium":
        integration_time = 10.0
        time_range = [-0.5, 0.5]
        time_chunk = 100.0
    elif args.duration == "long":
        integration_time = 60.0
        time_range = [-4.0, 4.0]
        if args.mode == "surface":
            time_chunk = integration_time
        else:
            time_chunk = 1800.0
    else:
        args.duration = "custom"
        integration_time = args.integration_time
        time_range = args.time_range
        if args.mode == "surface":
            time_chunk = integration_time
        else:
            time_chunk = args.time_chunk
    image_polarisation_frame = PolarisationFrame("stokesIQUV")
    sim_polarisation_frame = PolarisationFrame("linear")
    vis_polarisation_frame = PolarisationFrame("linear")

    log.info("Image polarisation: %s", str(image_polarisation_frame))
    log.info("Simulations polarisation: %s", str(sim_polarisation_frame))
    log.info("Vis polarisation: %s", str(vis_polarisation_frame))

    log.info(
        "Simulating %s observation: %s hours with integrations of %s s",
        args.duration,
        time_range,
        integration_time,
    )
    log.info("Simulations processed in chunks of %.1f seconds", time_chunk)

    if args.vp_support is None:
        vp_support = None
    else:
        vp_support = (args.vp_support, args.vp_support)

    # First get the full frequency coverage and then
    # extract those of interest
    channel_width = args.channel_width
    if args.band == "B1":
        if args.channel_width is None:
            channel_width = 5.0e7 / args.nchan
        frequency = (
            0.7650e9 + (numpy.arange(args.nchan) - args.nchan // 2) * channel_width
        )
    elif args.band == "B1LOW":
        if args.channel_width is None:
            channel_width = 5.0e7 / args.nchan
        frequency = (
            0.390e9 + (numpy.arange(args.nchan) - args.nchan // 2) * channel_width
        )
    elif args.band == "B2":
        if args.channel_width is None:
            channel_width = 1.2e8 / args.nchan
        frequency = (
            1.36e9 + (numpy.arange(args.nchan) - args.nchan // 2) * channel_width
        )
    elif args.band == "B2G":
        # B2G: 0.95 - 1.76 GHz
        if args.channel_width is None:
            channel_width = 8e8 / args.nchan
        frequency = (
            1.36e9 + (numpy.arange(args.nchan) - args.nchan // 2) * channel_width
        )
    elif args.band == "B5BG":
        # B5b: 8.3 - 15.4 GHz
        if args.channel_width is None:
            channel_width = 7e9 / args.nchan
        frequency = (
            11.8e9 + (numpy.arange(args.nchan) - args.nchan // 2) * channel_width
        )
    elif args.band == "B5G":
        # B5a: 4.6 - 8.5 GHz
        if args.channel_width is None:
            channel_width = 3.9e9 / args.nchan
        frequency = (
            6.55e9 + (numpy.arange(args.nchan) - args.nchan // 2) * channel_width
        )
    elif args.band == "Ku":
        if args.channel_width is None:
            channel_width = 5e8 / args.nchan
        frequency = (
            12.0e9 + (numpy.arange(args.nchan) - args.nchan // 2) * channel_width
        )
    else:
        raise ValueError(f"Unknown band {args.band}")

    # Now extract the frequencies of interest from the full range of
    # frequencies available
    if args.channel_start:
        frequency = args.channel_start + numpy.arange(args.nchan) * channel_width

    log.info(
        "Frequency coverage %f to %f GHz",
        frequency[0] / 1e9,
        frequency[-1] / 1e9,
    )

    frequency = numpy.array(frequency)
    channel_bandwidth = numpy.repeat(channel_width, len(frequency))

    phasecentre = SkyCoord(
        ra=args.ra * u.deg,
        dec=args.declination * u.deg,
        frame="icrs",
        equinox="J2000",
    )

    if args.configuration is not None:
        mid = create_named_configuration(args.configuration, rmax=args.rmax)
        if len(args.configuration.split("/")) > 1:
            args.configuration = configuration_name
    else:
        mid = create_named_configuration("MID", rmax=args.rmax)
    if args.antennas is not None:
        mid = select_configuration(mid, args.antennas)

    # pylint:disable=fixme
    # TODO:
    # if time_range is not divisible by time_chunk
    # This line will return a array of single element.
    # May be a bug, need to be revisited
    if args.time_step is None:
        times = numpy.arange(time_range[0] * 3600, time_range[1] * 3600, time_chunk)
    else:
        times = numpy.arange(time_range[0] * 3600, time_range[1] * 3600, args.time_step)
    log.info("Visibility data divided into %d partitions in time", len(times))
    log.info("Starting times offset from transit by %s (seconds)", times)

    bvis_list = create_bvis_list(
        channel_bandwidth,
        frequency,
        integration_time,
        mid,
        phasecentre,
        sim_polarisation_frame,
        time_chunk,
        times,
        obs_time=args.obs_time,
    )

    ra_s = args.ra_s
    dec_s = args.dec_s

    if abs(ra_s - 999.99) < 0.001 or abs(dec_s - 999.99) < 0.001:
        ra_s = args.ra
        dec_s = args.declination

    phasecentre_s = SkyCoord(
        ra=ra_s * u.deg, dec=dec_s * u.deg, frame="icrs", equinox="J2000"
    )

    original_components = calculate_components(
        args,
        frequency,
        image_polarisation_frame,
        phasecentre_s,
        vp_support,
        log,
    )

    # End of setup, now we generate the gaintables with and without errors.
    (
        actual_gt_list,
        actual_pt_list,
        nominal_gt_list,
        nominal_pt_list,
    ) = calculate_gaintable_lists(
        args,
        bvis_list,
        original_components,
        vp_support,
        log,
    )

    if args.write_pt:
        if actual_pt_list is not None:
            log.info("Writing out pointing tables.")
            write_pointingtables(
                args,
                actual_pt_list,
                nominal_pt_list,
            )
        else:
            log.info("Empty pointing table list, check your pointing error inputs.")

    # Now generate skymodels using the same components but
    # different gaintable. Note that there is a
    # different gaintable and skymodel for each blockvis.

    if actual_gt_list is None:
        raise ValueError(f"Mode is incorrect or not specified: {args.mode}")

    # Predict_gaintable_list_rsexecute_workflow calculates the Vis for each of a list of
    # SkyComponents. We want to add these across SkyComponents and then concatenate Vis.
    actual_bvis_list, out_dirtyname, out_msname = calculate_actual(
        args,
        bvis_list,
        image_polarisation_frame,
        actual_gt_list,
        original_components,
        vis_polarisation_frame,
    )

    if args.only_actual == "False":
        # Now we deal with the simulations of nominal (as opposed to actual) data.
        nominal_bvis_list = zero_list_rsexecute_workflow(bvis_list)
        nominal_bvis_list = rsexecute.persist(nominal_bvis_list)

        out_dirtyname, out_msname = calculate_nominal_and_difference(
            nominal_gt_list,
            args,
            image_polarisation_frame,
            nominal_bvis_list,
            actual_bvis_list,
            original_components,
            vis_polarisation_frame,
        )

    rsexecute.save_statistics(
        f"{args.results}/SKA_{args.configuration}_SIM_"
        f"{args.duration}_{args.band}_{args.mode}_nchan{args.nchan}"
    )
    rsexecute.close()

    return out_msname, out_dirtyname


def calculate_actual(
    args,
    bvis_list,
    image_polarisation_frame,
    actual_gt_list,
    original_components,
    vis_polarisation_frame,
):
    """
    Calculate the "actual" (i.e. containing observation errors)
    data and save them to HDF files. Optionally, make an image.

    :param args: user-provided arguments
    :param bvis_list: list of initial Visibilities
    :param image_polarisation_frame: PolarisationFrame of image to be created
    :param actual_gt_list: list of GainTables
    :param original_components: list of SkyComponents (compact sources)
    :param vis_polarisation_frame: Visibility PolarisationFrame

    :return:
        actual_bvis_list: list of Visibilities with actual data
        out_dirtyname: Dirty image name (if generated)
        out_msname: MeasurementSet name containing exported data
    """
    if args.write_gt == "True":
        log.info("Writing out gain tables.")
        actual_gt_list = rsexecute.execute(write_gaintable)(args, actual_gt_list)

    actual_bvis_list = [
        predict_gaintable_components_list_rsexecute_workflow(
            bvis,
            actual_gt_list[ibvis],
            original_components,
            docal=True,
            dft_compute_kernel=args.imaging_dft_kernel,
        )
        for ibvis, bvis in enumerate(bvis_list)
    ]
    actual_bvis_list = rsexecute.persist(actual_bvis_list)

    if args.add_noise == "True":
        actual_bvis_list = rsexecute.persist(actual_bvis_list)
        actual_bvis_list = [
            rsexecute.execute(addnoise_visibility)(bvis, seed=1234)
            for bvis in actual_bvis_list
        ]

    actual_bvis_list, out_dirtyname, out_msname = export_results(
        args,
        image_polarisation_frame,
        actual_bvis_list,
        vis_polarisation_frame,
        "actual",
    )
    actual_bvis_list = rsexecute.persist(actual_bvis_list)
    return actual_bvis_list, out_dirtyname, out_msname


def change_vis_time(vis, obs_time):
    """
    Replace time relevant fields in the visibility

    :param vis: Visibility
    :param obs_time: Start time (ISO Format)
    :return: Visibility after data changed
    """
    start_time = Time(obs_time).mjd
    change_time = []
    change_datetime = []
    vis_time = vis["time"].values
    vis_time = vis_time.tolist()
    time_start = vis_time[0]
    for time in vis_time:
        change_time.append(start_time * 86400 + (time - time_start))
        change_datetime.append(
            Time(
                start_time + (time - time_start) / 86400,
                format="mjd",
                scale="utc",
            ).to_datetime()
        )
    vis["time"] = numpy.array(change_time)
    vis["datetime"].data = numpy.array(
        change_datetime,
        dtype="datetime64[ns]",
    )

    return vis


def create_bvis_list(
    channel_bandwidth,
    frequency,
    integration_time,
    mid,
    phasecentre,
    sim_polarisation_frame,
    time_chunk,
    times,
    obs_time=None,
):
    """
    Create the list (or graph) for making blockvis

    :param channel_bandwidth: channel bandwidth [nchan]
    :param frequency: frequencies [nchan]
    :param integration_time: integration time, int
    :param mid: Mid Configuration
    :param phasecentre: Phase Centre (astropy SkyCoord)
    :param sim_polarisation_frame: PolarisationFrame of simulation
    :param time_chunk: length of a time chunk in seconds
    :param times: array of times to simulate
    :param obs_time: Observational time (For Pointing offset simulation)

    :return: Visibility list
    """
    bvis_list = []
    for _, start_time in enumerate(times):
        sub_times = numpy.arange(start_time, start_time + time_chunk, integration_time)
        sub_times *= numpy.pi / 43200.0
        bvis = rsexecute.execute(create_visibility, nout=1)(
            mid,
            sub_times,
            frequency=frequency,
            channel_bandwidth=channel_bandwidth,
            weight=1.0,
            phasecentre=phasecentre,
            polarisation_frame=sim_polarisation_frame,
        )
        if obs_time is not None:
            bvis = rsexecute.execute(change_vis_time)(bvis, obs_time)
        bvis_list.append(bvis)
    bvis_list = rsexecute.persist(bvis_list)
    return bvis_list


def calculate_nominal_and_difference(
    nominal_gt_list,
    args,
    image_polarisation_frame,
    nominal_bvis_list,
    actual_bvis_list,
    original_components,
    vis_polarisation_frame,
):
    """
    Calculate the "nominal" (i.e. data without any observation errors)
    data and the difference between nominal and actual data;
    and save these to HDF files. Optionally, make images.

    :param nominal_gt_list: GainTable list for nominal values
    :param args: user-provided arguments
    :param image_polarisation_frame: PolarisationFrame of image to be created
    :param nominal_bvis_list: list of nominal Visibilities
    :param actual_bvis_list: list of actual Visibilities
    :param original_components: list of SkyComponents (compact sources)
    :param vis_polarisation_frame: Visibility PolarisationFrame
    """

    nominal_bvis_list = [
        predict_gaintable_components_list_rsexecute_workflow(
            bvis,
            nominal_gt_list[ibvis],
            original_components,
            docal=False,
            dft_compute_kernel=args.imaging_dft_kernel,
        )
        for ibvis, bvis in enumerate(nominal_bvis_list)
    ]

    if args.add_noise == "True":
        nominal_bvis_list = [
            rsexecute.execute(addnoise_visibility)(bvis, seed=1234)
            for bvis in nominal_bvis_list
        ]

    (
        nominal_bvis_list,
        _,
        _,
    ) = export_results(
        args,
        image_polarisation_frame,
        nominal_bvis_list,
        vis_polarisation_frame,
        "nominal",
    )

    difference_bvis_list = subtract_list_rsexecute_workflow(
        actual_bvis_list, nominal_bvis_list
    )

    difference_bvis_list, out_dirtyname_diff, out_msname_diff = export_results(
        args,
        image_polarisation_frame,
        difference_bvis_list,
        vis_polarisation_frame,
        "difference",
    )

    return out_dirtyname_diff, out_msname_diff


def write_pointingtables(args, actual_pt_list, nominal_pt_list):
    """
    Save pointing tables (both nominal and actual) to HDF5 files.
    """
    nominal_pt_list = rsexecute.compute(nominal_pt_list, sync=True)
    actual_pt_list = rsexecute.compute(actual_pt_list, sync=True)

    for pt_type, pt_list in zip(
        ["nominal", "actual"], [nominal_pt_list, actual_pt_list]
    ):
        pt_hdf_name = (
            f"{args.results}/SKA_{args.configuration}_SIM_{args.duration}_{args.band}_"
            f"{args.mode}_nchan{args.nchan}_pt_{pt_type}.hdf"
        )
        export_pointingtable_to_hdf5(pt_list, pt_hdf_name)


def calculate_components(
    args,
    frequency,
    image_polarisation_frame,
    phasecentre,
    vp_support,
    log,
):
    """
    Generate a list of SkyComponents (compact source models) used for the simulation.
    Options: s3sky (all-sky), point (source), double (source)

    :param args: user-provided arguments
    :param frequency: frequencies of the simulation [nchan]
    :param image_polarisation_frame: Polarisation Frame of image
    :param phasecentre: PhaseCenter of observation
    :param vp_support: Support in pixels e.g. 256 means 256 x 256 pixel centered on peak
    :param log: logging object
    """
    fov_deg = 4.0 * 1.36e9 / frequency[0]
    pb_npixel = 512
    d2r = numpy.pi / 180.0
    pb_cellsize = d2r * fov_deg / pb_npixel
    pbradius = pb_cellsize * pb_npixel

    # We need different components for each frequency but not for each time.
    # We compute this immediately since we need to know the sizes of the
    # component lists below. Note that the component lists may be
    # of different length.
    comp_hdf_name = (
        f"{args.results}/SKA_{args.configuration}_SIM_{args.duration}_{args.band}_"
        f"{args.mode}_nchan{args.nchan}_pbcomponents.hdf"
    )

    if args.source == "s3sky":
        pb_components = create_mid_simulation_components(
            phasecentre,
            frequency,
            args.flux_limit,
            pbradius,
            pb_npixel,
            pb_cellsize,
            args.vp_directory,
            vp_support=vp_support,
            pb_type=args.pbtype,
            fov=10,
            polarisation_frame=image_polarisation_frame,
            flux_max=10.0,
            apply_pb=True,
            vp_real_file=args.vp_real_file,
            vp_imag_file=args.vp_imag_file,
        )
        log.info(
            "Found %d components above flux %f",
            len(pb_components),
            args.flux_limit,
        )
        export_skycomponent_to_hdf5(pb_components, comp_hdf_name)
        return pb_components

    if args.source in ("double", "point"):
        if image_polarisation_frame.npol == 1:
            flux = numpy.ones([len(frequency), image_polarisation_frame.npol])
        else:
            flux = numpy.zeros([len(frequency), image_polarisation_frame.npol])
            flux[:, 0] = 1.0

        if args.source == "point":
            original_components = [
                SkyComponent(
                    direction=phasecentre,
                    flux=flux,
                    polarisation_frame=image_polarisation_frame,
                    frequency=frequency,
                )
            ]
        else:
            # Double off-axis: put two sources at (5,4) (-4, 5) degrees wrt centre.
            source1 = SkyCoord(
                ra=(args.ra + 5.0) * u.deg,
                dec=(args.declination + 4.0) * u.deg,
                frame="icrs",
                equinox="J2000",
            )
            source2 = SkyCoord(
                ra=(args.ra - 4.0) * u.deg,
                dec=(args.declination - 5.0) * u.deg,
                frame="icrs",
                equinox="J2000",
            )
            original_components = [
                SkyComponent(
                    direction=source1,
                    flux=flux * 2.0,
                    polarisation_frame=image_polarisation_frame,
                    frequency=frequency,
                ),
                SkyComponent(
                    direction=source2,
                    flux=flux,
                    polarisation_frame=image_polarisation_frame,
                    frequency=frequency,
                ),
            ]
        export_skycomponent_to_hdf5(original_components, comp_hdf_name)
        return original_components

    raise ValueError(f"Source model {args.source} is not supported.")


def calculate_gaintable_lists(
    args,
    bvis_list,
    original_components,
    vp_support,
    log,
):
    """
    Function, which determines what mode to use
    to generate gain table lists, based on user input arguments.
    """
    nominal_gt_list = None
    actual_gt_list = None
    nominal_pt_list = None
    actual_pt_list = None
    # Phase errors across the field of view due to the ionosphere.
    if args.mode == "ionosphere":
        actual_gt_list, nominal_gt_list = _sim_ionosphere(
            args,
            bvis_list,
            original_components,
            vp_support,
            log,
            args.apply_pb == "True",
        )
    # Phase errors across the field of view due to the troposphere.
    if args.mode == "troposphere":
        actual_gt_list, nominal_gt_list = _sim_troposphere(
            args,
            bvis_list,
            original_components,
            vp_support,
            log,
            args.apply_pb == "True",
        )
    # Random wind induced pointing errors.
    if args.mode == "random_pointing":
        (
            actual_gt_list,
            nominal_gt_list,
            nominal_pt_list,
            actual_pt_list,
        ) = _sim_random_pointing(
            args,
            bvis_list,
            original_components,
            vp_support,
            log,
        )
    # Wind induced pointing errors derived from modelled dish PSDs.
    if args.mode == "wind_pointing":
        (
            actual_gt_list,
            nominal_gt_list,
            nominal_pt_list,
            actual_pt_list,
        ) = _sim_wind_pointing(
            args,
            bvis_list,
            original_components,
            vp_support,
            log,
        )
    # Gravity induced sag of the dish surface.
    if args.mode == "surface":
        actual_gt_list, nominal_gt_list = _sim_surface(
            args.band,
            args.vp_directory,
            args.elevation_sampling,
            bvis_list,
            original_components,
            log,
            args,
        )
    # Take into account different voltage patterns for SKA and MeerKAT dishes.
    if args.mode == "heterogeneous":
        actual_gt_list, nominal_gt_list = _sim_heterogeneous(
            args.band,
            bvis_list,
            original_components,
            args.vp_directory,
            vp_support,
            log,
        )
    # Effects of polarisation leakage, compared to no leakage.
    if args.mode == "polarisation":
        actual_gt_list, nominal_gt_list = _sim_polarisation(
            args.band,
            bvis_list,
            original_components,
            args.vp_directory,
            vp_support,
            log,
            args,
        )

    if nominal_gt_list is None:
        raise ValueError("Nominal gaintable list is empty")

    if actual_gt_list is None:
        raise ValueError("Actual gaintable list is empty")

    return actual_gt_list, actual_pt_list, nominal_gt_list, nominal_pt_list


def export_results(
    args, image_polarisation_frame, bvis_list, vis_polarisation_frame, state
):
    """
    Export the dirty images (if needed) and MS

    :param args: user-provided arguments
    :param image_polarisation_frame: PolarisationFrame of image to be generated
    :param bvis_list: list of Visibilities to be exported
    :param vis_polarisation_frame: PolarisationFrame of Visibilities
    :param state: nominal, actual, or difference

    :return:
        bvis_list: exported BVis list
        out_dirtyname: name of dirty image (if generated)
        out_msname: MeasurementSet name containing exported data
    """
    # Convert to required polarisation.
    bvis_list = [
        rsexecute.execute(convert_visibility_stokesI_to_polframe)(
            bvis, vis_polarisation_frame
        )
        for bvis in bvis_list
    ]
    bvis_list = rsexecute.persist(bvis_list)

    out_dirtyname = (
        f"{args.results}/SKA_{args.configuration}_SIM_{args.duration}_{args.band}_"
        f"{args.mode}_nchan{args.nchan}_{state}_dirty.hdf"
    )

    if args.make_images == "True":
        bvis_list = make_images_workflow(
            out_dirtyname,
            args,
            image_polarisation_frame,
            bvis_list,
            state,
        )
        bvis_list = rsexecute.persist(bvis_list)

    # Do the concatenate and write in the cluster.
    def write_hdf(bvis, i):
        out_hdfname = (
            f"{args.results}/SKA_{args.configuration}_SIM_{args.duration}_{args.band}_"
            f"{args.mode}_nchan{args.nchan}_{state}_{i}.hdf"
        )
        export_visibility_to_hdf5(bvis, out_hdfname)
        return out_hdfname

    hdfnames_graph = [
        rsexecute.execute(write_hdf)(bvis, ibvis)
        for ibvis, bvis in enumerate(bvis_list)
    ]
    hdfnames = rsexecute.compute(hdfnames_graph, sync=True)
    out_msname = merge_hdf_to_ms(hdfnames, vis_polarisation_frame)

    return bvis_list, out_dirtyname, out_msname


def _sim_polarisation(
    band,
    bvis_list,
    original_components,
    vp_directory,
    vp_support,
    log,
    args,
):
    """
    Generate gain table lists from polarisation leakage.
    """
    log.info("Polarisation leakage modeling")

    # Polarised beams.
    (
        nominal_gt_list,
        actual_gt_list,
    ) = create_polarisation_gaintable_rsexecute_workflow(
        band,
        bvis_list,
        original_components,
        get_vp=partial(
            get_vp_frequency,
            fixpol=False,
            vp_directory=vp_directory,
            vp_support=vp_support,
            vp_real_file=args.vp_real_file,
            vp_imag_file=args.vp_imag_file,
        ),
    )
    return actual_gt_list, nominal_gt_list


def _sim_heterogeneous(
    band,
    bvis_list,
    original_components,
    vp_directory,
    vp_support,
    log,
):
    """
    Generate gain table lists from different
    voltage patterns for SKA and MeerKAT antennas.
    """
    log.info("Heterogeneous array modeling")

    # Different antennas.
    (
        nominal_gt_list,
        actual_gt_list,
    ) = create_heterogeneous_gaintable_rsexecute_workflow(
        band,
        bvis_list,
        original_components,
        get_vp=partial(
            get_vp_frequency,
            fixpol=False,
            vp_directory=vp_directory,
            vp_support=vp_support,
        ),
        default_vp="MID",
    )
    return actual_gt_list, nominal_gt_list


def _sim_surface(
    band,
    vp_directory,
    elevation_sampling,
    bvis_list,
    original_components,
    log,
):
    """
    Generate gain table lists from gravity induced surface errors.
    """
    log.info("Surface deformation modeling")

    # Dish surface sag due to gravity.
    (
        nominal_gt_list,
        actual_gt_list,
    ) = create_surface_errors_gaintable_rsexecute_workflow(
        band,
        bvis_list,
        original_components,
        vp_directory=vp_directory,
        elevation_sampling=elevation_sampling,
    )
    return actual_gt_list, nominal_gt_list


def _sim_wind_pointing(
    args,
    bvis_list,
    original_components,
    vp_support,
    log,
):
    """
    Generate gain table lists from wind-induced pointing errors.
    """
    log.info("Wind pointing modeling")

    # Wind-induced pointing errors.
    vp_list = [
        rsexecute.execute(get_vp_frequency)(
            telescope=args.pbtype,
            vp_directory=args.vp_directory,
            vp_support=vp_support,
            vp_real_file=args.vp_real_file,
            vp_imag_file=args.vp_imag_file,
        )
        for i in range(len(bvis_list))
    ]
    vp_list = rsexecute.persist(vp_list)
    (
        nominal_gt_list,
        actual_gt_list,
        nominal_pt_list,
        actual_pt_list,
    ) = create_pointing_errors_gaintable_rsexecute_workflow(
        bvis_list,
        original_components,
        sub_vp_list=vp_list,
        time_series="wind",
        time_series_type=args.wind_conditions,
        seed=args.seed,
        pointing_directory=args.pointing_directory,
    )
    return actual_gt_list, nominal_gt_list, nominal_pt_list, actual_pt_list


def _sim_random_pointing(
    args,
    bvis_list,
    original_components,
    vp_support,
    log,
):
    """
    Generate gain table lists from random pointing errors.
    """
    log.info("Random pointing modeling")

    a2r = numpy.pi / (3600.0 * 180)
    vp_list = [
        rsexecute.execute(get_vp_frequency)(
            telescope=args.pbtype,
            vp_directory=args.vp_directory,
            vp_support=vp_support,
            vp_real_file=args.vp_real_file,
            vp_imag_file=args.vp_imag_file,
        )
        for i in range(len(bvis_list))
    ]
    vp_list = rsexecute.persist(vp_list)
    (
        nominal_gt_list,
        actual_gt_list,
        nominal_pt_list,
        actual_pt_list,
    ) = create_pointing_errors_gaintable_rsexecute_workflow(
        bvis_list,
        original_components,
        sub_vp_list=vp_list,
        pointing_error=a2r * args.dynamic_pe,
        static_pointing_error=a2r * numpy.array(args.static_pe),
        global_pointing_error=a2r * numpy.array(args.global_pe),
        seed=args.seed,
    )
    return actual_gt_list, nominal_gt_list, nominal_pt_list, actual_pt_list


def _sim_troposphere(
    args,
    bvis_list,
    original_components,
    vp_support,
    log,
    apply_beam=False,
):
    """
    Generate gain table lists from phase errors across
    the field of view due to the troposphere.
    """
    log.info("Troposphere modeling")

    (
        nominal_gt_list,
        actual_gt_list,
    ) = create_atmospheric_errors_gaintable_rsexecute_workflow(
        bvis_list,
        original_components,
        r0=args.r0,
        screen=args.screen,
        height=args.height,
        type_atmosphere=args.mode,
        reference=True,
        jones_type="B",
    )
    actual_gt_list, nominal_gt_list = merge_atmosphere_gt_lists(
        actual_gt_list,
        apply_beam,
        args,
        bvis_list,
        nominal_gt_list,
        original_components,
        vp_support,
    )

    return actual_gt_list, nominal_gt_list


def merge_atmosphere_gt_lists(
    actual_gt_list,
    apply_beam,
    args,
    bvis_list,
    nominal_gt_list,
    original_components,
    vp_support,
):
    """Merge the atmospheric gaintables with the beam gaintables

    :param actual_gt_list: Gaintable with atmosphere included (or graph)
    :param args: CLI arguments
    :param bvis_list: List of Visibilities (or graph)
    :param nominal_gt_list: Nominal gaintable (or graph)
    :param original_components: The components (or graph)
    :param vp_directory: Directory for the voltage patterns
    :param vp_support: Support in pixels e.g. 256 means 256 x 256 pixel centered on peak
    :return:
    """
    # We need to include the voltage pattern since the atmospheric modelling does not
    # include it.
    if apply_beam:
        beam_gt_list, _ = create_polarisation_gaintable_rsexecute_workflow(
            args.band,
            bvis_list,
            original_components,
            get_vp=partial(
                get_vp_frequency,
                fixpol=False,
                vp_directory=args.vp_directory,
                vp_support=vp_support,
                vp_real_file=args.vp_real_file,
                vp_imag_file=args.vp_imag_file,
            ),
        )
        nominal_gt_list = [
            [
                rsexecute.execute(multiply_gaintables)(
                    nominal_gt_list[ibvis][icomp], beam_gt_list[ibvis][icomp]
                )
                for icomp, _ in enumerate(original_components)
            ]
            for ibvis, _ in enumerate(bvis_list)
        ]

        if args.isoplanatic == "True":
            # Copy the gaintable for component 0 to all the others.
            new_actual_gt_list = [
                [
                    rsexecute.execute(actual_gt_list[ibvis][0].copy)(deep=True)
                    for icomp, _ in enumerate(original_components)
                ]
                for ibvis, _ in enumerate(bvis_list)
            ]

            actual_gt_list = [
                [
                    rsexecute.execute(multiply_gaintables)(
                        new_actual_gt_list[ibvis][icomp],
                        beam_gt_list[ibvis][icomp],
                    )
                    for icomp, _ in enumerate(original_components)
                ]
                for ibvis, _ in enumerate(bvis_list)
            ]
        else:
            actual_gt_list = [
                [
                    rsexecute.execute(multiply_gaintables)(
                        actual_gt_list[ibvis][icomp],
                        beam_gt_list[ibvis][icomp],
                    )
                    for icomp, _ in enumerate(original_components)
                ]
                for ibvis, _ in enumerate(bvis_list)
            ]

    return actual_gt_list, nominal_gt_list


def _sim_ionosphere(
    args,
    bvis_list,
    original_components,
    vp_support,
    log,
    apply_beam=False,
):
    """
    Generate gain table lists from phase errors across
    the field of view due to the ionosphere.
    """
    log.info("Ionosphere modeling")

    (
        nominal_gt_list,
        actual_gt_list,
    ) = create_atmospheric_errors_gaintable_rsexecute_workflow(
        bvis_list,
        original_components,
        r0=args.r0,
        screen=args.screen,
        height=args.height,
        type_atmosphere=args.mode,
        reference=True,
        jones_type="B",
    )
    actual_gt_list, nominal_gt_list = merge_atmosphere_gt_lists(
        actual_gt_list,
        apply_beam,
        args,
        bvis_list,
        nominal_gt_list,
        original_components,
        vp_support,
    )

    return actual_gt_list, nominal_gt_list


def main():
    """Get command line inputs."""
    parser = cli_parser()
    args = parser.parse_args()
    simulation(args)


if __name__ == "__main__":
    main()
