"""
This checks the frequency and elevation interpolations.
It's not meant for general use.
"""

import logging
import sys

log = logging.getLogger()
log.setLevel(logging.INFO)
log.addHandler(logging.StreamHandler(sys.stdout))

from src.common import get_vp_elevation, get_vp_frequency


def main():
    """Check frequency and elevation interpolations."""
    print("Checking frequency interpolation")

    directory = "../resources/beam_models"
    for telescope in ["MEERKAT_B2", "MID_B2"]:
        try:
            print(telescope)
            _ = get_vp_frequency(telescope, directory, fixpol=False)
        except ValueError as err:
            print(err)

    print("Checking elevation interpolation")

    for telescope in ["MID_B1", "MID_B2", "MID_Ku"]:
        try:
            print(telescope)
            _ = get_vp_elevation(telescope, directory, el=30.0, fixpol=False)
        except ValueError as err:
            print(err)


if __name__ == "__main__":
    main()
