"""
This makes a randomly selected subset of MID.
It's not meant for general use.
"""

import matplotlib.pyplot as plt
import numpy
from numpy.random import default_rng
from rascil.processing_components import plot_configuration
from ska_sdp_datamodels.configuration import (
    create_named_configuration,
    select_configuration,
)


def main():
    """Selecte subset of MID configuration"""
    rng = default_rng(1805550721)

    conf = create_named_configuration("MID", rmax=1e4)

    names = numpy.array(  # noqa: F841 pylint: disable=unused-variable
        [
            "M000",
            "M001",
            "M002",
            "M003",
            "M004",
            "M005",
            "M006",
            "M007",
            "M008",
            "M009",
            "M010",
            "M011",
            "M012",
            "M013",
            "M014",
            "M015",
            "M016",
            "M017",
            "M018",
            "M019",
            "M020",
            "M021",
            "M022",
            "M023",
            "M024",
            "M025",
            "M026",
            "M027",
            "M028",
            "M029",
            "M030",
            "M031",
            "M032",
            "M033",
            "M034",
            "M035",
            "M036",
            "M037",
            "M038",
            "M039",
            "M040",
            "M041",
            "M042",
            "M043",
            "M044",
            "M045",
            "M046",
            "M047",
            "M048",
            "M049",
            "M050",
            "M051",
            "M052",
            "M053",
            "M054",
            "M055",
            "M056",
            "M057",
            "M058",
            "M059",
            "M060",
            "M061",
            "M062",
            "M063",
            "SKA001",
            "SKA002",
            "SKA003",
            "SKA004",
            "SKA005",
            "SKA006",
            "SKA007",
            "SKA008",
            "SKA009",
            "SKA010",
            "SKA011",
            "SKA012",
            "SKA013",
            "SKA014",
            "SKA015",
            "SKA016",
            "SKA017",
            "SKA018",
            "SKA019",
            "SKA020",
            "SKA021",
            "SKA022",
            "SKA023",
            "SKA024",
            "SKA025",
            "SKA026",
            "SKA027",
            "SKA028",
            "SKA029",
            "SKA030",
            "SKA031",
            "SKA032",
            "SKA033",
            "SKA034",
            "SKA035",
            "SKA036",
            "SKA037",
            "SKA038",
            "SKA039",
            "SKA040",
            "SKA041",
            "SKA042",
            "SKA043",
            "SKA044",
            "SKA045",
            "SKA046",
            "SKA047",
            "SKA048",
            "SKA049",
            "SKA050",
            "SKA051",
            "SKA052",
            "SKA053",
            "SKA054",
            "SKA055",
            "SKA056",
            "SKA057",
            "SKA058",
            "SKA059",
            "SKA060",
            "SKA061",
            "SKA062",
            "SKA063",
            "SKA064",
            "SKA065",
            "SKA066",
            "SKA067",
            "SKA068",
            "SKA069",
            "SKA070",
            "SKA071",
            "SKA072",
            "SKA073",
            "SKA074",
            "SKA075",
            "SKA076",
            "SKA077",
            "SKA078",
            "SKA079",
            "SKA080",
            "SKA081",
            "SKA082",
            "SKA083",
            "SKA084",
            "SKA085",
            "SKA086",
            "SKA087",
            "SKA088",
            "SKA089",
            "SKA090",
            "SKA091",
            "SKA092",
            "SKA093",
            "SKA094",
            "SKA095",
            "SKA096",
            "SKA097",
            "SKA098",
            "SKA099",
            "SKA100",
            "SKA101",
            "SKA102",
            "SKA103",
            "SKA104",
            "SKA105",
            "SKA106",
            "SKA107",
            "SKA108",
            "SKA109",
            "SKA110",
            "SKA111",
            "SKA112",
            "SKA113",
            "SKA114",
            "SKA115",
            "SKA116",
            "SKA117",
            "SKA118",
            "SKA119",
            "SKA120",
            "SKA121",
            "SKA122",
            "SKA123",
            "SKA124",
            "SKA125",
            "SKA126",
            "SKA127",
            "SKA128",
            "SKA129",
            "SKA130",
            "SKA131",
            "SKA132",
            "SKA133",
        ]
    )

    yes = rng.uniform(0.0, 1.0, len(conf.names)) > 0.5
    print(yes)
    selected = conf.names[yes]
    conf = create_named_configuration("MID", rmax=1e4)
    conf = select_configuration(conf, selected)
    print(conf.names)
    plot_configuration(conf, label=True)
    print(conf.names)
    plt.show()

    # Select half of all antennas < 10km and all further out
    #   --antennas  M000 M001 M007 M008 M009 M010 M011 M012  \
    #         M014 M016 M017 M018 M019 M022 M024 M025  \
    #         M026 M029 M030 M031 M033 M034 M037 M041  \
    #         M043 M046 M047 M049 M051 M052 M057 M058  \
    #         M059 M062 M063 SKA001 SKA023 SKA024 SKA028  \
    #         SKA029 SKA030 SKA031 SKA034 SKA038 SKA040  \
    #         SKA041 SKA042 SKA047 SKA052 SKA058 SKA063  \
    #         SKA064 SKA065 SKA066 SKA067 SKA068 SKA069  \
    #         SKA070 SKA071 SKA074 SKA077 SKA080 SKA081  \
    #         SKA082 SKA083 SKA084 SKA085 SKA086 SKA087  \
    #         SKA088 SKA090 SKA091 SKA092 SKA093 SKA094  \
    #         SKA098 SKA101 SKA103 SKA104 SKA105 SKA106  \
    #         SKA108 SKA109 SKA110 SKA113 SKA117 SKA118  \
    #         SKA119 SKA120  \
    #         SKA121 SKA122 SKA123 SKA124 SKA125 SKA126  \
    #         SKA127 SKA128 SKA129 SKA130 SKA131 SKA132  \
    #         SKA133


if __name__ == "__main__":
    main()
