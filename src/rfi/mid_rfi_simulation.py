# pylint: disable=too-many-locals,too-many-arguments
# pylint: disable=too-many-positional-arguments

"""
Generate Radio Frequency Interference simulations for the
SKA Mid telescope.
"""

import argparse
import logging
import os
import pprint
import sys
import time

import astropy.constants as const
import h5py
import numpy as np
from astropy import units as u
from astropy.coordinates import EarthLocation, SkyCoord
from astropy.time import Time
from rascil.processing_components.simulation.rfi import (
    calculate_averaged_correlation,
    simulate_rfi_block_prop,
)
from ska_sdp_datamodels.configuration.config_create import (
    _find_vptype_from_name,
    create_named_configuration,
    select_configuration,
)
from ska_sdp_datamodels.configuration.config_model import Configuration
from ska_sdp_datamodels.science_data_model.polarisation_model import PolarisationFrame
from ska_sdp_datamodels.visibility import export_visibility_to_ms
from ska_sdp_datamodels.visibility.vis_create import create_visibility
from ska_sdp_func_python.util import average_chunks, ecef_to_enu, lla_to_ecef
from ska_sdp_func_python.util.geometry import calculate_hourangles


def cli_parser():
    """CLI argument parser."""
    parser = argparse.ArgumentParser(
        description="Generate Radio Frequency Interference "
        "simulations for the SKA Mid telescope"
    )
    parser.add_argument(
        "--input_file",
        type=str,
        default="",
        help="Full path to the HDF5 file, which contains necessary "
        "RFI information for each RFI source.",
    )
    parser.add_argument(
        "--output_dir",
        type=str,
        default="./",
        help="Output directory for storing files",
    )
    parser.add_argument(
        "--msout",
        type=str,
        default="",
        help="Name for MeasurementSet. If empty string, "
        "no MeasurementSet is written",
    )
    parser.add_argument(
        "--antenna_file",
        type=str,
        default="",
        help="Path to antenna file. If left empty, "
        "MID configuration from RASCIL will be used.",
    )
    parser.add_argument("--seed", type=int, default=18051955, help="Random number seed")
    parser.add_argument(
        "--noise",
        type=str,
        default="False",
        help="Add random noise to the visibility samples?",
    )
    # default RA and DEC works with dataset at
    # ./data/aeronautical_v2_SKA_Mid_coord/aeronautical_sim_datacube_20190313_all.h5
    # They are for an object in ~zenith at time of observation,
    # which is 13/Mar/2019 ~12 UTC
    parser.add_argument(
        "--ra",
        type=float,
        default=0.05000036,
        help="Right Ascension (degrees)",
    )
    parser.add_argument(
        "--dec", type=float, default=-45.95, help="Declination (degrees)"
    )
    parser.add_argument(
        "--nchannels",
        type=int,
        default=8,
        help="How many channels to create within given --frequency_range",
    )
    # frequency_range: default is to encompass the frequency in
    # ./data/aeronautical_v2_SKA_Mid_coord/aeronautical_sim_datacube_20190313_all.h5,
    # which is 1090 MHz
    parser.add_argument(
        "--frequency_range",
        type=float,
        nargs=2,
        default=[1.0e9, 1.2e9],
        help="Frequency range (Hz)",
    )
    parser.add_argument(
        "--apply_primary_beam",
        type=str,
        default="True",
        help="Apply primary beam to RFI sources?",
    )
    # Averaging
    parser.add_argument(
        "--return_average",
        type=str,
        default="False",
        help="Return visibility with time and frequency averaged data?",
    )
    parser.add_argument(
        "--time_average",
        type=int,
        default=16,
        help="Number of integrations in a chunk to average",
    )
    parser.add_argument(
        "--channel_average",
        type=int,
        default=16,
        help="Number of channels in a chunk to average",
    )

    return parser


def _init_logging(output_dir):
    """Set up logging."""
    logfile = f"{output_dir}/mid_rfi_simulation.log"

    logging.basicConfig(
        filename=logfile,
        filemode="a",
        format="%(asctime)s.%(msecs)d %(name)s %(levelname)s %(message)s",
        datefmt="%H:%M:%S",
        level=logging.INFO,
    )

    log = logging.getLogger("rfi-logger")
    log.setLevel(logging.INFO)
    log.addHandler(logging.StreamHandler(sys.stdout))
    log.info("Writing log file to %s", logfile)

    return log


def _get_diam(ant_name):
    """
    According to rascil/data/configurations/ska1mid.cfg
    MeerKAT's (M0) diameter is 13.5 m, while SKA dishes will be 15.0 m
    """
    if "M0" in ant_name:
        return 13.5
    if "SKA" in ant_name:
        return 15.0
    raise ValueError("Unknown antenna type. Cannot determine diameter of dish.")


def generate_ska_mid_configuration(log, antenna_file=None, rmax=None):
    """
    Generate antenna configuration for SKA MID.

    :param antenna_file: csv file containing antenna coordinates at the moment,
                         the code is set up to read files with the structure that
                         rfi/data/aeronautical_v2_SKA_Mid_coord/SKA1MID.csv has
                         expected columns: name, longitude, latitude, altitude
    :param rmax: maximum distance of station from SKA antenna centre
                 if antenna_file is give, this is not used
    """
    if antenna_file:
        antenna_file = os.path.abspath(antenna_file)
        log.info("Using antenna file from %s", antenna_file)

        # taken from ska_sdp_datamodels.configuration.create_named_configuration
        mid_location = EarthLocation(
            lon=21.443803 * u.deg, lat=-30.712925 * u.deg, height=1053.000000
        )

        # the first col contains the name, hence we need to specify the dtype as None,
        # so the code can determine the types on its own for each column
        ant_data = np.genfromtxt(
            antenna_file,
            delimiter=",",
            dtype=None,
            unpack=True,
            encoding="utf-8",
        )

        nants = ant_data[0].shape[0]

        names, lon, lat, alt = (
            ant_data[0],
            ant_data[1],
            ant_data[2],
            ant_data[3],
        )

        x, y, z = lla_to_ecef(lat * u.deg, lon * u.deg, alt)
        ant_xyz = np.stack((x, y, z), axis=1)
        ant_xyz = ecef_to_enu(mid_location, ant_xyz)

        diams = map(_get_diam, names)
        diameters = np.array(list(diams))

        mounts = np.repeat("azel", nants)

        mid = Configuration.constructor(
            name="MID_AERO",
            location=mid_location,
            names=names,
            mount=mounts,
            xyz=ant_xyz,
            vp_type=_find_vptype_from_name(names, {"M0": "MEERKAT", "SKA": "MID"}),
            diameter=diameters,
        )

    else:
        log.info("Using RASCIL MID configuration file")
        mid = create_named_configuration("MID", rmax=rmax)

    return mid


def add_noise(bvis):
    """
    Add noise to the visibilities.
    Note: this will update the original bvis input array in-place too.

    :param bvis: Visibility
    :return: Visibility with noise
    """
    # The specified sensitivity (effective area / T_sys) is roughly
    #   610 m ^ 2 / K in the range 160 - 200MHz
    # sigma_vis = 2 k T_sys / (area * sqrt(tb)) = 2 k 512 / (610 * sqrt(tb)
    sens = 610
    bt = bvis.channel_bandwidth[0] * bvis.integration_time[0]
    # pylint: disable-next=no-member
    sigma = 2 * 1e26 * const.k_B.value / ((sens / 512) * (np.sqrt(bt)))
    sshape = bvis.vis.shape
    bvis["vis"].data += np.random.normal(0.0, sigma, sshape) + 1j * np.random.normal(
        0.0, sigma, sshape
    )
    return bvis


def simulate_rfi_image_prop(
    telescope_config,
    simulation_frequency,
    channel_bandwidth,
    hour_angles,
    apparent_emitter_power,
    emitter_coordinates,
    emitter_ids,
    rfi_frequencies,
    phase_centre,
    polarisation_frame=PolarisationFrame("linear"),
    apply_primary_beam=True,
):
    """
    Create a visibility and fill it in with RFI data using RASCIL.

    :param telescope_config: telescope configuration object
    :param simulation_frequency: frequencies at which we run the simulations
        for (central freq of channels)
    :param channel_bandwidth: bandwidth of frequency channels for simulation
    :param hour_angles: hour angles to create the visibility for;
        they come from the time samples of RFI data
    :param apparent_emitter_power: RFI source power as observed by an isotropic antenna
    :param emitter_coordinates: RFI source coordinates: [azimuth, elevation, distance]
    :param emitter_ids: RFI source IDs or names
    :param rfi_frequencies: frequencies at which we get RFI signal
    :param phase_centre: phase centre where the antenna is pointing at
    :param polarisation_frame: polarisation frame object, default is Stokes I only
    """
    np.nan_to_num(apparent_emitter_power, copy=False)
    np.nan_to_num(emitter_coordinates, copy=False)
    # Override the elevation limit
    bvis = create_visibility(
        telescope_config,
        hour_angles,
        simulation_frequency,
        channel_bandwidth=channel_bandwidth,
        phasecentre=phase_centre,
        polarisation_frame=polarisation_frame,
        zerow=False,
        elevation_limit=0.0,
    )

    # Now fill in the visibility with simulated data
    bvis = simulate_rfi_block_prop(
        bvis,
        apparent_emitter_power,
        emitter_coordinates,
        emitter_ids,
        np.array([rfi_frequencies]),
        low_beam_gain=None,
        apply_primary_beam=apply_primary_beam,
    )

    return bvis


def generate_average_bvis_data(bvis, hour_angles, channel_average, time_average):
    """
    Create a visibility with averaged RFI information.

    :param bvis: visibility to be averaged, containing the RFI signal
    :param hour_angles: hour angles to create the visibility for;
        they come from the time samples of RFI data
    :param channel_average: number of channels to average
    :param time_average: number of time samples (hour angels) to average
    """
    frequency = bvis.frequency.data

    averaged_frequency = np.array(
        average_chunks(frequency, np.ones_like(frequency), channel_average)
    )[0]
    averaged_channel_bandwidth, wts = np.array(
        average_chunks(
            bvis.channel_bandwidth.data,
            np.ones_like(frequency),
            channel_average,
        )
    )
    averaged_channel_bandwidth *= wts
    averaged_times = np.array(
        average_chunks(hour_angles, np.ones_like(hour_angles), time_average)
    )[0]

    averaged_bvis = create_visibility(
        bvis.configuration,
        averaged_times,
        averaged_frequency,
        channel_bandwidth=averaged_channel_bandwidth,
        phasecentre=bvis.phasecentre,
        # pylint: disable-next=protected-access
        polarisation_frame=PolarisationFrame(bvis._polarisation_frame),
        zerow=False,
        elevation_limit=0.0,
    )

    # Calculate the averaged visibility values and fill into the averaged_bbvis
    npol = bvis["vis"].data.shape[-1]
    for itime in range(len(averaged_times)):
        atime = itime * time_average

        for ibaseline in range(len(averaged_bvis.baselines.data)):
            for ichan in range(len(averaged_frequency)):
                achan = ichan * channel_average

                for pol in range(npol):
                    averaged_bvis["vis"].data[itime, ibaseline, ichan, pol] = (
                        calculate_averaged_correlation(
                            bvis["vis"].data[
                                atime : (atime + time_average),
                                ibaseline,
                                achan : (achan + channel_average),
                                pol,
                            ],
                            time_average,
                            channel_average,
                        )[0, 0]
                    )
                    averaged_bvis["vis"].data[itime, ibaseline, ichan, pol] = (
                        np.conjugate(
                            averaged_bvis["vis"].data[itime, ibaseline, ichan, pol]
                        )
                    )
                achan += 1
        atime += 1

    return averaged_bvis


def rfi_simulation(args):
    """
    Run Mid RFI simulations using input HDF5 RFI data and RASCIL.

    :param args: user-defined arguments; result of argparse.ArgumentParser.parse_args().
    """

    output_dir = os.path.abspath(args.output_dir)
    log = _init_logging(output_dir)

    log.info("Starting Mid low-level RFI simulation")

    start_epoch = time.asctime()
    log.info("\nSKA Mid RFI simulation using RASCIL\nStarted at %s\n", start_epoch)
    log.info("Input arguments:\n%s", pprint.pformat(vars(args)))

    # Used when adding noise to bvis
    np.random.seed(args.seed)

    with h5py.File(os.path.abspath(args.input_file), "r") as hdf:
        coords = hdf["coordinates"][()]
        rfi_freq_channels = hdf["frequency_channel"][()]
        source_ids = hdf["source_id"][()]
        rfi_signal = hdf["signal"][()]
        time_samples = hdf["time_samples"][()]
        station_ids = hdf["station_id"][()]

    source_ids = [sid.decode() for sid in source_ids]
    station_ids = [sid.decode() for sid in station_ids]

    full_mid = generate_ska_mid_configuration(log, antenna_file=args.antenna_file)
    mid_conf = select_configuration(full_mid, station_ids)
    log.info("Number of SKA antennas in simulation: %s", len(mid_conf.names))

    # default RA and DEC were chosen to be visible during the day
    # when the test input data was calculated for: 2019.08.16 at 11am Germany
    # (pretend same for Mid site)
    phase_centre = SkyCoord(
        ra=args.ra * u.deg, dec=args.dec * u.deg, frame="icrs", equinox="J2000"
    )

    # Frequency and bandwidth related set-up
    frequency = np.linspace(
        args.frequency_range[0], args.frequency_range[1], args.nchannels
    )
    channel_bandwidth = (frequency[-1] - frequency[0]) / (args.nchannels - 1)
    log.info(
        "Simulating %d frequency channels of width %.3f (MHz)",
        args.nchannels,
        channel_bandwidth * 1e-6,
    )
    channel_bandwidth = np.ones_like(frequency) * channel_bandwidth

    ha = calculate_hourangles(
        mid_conf.location,
        Time(time_samples, format="unix", scale="utc"),
        phase_centre,
    )

    vis = simulate_rfi_image_prop(
        mid_conf,
        frequency,
        channel_bandwidth,
        ha.rad,
        rfi_signal,  # [:2, ...] 2 sources for testing
        coords,  # [:2, ...]
        source_ids,  # [:2]
        rfi_freq_channels,
        phase_centre,
        apply_primary_beam=args.apply_primary_beam == "True",
    )

    if args.return_average == "True":
        vis = generate_average_bvis_data(
            vis, ha.rad, args.channel_average, args.time_average
        )

    if args.noise == "True":
        vis = add_noise(vis)

    if args.msout != "":
        ms_name = output_dir + "/" + args.msout
        export_visibility_to_ms(ms_name, [vis])

    finish_time = time.asctime()
    log.info("\nSKA Mid RFI simulation")
    log.info("Started at %s", start_epoch)
    log.info("Finished at %s", finish_time)


def main():
    """main function"""
    parser = cli_parser()
    args = parser.parse_args()
    rfi_simulation(args)


if __name__ == "__main__":
    main()
