include .make/base.mk
include .make/python.mk

PROJECT_NAME = ska-sim-mid

PYTHON_LINE_LENGTH = 88  # default black line length

# E203: whitespace before ':'
# W503: line break before binary operator
# E402: module level import not at top of file -> caused by matplotlib.use("Agg")
PYTHON_SWITCHES_FOR_FLAKE8 = --ignore=E203,W503,E402 --max-line-length=100

# C0413: wrong-import-position -> caused by matplotlib.use("Agg")
# E0401: import-error -> if we don't want to install all the requirements for linting, ignore this
PYTHON_SWITCHES_FOR_PYLINT = --disable C0413,E0401

# Use bash shell with pipefail option enabled so that the return status of a
# piped command is the value of the last (rightmost) commnand to exit with a
# non-zero status. This lets us pipe output into tee but still exit on test
# failures.
SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -c

clean:  ## Remove all temporary build files
	-rm -rf linting.xml
	-rm -rf htmlcov
	-rm -rf .coverage
	-rm -rf coverage
	-rm -rf coverage.xml
	-rm -rf code-coverage.xml
	-rm -rf unit-tests.xml
	-rm -rf docs/build
	-rm -rf docs/src/_build
	-rm -rf src/*.egg-info
	-rm -rf .eggs

get-rascil-lfs:
	mkdir rascil_data
	curl https://ska-telescope.gitlab.io/external/rascil-main/rascil_data.tgz -o rascil_data.tgz
	tar -zxf rascil_data.tgz -C rascil_data/
	export RASCIL_DATA=`pwd`/rascil_data/data

.PHONY: clean get-rascil-lfs
