.. _rfi_api:

Radio Frequency Interference
============================

.. _mid_rfi_api:

mid_rfi_simulation
------------------

.. argparse::
   :module: rfi.mid_rfi_simulation
   :func: cli_parser
   :prog: mid_rfi_simulation.py


sim_rfi_mid_aa05
----------------

.. automodule:: rfi.sim_rfi_mid_aa05
    :members:
    :undoc-members:
    :show-inheritance:
