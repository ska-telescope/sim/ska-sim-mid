Other handy scripts
===================

calculate_and_save_difference
-----------------------------

.. argparse::
   :module: calculate_and_save_difference
   :func: cli_parser
   :prog: calculate_and_save_difference.py

.. _convert_ms:

convert_to_ms
-------------

.. argparse::
   :module: convert_to_ms
   :func: cli_parser
   :prog: convert_to_ms.py