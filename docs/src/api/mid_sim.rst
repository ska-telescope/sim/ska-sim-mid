.. _mid_sim_api:

Antenna gain effects
====================

The main python script for to simulate visibility data observed by SKA
observations in the presence of any of a number of physical effects afflicting the antenna
gain is `mid_simulation.py <https://gitlab.com/ska-telescope/sim/ska-sim-mid/-/blob/master/src/mid_simulation.py>`_.

mid_simulation
--------------

.. argparse::
   :module: mid_simulation
   :func: cli_parser
   :prog: mid_simulation.py