.. _mid_pointing_api:

Pointing offsets
================

Scripts used for generating voltage pattern files and input data
for mid_simulations.py, which then are used for simulating
data with pointing offsets introduced in azimuth (Az) and elevation (El).
The second script calculates RA and DEC for phase centre from
input Az/El, which define a 5-point scan observation.

gen_gaussian_vp
---------------

.. argparse::
   :module: pointing_offset.gen_gaussian_vp
   :func: cli_parser
   :prog: gen_gaussian_vp.py

get_radec_time
--------------

.. argparse::
   :module: pointing_offset.get_radec_time
   :func: cli_parser
   :prog: get_radec_time.py

.. _global_pt_api:

Global pointing
---------------

.. automodule:: pointing_offset.generate_global_pointing_example_hdf
    :members:
    :undoc-members:
    :show-inheritance:
