.. _documentation_master:

SKA-Mid Simulations
===================

This package collects scripts for various SKA-MID simulations.
It uses `RASCIL <https://gitlab.com/ska-telescope/external/rascil-main.git>`_ for workflow functions,
`ska-sdp-func-python <https://gitlab.com/ska-telescope/sdp/ska-sdp-func-python.git>`_ for processing functions
and `ska-sdp-datamodels <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels.git>`_ for data models.

.. toctree::
  :maxdepth: 2
  :caption: Getting Started

  getting_started
  screens

.. toctree::
  :maxdepth: 1
  :caption: Simulated scenarios

  scenarios/direction_dependent
  scenarios/continuum_imaging
  scenarios/rfi
  scenarios/rcal
  scenarios/pointing_offset
  scenarios/global_pointing

.. toctree::
  :maxdepth: 2
  :caption: API

  api/index