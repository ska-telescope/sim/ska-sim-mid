.. _pointing_offset:

Pointing offset simulation
==========================

These scripts simulate MID pointing offset observations for 5-point (or more)
scans and prepare the simulation data for testing the
`pointing offset calibration pipeline <https://developer.skao.int/projects/ska-sdp-wflow-pointing-offset/en/latest/index.html>`_.

:ref:`mid_simulation.py <mid_sim_api>` is used to generate the MeasurementSet with the simulated data.
See also: :ref:`direction_dependent`

Several bash scripts have been provided in the
`pointing_offset directory <https://gitlab.com/ska-telescope/sim/ska-sim-mid/-/tree/master/scripts/pointing_offset>`_,
which can be used to generate the simulated data for a set of pre-defined observations.

They allow for looping over observational duration and source coordinate (RA and DEC),
and defining the radio source and observational time. The observation they generate is
for MID-AA0.5 telescope layout over a range of hour angles. They use `Dask <https://www.dask.org/>`_
to distribute the processing over several workers.

Note that the simulation has four steps: 

1. Calculate 5-point pointing directions (RA/DEC) according to the given offset
   values, source RA, source DEC, and observation time.
#. Generate Gaussian Voltage Pattern FITS files.
#. Simulate pointing offset data (MeasurementSet format) using mid_simulation.py and generate relevant HDF files. 
#. Add SOURCE OFFSET and relevant tables in the MS files.

The simulation can be run in any PC servers. Depending on the number of channels,
the amount of memory is quite important for the simulation.
Based on current simulation results, the size of voltage pattern files (real or imagined part)
of 8, 256, 1024, and 16384 channels is about 268 MB, 859 MB, 3.43 GB, and 549 GB, respectively.

Outputs of the simulations:

- Measurement Sets (3 MS): actual (expected values with pointing offsets added), nominal (expected values),
  difference (between actual and nominal)
- FITS images: storing the Voltage Pattern (VP) data
- HDF5 files: they include primary beam components and actual, nominal, and source gaintable files

If the user wants to run on a local machine, make sure to setup the below environment variables:

.. code-block::

    SSMROOT: location where the ska-sim-mid repository is
    SSMRESOURCES: location of the resources,  e.g. beam models
    SSMRESULTS: location of results folder

For more details, see:

- https://confluence.skatelescope.org/display/SE/Pointing+Offset+Simulation+Using+SKA-SIM-MID
- https://gitlab.com/ska-telescope/sim/ska-sim-mid/-/blob/master/scripts/pointing_offset/README.md


Script Descriptions
-------------------

In addition to mid_simulations.py, the following python and bash
scripts are used for these simulations. See also: :ref:`Pointing offset API <mid_pointing_api>`.

get_radec_time.py
*****************

- Calculate RA and DEC according to the 5-point observation requirements.
  AstroPy is used in the position calculation.
- Output data files are used in the simulation. 


gen_band2_gaussian_vp.py
************************

- A point source is simulated, and the relevant voltage pattern is applied before the Fourier transform.
- Simulates observations with SKA1-MID AA0.5.
- stokesIQUV, Band B2

make_B2_AA05_5points.sh
***********************

- A shell script to run the simulation.


