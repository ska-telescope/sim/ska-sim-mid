Global pointing data
====================

In support of system-wide testing of a global pointing model update
(`SS-131 <https://jira.skatelescope.org/browse/SS-131>`_ in PI23,
and `SS-162 <https://jira.skatelescope.org/browse/SS-162>`_ in PI24),
we generated pointing offset data products which simulate the output
of the `pointing offset calibration pipeline <https://developer.skao.int/projects/ska-sdp-wflow-pointing-offset/en/latest/index.html>`_
and can be used for generating a global pointing model offline.

These are example data products and are based on running:

- The pointing-offset pipeline on MeerKAT test data for a 9-point observation with 61 dishes.
  The original file on which the generated set are based was derived from running the standalone
  version of the pointing-offset: 1672727931_sdp_l0_scan_pointing_offsets.hdf5
- TPOINT on the Max Planck prototype dish pointing observation: 20210311.ral.dat

The HDF5 and TPOINT files are available for download from
`GDrive <https://drive.google.com/drive/u/1/folders/1CW6o8lvwAmH-NLvyQvBmsrYhTq44fbi9>`_.
The HDF5 file format is described
in `this documentation <https://developer.skao.int/projects/ska-sdp-wflow-pointing-offset/en/latest/data_product.html>`_.

The format definition for the input TPOINT file is from: ``indat.pdf``

The example file uses "format 4" which has the following 4 columns:

- star "observed" Az
- star "observed" El
- telescope raw Az
- telescope raw El

In our case, these columns translate to actual and commanded position as follows:

- "Observed" coordinates = position where the source has been found - i.e.
  the commanded pointing with fitted pointing offset from the pipeline
  applied to it = Actual position
- Raw coordinates = where the telescope thinks the target should be
  (without any existing pointing model applied) = Commanded position

Assumptions/simplifications made when generating the files:

- a single execution block will be used for all sources
- a single processing block will be used for all sources
  (note that this assumes that the scans for all observations are
  taken continuously with no interruptions. It is possible there might
  be multiple PB IDs used in real observation sets if that is not the case.)
- pointing results for all dishes will be set to identical values for this test
  since the TPOINT file contains results from a single dish
- scan numbers start at one and continuously increment through all source observations
- each pointing observation is assumed to take 9 minutes to execute (track_duration set to 1 minute)
- the central position of the pattern is observed in the last scan (as in the input MeerKAT data)
- only telescope latitude will be updated, as that is the only coordinate used by TPOINT.
  The longitude and altitude of the telescope will be set to the MeerKAT values.

The files are saved into a directory structure based on

.. code-block::

    /product/{eb_id}/ska-sdp/{pb_id}/scans{first_scan}-{last_scan}

which replicates the directory structure defined by
`ADR-55 <https://confluence.skatelescope.org/display/SWSI/ADR-55+Definition+of+metadata+for+data+management+at+AA0.5>`_,
with filename:

.. code-block::

    pointing_offsets_scans{first_scan}-{last_scan}.hdf5

The script used to generate the files is
:ref:`generate_global_pointing_example_hdf.py <global_pt_api>`.

This script does the following:

1. Reads example HDF5 file from the pointing pipeline to use as the template
   `pointing_table <https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/blob/main/src/ska_sdp_datamodels/calibration/calibration_model.py?ref_type=heads#L389>`_ object
    - This is from MeerKAT data and so already has positional information included for 61 dishes
#. Reads input TPOINT file supplied by System Science team
#. Extracts the observation date and telescope latitude from TPOINT header
#. Extracts commanded pointing (Az/El) and pointing offsets (x-el/el) from TPOINT file for each source
    - commanded pointing = columns 3 and 4 in TPOINT file ("telescope raw")
    - pointing offsets = simple delta offset from commanded pointing
      to actual pointing (cols 1 and 2: "star observed") - i.e. the pointing offset is positive when
      the actual coordinate is greater than the commanded coordinate.
#. Updates pointing_table object for each source:
    - pointing - same value (the fitted pointing offsets in cross-elevation and elevation)
      extracted from the TPOINT file used for all 61 dishes in radians
    - time - assuming 1 min per scan, with first observation starting at 00:00 on date given in TPOINT header.
      Note that time is set to the mid-point of the central scan.
    - commanded_pointing - same value for all 61 dishes
#. Updates pointing_table configuration object:
    - telescope name attribute set to "SKA"
    - telescope location updated using latitude from TPOINT header (keep existing MeerKAT longitude and altitude)
    - dish locations updated using latitude from TPOINT header (keep existing MeerKAT longitude and altitude)
#. Updates pointing_table metadata:
    - scan_mode - uses "9-point" from original MeerKAT input file
    - pointingcentre - calculated for each source from the time and actual Az/El using parameters of first dish
#. Constructs filename and path using:
    - A single execution block id constructed using the date from the example HDF:
      ``eb-globalpointing-{date}-00000``
    - A single processing block id constructed using the date from the TPOINT file:
      ``pb-globalpointingpo-{date}-00000``
    - scan numbers, starting from 1 and running through all sources using a 9-point pattern
#. Writes out HDF file for each source into the relevant directory
#. Writes out metadata yaml file for each source

How to use the script
---------------------

The script requires 2 input files which are given as arguments:

- Example pointing-offset pipeline output in HDF format
- TPOINT input file using INDAT format 4

The script writes the new files into a directory structure at the location of the input example HDF file.

The script should be run in Python:

.. code-block:: bash

    python generate_global_pointing_example_hdf.py --hdf_filename=<HDF_file> --tpoint_filename=<TPOINT_file>

It has the following dependencies:

- astropy
- katpoint
- ska_sdp_datamodels
- ska_sdp_dataproduct_metadata
- ska_sdp_func_python
