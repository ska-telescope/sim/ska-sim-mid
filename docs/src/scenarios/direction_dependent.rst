.. _direction_dependent:

Direction-dependent effects
===========================

The bash scripts located in
`ska-sim-mid/scripts/direction_dependent/ <https://gitlab.com/ska-telescope/sim/ska-sim-mid/-/tree/master/scripts/direction_dependent/scripts>`_
simulate MID observations of multiple point sources and calculate the effect of direction dependent (DD) gain
errors.

    - The sky model is constructed from Oxford S3-SEX catalogue. These are unpolarised point sources.
    - The observation is by MID or MEERKAT+ over a range of hour angles.
    - The visibility is calculated by Direct Fourier transform  after application of the gaintable for each source.
    - `Dask <https://www.dask.org/>`_ is used to distribute the processing over a number of workers.
    - Processing can be divided into chunks of time (default 1800s).

See the following presentation for an overview of the results:
`Simulating MID direction dependent gain effects SPO-1057 <https://docs.google.com/presentation/d/1NfhdqEHHfXl_MvZnfrCh0Je1JQrJjRdGb39tyUU2V7c>`_

In addition, see discussions on
`this Confluence page <https://confluence.skatelescope.org/display/SE/Simulations+with+Direction-Dependent+Effects>`_.

`mid_simulation.py <https://gitlab.com/ska-telescope/sim/ska-sim-mid/-/blob/master/src/mid_simulation.py>`_
is a python wrapper around `RASCIL <https://developer.skao.int/projects/rascil/en/latest/>`_ functionality,
which was originally designed to simulate (DD) effects for the SKA Mid telescope.
It can be run directly using the command line
arguments described by the :ref:`API <mid_sim_api>`,
or by the example bash scripts linked above.
The bash scripts allow for looping over duration and declination. They will need to be altered for
location of the various files needed. We recommend use of the bash scripts at first.
These simulations typically require a cluster to run (with minimum 512GB cores, and
a large number of cores/threads).

The simulation has two steps:

- first the visibilities are calculated and written to HDF files, using mid_simulation.py
- then all the HDF files are combined into one MeasurementSet using :ref:`convert_to_ms.py <convert_ms>`.
  In this conversion step, a number of diagnostic plots are written.

Types of output:

- MeasurementSets (MS)
- Images in FITS format: note that by default, an imager is not run as part of the simulations.
  Images can be made by setting the ``--make_images`` CLI argument to "True".

The following MS are generated: actual, nominal, and difference

- actual: data with DD effects added, is it would "actually" be after an observation.
  See below for list of effects that can be simulated.
- nominal: data without any gain errors. A set of point sources is simulated and the relevant
  nominal voltage pattern for each end of interferometer is applied before Fourier transform.
  The nominal pattern is constructed from a tapered symmetric illumination pattern,
  with the diameter of the SKA and/or MeerKAT dishes.
- difference: difference between actual and nominal

If you are running on your own machine, make sure you have the below environment variables set up:

.. code-block::

    SSMROOT : location where the ska-sim-mid repository is
    SSMRESOURCES : location of the resources e.g. beam models
    SSMRESULTS: location of results folder


Effects simulated
-----------------

The following effects can be simulated using mid_simulation.py. This can be set with
the ``--mode`` CLI argument:

- ``wind_pointing``: wind-induced pointing errors
  Three types of wind-conditions are available: precision, standard or degraded. The definitions
  are available in the
  `Operation conditions.docx <https://confluence.skatelescope.org/display/SE/MID+pointing+error+simulations?preview=/73467563/74716283/Operating%20Conditions.docx>`_
  document attached to `Confluence <https://confluence.skatelescope.org/display/SE/MID+pointing+error+simulations>`_.
  Pre-generated pointing data used for the simulations is available from the Google Cloud Platform,
  `ska1-simulation-data/resources/mid/pointing_models <https://console.cloud.google.com/storage/browser/ska1-simulation-data/resources/mid/pointing_models>`_.
- ``random_pointing``: allows simulating dynamic, static, and global pointing errors using the
  `simulate_pointingtable RASCIL function <https://gitlab.com/ska-telescope/external/rascil-main/-/blob/master/rascil/processing_components/simulation/pointing.py?ref_type=heads#L280>`_.
  The size of pointing error is adjustable via CLI arguments of mid_simulations.py.
- ``polarisation``: uses pre-generated voltage pattern files for both actual and nominal data.
  See `create_polarisation_gaintable_rsexecute_workflow <https://gitlab.com/ska-telescope/external/rascil-main/-/blob/master/rascil/workflows/rsexecute/simulation/simulation_rsexecute.py?ref_type=heads#L494>`_.
- ``heterogeneous``: similar to ``polarisation`` with the nominal voltage pattern modified slightly.
  See `create_heterogeneous_gaintable_rsexecute_workflow <https://gitlab.com/ska-telescope/external/rascil-main/-/blob/master/rascil/workflows/rsexecute/simulation/simulation_rsexecute.py?ref_type=heads#L602>`_.
- ``surface``: simulate gravity-induced surface errors.
  See `create_surface_errors_gaintable_rsexecute_workflow <https://gitlab.com/ska-telescope/external/rascil-main/-/blob/master/rascil/workflows/rsexecute/simulation/simulation_rsexecute.py?ref_type=heads#L377>`_.
- ``ionosphere``: ionospheric effects are simulated as a combination of :ref:`pre-generated screen data <screens>` and the
  voltage pattern data used for polarisation effect simulations. A set of point sources is simulated and the phases
  calculated using a thin screen model for the ionosphere. The screen has units of meters of Total Electron Content.
  The phase is evaluated at places in the screen where the line of sight from source to a dish pierces the screen.
- ``troposphere``: same method as ``ionosphere`` but using :ref:`screen data <screens>` with atmospheric effects.

``wind_pointing``, ``random_pointing``, ``polarisation``, and ``heterogeneous``
effects use, as a first step, pre-generated
`voltage pattern files <https://console.cloud.google.com/storage/browser/ska1-simulation-data/resources/mid/beam_models/SKADCBeamPatterns>`_,
which are available on GCP. For each option, a set of point sources is simulated and
the relevant voltage pattern for each end of the interferometer is applied before Fourier transform.

For more information on the provided bash scripts, see the documentation here:
https://gitlab.com/ska-telescope/sim/ska-sim-mid/-/blob/master/scripts/direction_dependent/scripts/README.md