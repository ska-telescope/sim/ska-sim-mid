Realtime calibration simulations
================================

Realtime calibration pipeline (RCAL) generates beamformer gain calibration
results, in realtime, i.e. when an observation is ongoing.

The simulations generated as part of this task, are used to test the
`RASCIL RCAL pipeline <https://developer.skao.int/projects/rascil/en/latest/apps/rascil_rcal.html>`_.

They were generated using the :ref:`mid_simulations.py <mid_sim_api>` script.

The bash script and other relevant scripts are described here:
https://gitlab.com/ska-telescope/sim/ska-sim-mid/-/tree/master/scripts/rcal_simulations

For more information on the pipeline and the testing process, see the
following Confluence page:
`Testing the RCAL pipeline <https://confluence.skatelescope.org/display/SE/Testing+the+RCAL+pipeline>`_
