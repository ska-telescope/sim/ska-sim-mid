.. _continuum_imaging:

Continuum imaging
=================

The bash and slurm scripts which generated these simulations can be
found at `ska-sim-mid/scripts/continuum_imaging <https://gitlab.com/ska-telescope/sim/ska-sim-mid/-/tree/master/scripts/continuum_imaging>`_.

These scripts simulate MID continuum imaging observations of multiple point sources.
Originally, these scripts used ``--mode nominal`` of :ref:`mid_simulation.py <mid_sim_api>`,
which is no longer available. However, one can run the code in any mode, which will
generate nominal data. Nominal data do not have any gain errors added to them.

See also: :ref:`direction_dependent`

In addition to generating the simulations, there are bash scripts provided, which run the
`RASCIL Imager <https://developer.skao.int/projects/rascil/en/latest/apps/rascil_imager.html>`_
and the `RASCIL Imaging QA tool <https://developer.skao.int/projects/rascil/en/latest/apps/imaging_qa.html>`_.

Naming convention of bash scripts:

- ``make_``: generate simulated Measurement Sets
- ``image_``: run the RASCIL Imager and generate images in FITS format
- ``check_``: run the Imaging QA and generate various quality assessment plots from the images

For more information, see the following Confluence pages:

- `Continuum Imaging Pipeline and Imaging QA <https://confluence.skatelescope.org/display/SE/Continuum+Imaging+Pipeline+and+Imaging+QA>`_ (and sub-pages)
- `Continuum Imaging Simulations PI12: Methodology <https://confluence.skatelescope.org/display/SE/Continuum+Imaging+Simulations+PI+12%3A+Methodology>`_
- `Continuum Imaging Simulations PI12: Results <https://confluence.skatelescope.org/display/SE/Continuum+Imaging+Simulations+PI+12%3A+Results>`_
