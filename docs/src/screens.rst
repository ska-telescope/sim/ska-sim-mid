.. _screens:

Ionospheric and tropospheric screens
====================================

The :ref:`mid_simulations.py <mid_sim_api>` python script, which is used to generate various
simulated data for the SKA Mid telescope, uses ionospheric and atmospheric
screens in the form of FITS file fo two of its simulation modes:

- ``ionosphere``
- ``troposphere``

These screens are generated using the python code described at :ref:`screen_api`.
They use the `ARatmospy <https://github.com/shrieks/ARatmospy>`_ package under the hood, which
needs to be installed. For python3, use the forked version at https://github.com/timcornwell/ARatmospy.

There is also a `Makefile <https://gitlab.com/ska-telescope/sim/ska-sim-mid/-/blob/master/src/screens/Makefile>`_
provided, which simplifies running the right commands for generating the files.
A detailed description of how one can do this can be found in the code
`README <https://gitlab.com/ska-telescope/sim/ska-sim-mid/-/blob/master/src/screens/README.md>`_.

The files are also available on the `Google Cloud Platform <https://console.cloud.google.com/storage/browser/ska1-simulation-data/resources/shared/screens>`_.