Accessing the code and data
---------------------------

To run the scripts, clone the repository with git::

    git clone https://gitlab.com/ska-telescope/sim/ska-sim-mid.git --recurse-submodules

Set the below environment variables:

.. code-block:: bash

    export SSMROOT=/path/to/code
    export SSMRESULTS=/path/to/results

In direction dependent simulations, one will also need the additional data resources
(e.g. beam models) which requires setting the below environment variable:

.. code-block:: bash

    export SSMRESOURCES=/path/to/resources

The resources are available from the Google Cloud Platform (GCP):

- Mid beam and pointing models: https://console.cloud.google.com/storage/browser/ska1-simulation-data/resources/mid
- Ionospheric and tropospheric screens: https://console.cloud.google.com/storage/browser/ska1-simulation-data/resources/shared

They are 80GB in size. They can also be downloaded via the GCP command line interface,
`gsutil <https://cloud.google.com/storage/docs/gsutil>`_:

.. code-block:: bash

    cd resources
    gsutil -m rsync -r gs://ska1-simulation-data/resources/mid/beam_models/ beam_models
    gsutil -m rsync -r gs://ska1-simulation-data/resources/shared/screens screens

Installing requirements
-----------------------

`Poetry <https://python-poetry.org/>`_ is used for dependency management for the repository.
We recommend that you install the dependencies into a virtual environment.
Once the environment is activated (or if you set poetry up to create one automatically), run:

.. code-block:: bash

    poetry install

The dependencies are listed in the
`pyproject.toml <https://gitlab.com/ska-telescope/sim/ska-sim-mid/-/blob/master/pyproject.toml>`_ file, and
`poetry.lock <https://gitlab.com/ska-telescope/sim/ska-sim-mid/-/blob/master/poetry.lock>`_ file.
Poetry will install the versions listed in poetry.lock.

Repository structure
--------------------

The `ska-sim-mid <https://gitlab.com/ska-telescope/sim/ska-sim-mid>`_ repository
contains bash scripts to run simulations, which can be found in the
`scripts <https://gitlab.com/ska-telescope/sim/ska-sim-mid/-/tree/master/scripts>`_ directory.
The python source code can be found in
the `src <https://gitlab.com/ska-telescope/sim/ska-sim-mid/-/tree/master/src>`_ directory, with its tests in the
`tests <https://gitlab.com/ska-telescope/sim/ska-sim-mid/-/tree/master/tests>`_ directory.

Contributing to the repository
------------------------------

You are welcome to submit merge requests to the repository.

`Black <https://github.com/psf/black>`_, `isort <https://pycqa.github.io/isort/>`_,
and various linting tools are used to keep the Python code in good shape.
Please check that your code follows the formatting rules before committing it
to the repository. You can apply Black and isort to the code with:

.. code-block:: bash

    make python-format

and you can run the linting checks locally using:

.. code-block:: bash

    make python-lint

The linting job in the CI pipeline does the same checks, and it will fail if
the code does not pass all of them.

In addition, make sure your code is well tested and documented.
