#!/usr/bin/env bash
if [ -z "$SSMROOT" ]
then
    SSMROOT=${HOME}/Code/ska-sim-mid
fi
echo "SSMROOT : $SSMROOT"
export PYTHONPATH=$SSMROOT:$PYTHONPATH

if [ -z "$SSMRESULTS" ]
then
    SSMRESULTS=${SSMROOT}/scripts/pointing_offset/
fi
echo "SSMRESULTS : $SSMRESULTS"

if [ -z "$SSMRESOURCES" ]
then
    SSMRESOURCES=${SSMROOT}/resources
fi
echo "SSMRESOURCES : $SSMRESOURCES"

vp_directory=${SSMRESOURCES}/beam_models/
screens=${SSMRESOURCES}/screens

# DO NOT set source in south polar area
dec_s=-30.0
ra_s=295.35
offset_s=0.50

integration_time=7.5

echo "=============================point-1======================================"
dec=${dec_s}
ra=${ra_s}
results_dir=${SSMRESULTS}/random_p_ra_${ra}_dec_${dec}
mkdir -p ${results_dir}
python3 ${SSMROOT}/src/mid_simulation.py --mode random_pointing\
  --declination ${dec} --ra ${ra} --dec_s ${dec_s} --ra_s ${ra_s} --band B2 --results ${results_dir} \
  --source point  --time_chunk ${integration_time} --static_pe 1.0 1.0 \
  --duration custom --time_range -1.5 -1.0 --integration_time ${integration_time} --height 3000 \
  --configuration ${tmt} --vp_directory ${vp_directory} --nchan ${nchan} \
  --make_images False --write_gt True --apply_pb False \
  --write_pt True --seed 1234 --add_noise ${addnoise}

echo "=============================point-2======================================"
dec=`echo "$dec_s+$offset_s" |bc`
ra=${ra_s}
results_dir=${SSMRESULTS}/random_p_ra_${ra}_dec_${dec}
mkdir -p ${results_dir}
python3 ${SSMROOT}/src/mid_simulation.py --mode random_pointing\
  --declination ${dec} --ra ${ra} --dec_s ${dec_s} --ra_s ${ra_s} --band B2 --results ${results_dir} \
  --source point  --time_chunk ${integration_time} --static_pe 1.0 1.0 \
  --duration custom --time_range -1.0 -0.5 --integration_time ${integration_time} --height 3000 \
  --configuration ${tmt} --vp_directory ${vp_directory} --nchan ${nchan} \
  --make_images False --write_gt True --apply_pb False \
  --write_pt True --seed 1234 --add_noise ${addnoise}

echo "=============================point-3======================================"
dec=`echo "$dec_s-$offset_s" |bc`
ra=${ra_s}
results_dir=${SSMRESULTS}/random_p_ra_${ra}_dec_${dec}
mkdir -p ${results_dir}
python3 ${SSMROOT}/src/mid_simulation.py --mode random_pointing\
  --declination ${dec} --ra ${ra} --dec_s ${dec_s} --ra_s ${ra_s} --band B2 --results ${results_dir} \
  --source point  --time_chunk ${integration_time} --static_pe 1.0 1.0 \
  --duration custom --time_range -0.5 0.0 --integration_time ${integration_time} --height 3000 \
  --configuration ${tmt} --vp_directory ${vp_directory} --nchan ${nchan} \
  --make_images False --write_gt True --apply_pb False \
  --write_pt True --seed 1234 --add_noise ${addnoise}

echo "=============================point-4======================================"
dec=${dec_s}
ra=`echo "$ra_s+$offset_s" |bc`
results_dir=${SSMRESULTS}/random_p_ra_${ra}_dec_${dec}
mkdir -p ${results_dir}
python3 ${SSMROOT}/src/mid_simulation.py --mode random_pointing\
  --declination ${dec} --ra ${ra} --dec_s ${dec_s} --ra_s ${ra_s} --band B2 --results ${results_dir} \
  --source point  --time_chunk ${integration_time} --static_pe 1.0 1.0 \
  --duration custom --time_range 0.0 0.5 --integration_time ${integration_time} --height 3000 \
  --configuration ${tmt} --vp_directory ${vp_directory} --nchan ${nchan} \
  --make_images False --write_gt True --apply_pb False \
  --write_pt True --seed 1234 --add_noise ${addnoise}

echo "=============================point-5======================================"
dec=${dec_s}
ra=`echo "$ra_s-$offset_s" |bc`
results_dir=${SSMRESULTS}/random_p_ra_${ra}_dec_${dec}
mkdir -p ${results_dir}
python3 ${SSMROOT}/src/mid_simulation.py --mode random_pointing\
  --declination ${dec} --ra ${ra} --dec_s ${dec_s} --ra_s ${ra_s} --band B2 --results ${results_dir} \
  --source point  --time_chunk ${integration_time} --static_pe 1.0 1.0 \
  --duration custom --time_range 0.5 1.0 --integration_time ${integration_time} --height 3000 \
  --configuration ${tmt} --vp_directory ${vp_directory} --nchan ${nchan} \
  --make_images False --write_gt True --apply_pb False \
  --write_pt True --seed 1234 --add_noise ${addnoise}
  