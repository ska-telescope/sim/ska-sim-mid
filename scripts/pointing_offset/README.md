Scripts in this directory are simulation generated
to test the [pointing offset calibration pipeline](https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-wflow-pointing-offset.git)

The simulations are kept on the Google Cloud Platform, and can be downloaded via
(assume [gsutil](https://cloud.google.com/storage/docs/gsutil) is installed)

    gsutil -m cp -r gs://ska1-simulation-data/simulations/pointing-offset_SP-3221 .

If you wish to run these or similar simulations, install RASCIL:

    https://developer.skao.int/projects/rascil/en/latest/RASCIL_install.html

and use the shell scripts and slurm files as template.

We have provided to example bash scripts for AA0.5 layout (change the configuration
for full MID layouts.)

* ``make_B2_AA05_single.sh`` is for generating a single observation using a point source.
* ``make_B2_AA05_5points.sh`` is for generating 5-point observations, with the pointings 0.5 degrees in each direction in B2 band.
* ``make_B5_AA05_5points.sh`` is for generating 5-point observations, but with B5 band

Make sure you have the below environment variables set up:

    SSMROOT : location where the ska-sim-mid repository is
    SSMRESOURCES : location of the resources e.g. beam models
    SSMRESULTS: location of results folder

To do the full simulation, two preceding scripts need to be run
* ``src/pointing_offset/get_radec_time.py``: To get the RA, Dec and time for each observation of 5 point.
* ``src/pointing_offset/gen_gaussian_vp.py``: In case to use custom Voltage Pattern file, run this script to generate the FITS file to use.

