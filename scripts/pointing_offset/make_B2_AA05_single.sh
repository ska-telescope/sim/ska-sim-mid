#!/usr/bin/env bash
# Script for generating pointing offset simulation data with AA0.5 layout
# No noise or missing data points
# Note the time range are currently tentative.
if [ -z "$SSMROOT" ]
then
    SSMROOT=${HOME}/Code/ska-sim-mid
fi
echo "SSMROOT : $SSMROOT"
export PYTHONPATH=$SSMROOT:$PYTHONPATH

if [ -z "$SSMRESULTS" ]
then
    SSMRESULTS=${SSMROOT}/scripts/pointing_offset/
fi
echo "SSMRESULTS : $SSMRESULTS"

if [ -z "$SSMRESOURCES" ]
then
    SSMRESOURCES=${SSMROOT}/resources
fi
echo "SSMRESOURCES : $SSMRESOURCES"

results_dir=${SSMRESULTS}/random_aa05
mkdir -p ${results_dir}

vp_directory=${SSMRESOURCES}/beam_models/
screens=${SSMRESOURCES}/screens

rm -r ${results_dir}/SKA_MID-AA0.5_SIM_custom_B2_random-pointing_nchan*_{nominal,difference}.ms
rm -r ${results_dir}/SKA_MID-AA0.5_SIM_custom_B2_random-pointing_nchan*_{actual,nominal,difference}_*.hdf

python3 ${SSMROOT}/src/mid_simulation.py --mode random_pointing\
  --declination -63.71 --ra 295.35 --band B2 --results ${results_dir} \
  --source point  --time_chunk 3600.0 --static_pe 1.0 1.0 \
  --duration custom --time_range -0.25 0.75 --integration_time 3600.0 --height 3000 \
  --configuration MID-AA0.5 --vp_directory ${vp_directory} --nchan 100 \
  --make_images False --write_gt True --apply_pb False \
  --write_pt True --seed 1234

mv ${results_dir}/SKA_MID-AA0.5_SIM_custom_B2_random-pointing_nchan*_actual.ms ${results_dir}/pointing_${time}.ms
mv ${results_dir}/SKA_MID-AA0.5_SIM_custom_B2_random-pointing_nchan*_pbcomponents.hdf pointing_${time}_pbcomponents.hdf
rm -r ${results_dir}/SKA_MID-AA0.5_SIM_custom_B2_random-pointing_nchan*_{nominal,difference}.ms
rm -r ${results_dir}/SKA_MID-AA0.5_SIM_custom_B2_random-pointing_nchan*_{actual,nominal,difference}_*.hdf
