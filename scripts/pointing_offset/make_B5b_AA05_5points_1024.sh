#!/usr/bin/env bash
if [ -z "$SSMROOT" ]
then
    SSMROOT=/home/wangfeng/work/ska-sim-mid
fi
echo "SSMROOT : $SSMROOT"

if [ -z "$SSMRESOURCES" ]
then
    SSMRESOURCES=/mnt/storage-main/main/ska/ska1-simulation-data/resources/mid
fi
echo "SSMRESOURCES : $SSMRESOURCES"
vp_directory=${SSMRESOURCES}/beam_models/

if [ -z "$SSMRESULTS" ]
then
    SSMRESULTS=/mnt/storage-main/main/ska/meerkat/orc-1915/global-5_point-${nchan}-b5-astropy-${integration_time}s-${time_chunk}chunk-gaussianvp_pe0pe1-${globlepe0}-${globlepe1}
fi
echo "SSMRESULTS : $SSMRESULTS"

mkdir -p $SSMRESULTS

#Pre-defined runtime parameters
tmt=MID-AA0.5
addnoise=False
nchan=16384
globlepe0=7
globlepe1=4.5
# DO NOT set source in south polar area
dec_s=-63.440294035191656
ra_s=294.8629130087714

#time_chunk allows us to have 5 timestamps
# In each pointing table
integration_time=4
time_chunk=20
channel_bandwidth=427246.09375

# Input files to read
file="ha_1024_B5G.dat"
vp_real_file="B5b_real_gaussian_8.500e+09_8.fits"
vp_imag_file="B5b_imag_gaussian_8.500e+09_8.fits"


while IFS= read -r line; do
    echo "======doing=${line}========"

    array=($line)
    ra=${array[0]}
    dec=${array[1]}
    hastart=${array[2]}
    hastop=${array[3]}
    stime=${array[4]}

    results_dir=${SSMRESULTS}/random_p_ra_${ra}_dec_${dec}
    mkdir -p ${results_dir}

    python3 ${SSMROOT}/src/mid_simulation.py --mode random_pointing \
      --pbtype MID_B5BG \
      --nchan ${nchan}  --channel_width ${channel_bandwidth} \
      --declination ${dec} --ra ${ra} --dec_s ${dec_s} --ra_s ${ra_s} --band B5BG --results ${results_dir} \
      --source point  --time_chunk ${time_chunk} --global_pe ${globlepe0} ${globlepe1} \
      --duration custom --time_range ${hastart} ${hastop} \
      --integration_time ${integration_time} --height 3000 \
      --configuration ${tmt} --dynamic_pe 0 \
      --vp_directory ${vp_directory} \
      --vp_real_file ${vp_real_file} \
      --vp_imag_file ${vp_imag_file} \
      --make_images False --write_gt True --apply_pb False --use_dask True \
      --write_pt True --seed 1234 --add_noise ${addnoise} --obs_time ${stime} \
 
done < $file


