#!/usr/bin/env bash

#! Set essential environment variables
if [ -z "$SSMRESULTS" ]
then
    SSMRESULTS=${HOME}/data/ska_mid_simulations/results
fi

if [ -z "$SSMROOT" ]
then
    SSMROOT=${HOME}/Code/ska-sim-mid
fi

if [ -z "$SSMRESOURCES" ]
then 
    SSMRESOURCES=/alaska/shared/ska-sim-mid/ # only on P3
fi

echo "SSMROOT : $SSMROOT"
echo "SSMRESULTS : $SSMRESULTS"
echo "SSMRESOURCES: $SSMRESOURCES"

vp_directory=${SSMRESOURCES}/beam_models/
screens=${SSMRESOURCES}/screens


for mode in troposphere wind_pointing surface
  do
    python3 ${SSMROOT}/src/mid_simulation.py --mode ${mode}  --flux_limit 0.000005 \
      --declination -45.0 --band Ku --pbtype MID_Ku --results ${SSMRESULTS} --rmax 1e3 \
      --nworkers 8 --processes 1 --nthreads 1   --memory 512GB \
      --duration custom --integration_time 600.0   \
      --screen ${screens}/tropo_screen_long.fits --height 3000 \
      --configuration MID --vp_directory ${vp_directory} --nchan 4
  done

