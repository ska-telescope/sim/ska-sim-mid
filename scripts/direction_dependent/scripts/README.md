## Introduction and set up

In this directory we have example driver scripts for use of the RASCIL-based simulation script
mid_simulation.py. The scripts will need to be altered for location of the various files needed.

All the  driver scripts (e.g. wind_pointing/make_B2.sh) can loop over duration and declination.

These simulations work best on the P3 cluster or login node. See 
https://confluence.skatelescope.org/display/SE/P3+How+To

If you are running on your own machine, make sure you have the below environment variables set up:

    SSMROOT : location where the ska-sim-mid repository is
    SSMRESOURCES : location of the resources e.g. beam models
    SSMRESULTS: location of results folder

The default or these variables are set as common directories on P3.

There are two outputs: Images and MeasurementSets. The images can be viewed using 
the [casaviewer](https://casadocs.readthedocs.io/en/v6.4.0/api/casaviewer.html) or 
[carta](https://cartavis.org/). 
The MeasurementSets can be viewed using casaviewer. 

## Relevant simulation parameters

The driver script for these simulations is mid_simulations.py.
Its API is described here: 
https://developer.skao.int/projects/ska-sim-mid/en/latest/api.html#antenna-gain-effects

For a detailed description of direction dependent simulations, 
follow the documentation: 
https://developer.skao.int/projects/ska-sim-mid/en/latest/direction_dependent/guide.html

The CLI argument ``--mode`` defines the type of gain error simulated. By default,
nominal, actual, and difference data are written. Setting ``--only_actual`` to
``True``, will result in a faster run, since only the actual data will be
computed and written to disk.

In addition to the ``--mode`` command line arguments, there are three particularly important other
command line arguments:

``--rmax``: The maximum radius of antennas used in the simulation from the array centre.
If rmax is set too large then calculating the dirty images can take a long time. 

``--npixel``: The size of the image on each axis e.g. 1024 gives a 1024 x 1024 imageIf this is set large
the imaging will take a longer

``--nchan``: The number of channels in the simulation. Only nchan greater than 1
unless you have a good reason. The processing time will scale with nchan.

## Provided scripts

Below are some of the important parameters that are changed between the scripts
provided in this directory. Once can use these as examples, and adjust them for 
their own needs.

### heterogenous

- Mode: heterogeneous
- Band: B2 only
- Configuration: all SKA dishes, with ``--rmax: 1e4`` [m]
- Polarisation: Stokes IQUV

### heterogenous_meerkat+

- Mode: heterogeneous
- Band: B2 only
- Configuration: MEERKAT+ dishes, with ``--rmax: 1e4`` [m]
- Polarisation: Stokes IQUV

### ionosphere

- Simulates observations with ionospheric screens on and off.
- A set of point sources is simulated and the phases calculated using a thin screen model for the
  ionosphere. The screen has units of meters of Total Electron Content. The phase is evaluated at
  places in the screen where the line of sight from source to a dish pierces the screen.
- Simulates observations with ionospheric screens on and off.
- Mode: ionosphere
- Band: B1LOW
- Configuration: all SKA dishes, with ``--rmax: 1e4`` [m]
- Polarisation: Stokes I

### polarisation

- Simulates observations with SKA with cross pol (on) and SKA no cross pol (off)
- Mode: polarisation
- Band: B2
- Configuration: all SKA dishes, with ``--rmax: 1e4`` [m]
- Polarisation: Stokes IQUV, linear

### random_pointing

- Mode: random_pointing
- Band: B2
- Configuration: all SKA dishes, with ``--rmax: 1e4`` [m]

### surface

- Simulates observations with sagging dish (on) and nominal dish (off)
- Models of the voltage pattern are available at +15, +45, +90 deg elevation. 
  We interpolate between those to 5 degrees.
- Mode: surface
- Band: B2
- Configuration: all SKA dishes, with ``--rmax: 1e4`` [m]
- Polarisation: Stokes I
- For more details see: https://confluence.skatelescope.org/display/SE/Dish+deformation+simulations

### troposphere

- Simulates observations with tropospheric screens on and off.
- A set of point sources is simulated and the phases calculated using a thin screen model for the
  atmosphere. The phase is evaluated at places in the screen where the line of sight from source to
  dish pierces the screen. The screen has units of meters of delay.
- Band: B2 and B5
- Mode: troposphere
- Configuration: all SKA dishes, with ``--rmax: 1e4`` [m]
- Polarisation: Stokes I

### wind_pointing

- Simulates observations with wind buffeting of dishes (on) and without (off)
- Mode: 
- Band: B2 and B5
- Configuration: all SKA dishes, with ``--rmax: 1e4`` [m]
- Polarisation: Stokes I
- For more details see: https://confluence.skatelescope.org/display/SE/MID+pointing+error+simulations
