#!/usr/bin/env bash

#! Set essential environment variables
if [ -z "$SSMRESULTS" ]
then
    SSMRESULTS=${HOME}/data/ska_mid_simulations/results
fi

if [ -z "$SSMROOT" ]
then
    SSMROOT=${HOME}/Code/ska-sim-mid
fi

if [ -z "$SSMRESOURCES" ]
then
    SSMRESOURCES=/alaska/shared/ska-sim-mid/ # only on P3
fi

echo "SSMROOT : $SSMROOT"
echo "SSMRESULTS : $SSMRESULTS"
echo "SSMRESOURCES: $SSMRESOURCES"

vp_directory=${SSMRESOURCES}/beam_models/
screens=${SSMRESOURCES}/screens

for nchan in 8
  do
    for duration in custom
      do
        for dec in -45
          do
            python3 ${SSMROOT}/src/mid_simulation.py --mode ionosphere  --flux_limit 0.1 \
              --declination ${dec} --band B1LOW --pbtype MID_B1LOW --results ${SSMRESULTS} --rmax 1e4 \
              --duration ${duration} --time_range -2.0 2.0    \
              --screen ${screens}/iono_screen_long.fits --height 3e5 \
              --configuration MID --vp_directory ${vp_directory} --nchan ${nchan}
          done
      done
  done

