## Continuum Imaging simulations

The SKA continuum imaging MID simulations are kept on the Google Cloud Platform.

The python command line tool gsutil allows for interacting with the Google Cloud Platform: 

    https://cloud.google.com/storage/docs/gsutil 

After installing gsutil, you may download the simulations as follows: 

    cd continuum_sims_SP-1331
    gsutil rsync gs://ska1-simulation-data/simulations/continuum_simulations_SP-1331 .

and:
    
    cd continuum_sims_SP-11901
    gsutil rsync gs://ska1-simulation-data/simulations/continuum_simulations_SP-1901 .

If you wish to run these or similar simulations, install RASCIL, and use the
shell scripts and slurm files as template.

Make sure you have the below environment variables set up:

    SSMROOT : location where the ska-sim-mid repository is
    SSMRESOURCES : location of the resources e.g. beam models
    SSMRESULTS: location of results folder

Naming convention of provided bash scripts:

- ``make_``: generate simulated Measurement Sets
- ``image_``: run the RASCIL Imager and generate images in FITS format
- ``check_``: run the Imaging QA and generate various quality assessment plots from the images

In addition, scripts to run in slurm are also provided (extension ``.slurm``).
Originally these files were written to be run on [P3](https://confluence.skatelescope.org/display/SE/P3+How+To).

For more information see Confluence:

- [Continuum Imaging Pipeline and Imaging QA](https://confluence.skatelescope.org/display/SE/Continuum+Imaging+Pipeline+and+Imaging+QA>) (and sub-pages)
- [Continuum Imaging Simulations PI12: Methodology](https://confluence.skatelescope.org/display/SE/Continuum+Imaging+Simulations+PI+12%3A+Methodology>)
- [Continuum Imaging Simulations PI12: Results](https://confluence.skatelescope.org/display/SE/Continuum+Imaging+Simulations+PI+12%3A+Results>)

