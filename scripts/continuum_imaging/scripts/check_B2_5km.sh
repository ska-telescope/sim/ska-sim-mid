#!/usr/bin/env bash
#
# Sample script to use QA checker to analyze MID data
# Run this from the directory containing the FITS files
#
python3 ${RASCIL}/rascil/apps/imaging_qa_main.py  \
  --ingest_fitsname_restored SKA_MID_SIM_custom_B2_dec_-45.0_nominal_nchan100_nominal_nmoment3_cip_restored.fits \
  --ingest_fitsname_residual SKA_MID_SIM_custom_B2_dec_-45.0_nominal_nchan100_nominal_nmoment3_cip_residual.fits \
  --input_source_filename SKA_MID_SIM_custom_B2_dec_-45.0_nominal_nchan100_components.hdf \
  --check_source True --plot_source True --perform_diagnostics True

