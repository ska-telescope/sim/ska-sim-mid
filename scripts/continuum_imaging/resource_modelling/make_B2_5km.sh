#!/usr/bin/env bash
#
# Simulate using all dishes within 5km of the MID core. Pass into integration time (s)
# as first command line argument

vp_directory=${SSMRESOURCES}/beam_models/
int_time=$1
  results_dir=${SSMRESULTS}/5km_resource_modelling/int_time${int_time}
  mkdir -p ${results_dir}
  python3 ${SSMROOT}/src/mid_simulation.py --mode nominal --flux_limit 0.0001 \
    --declination -45 --band B2 --pbtype MID_B2 --results ${results_dir}  \
    --integration_time ${int_time} --use_dask True --time_chunk 2880.0 \
    --duration custom --image_pol stokesI --vis_pol linear \
    --vp_directory ${vp_directory} --nchan 100 --rmax 5e3
