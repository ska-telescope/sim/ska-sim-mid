#!/bin/bash
#!
#! Dask job script for CSD3, runs sh make_B2_5km.sh
#! Tim Cornwell
#!
#!#############################################################
#!#### Modify the options in this section as appropriate ######
#!#############################################################

#! sbatch directives begin here ###############################
#! Name of the job:
#SBATCH -J TIME_NPIXEL
#! Which project should be charged:
#SBATCH -A SKA-SDP-SL2-CPU

#! How many nodes should be allocated? If not specified SLURM
#! assumes 1 node.
#SBATCH --nodes=32
#! How many (MPI) tasks will there be in total? (<= nodes*32)
#! The skylake/skylake-himem nodes have 32 CPUs (cores) each.
#SBATCH --ntasks=32
#! For 6GB per CPU, set "-p skylake"; for 12GB per CPU, set "-p skylake-himem":
#SBATCH -p skylake-himem
#! How much memory in MB is required _per node_? Not setting this
#! as here will lead to a default amount per task.
#! Setting a larger amount per task increases the number of CPUs.
#SBATCH --mem=150000
#! How much wallclock time will be required?
#SBATCH --time=10:00:00
#! What types of email messages do you wish to receive?
#SBATCH --mail-type=BEGIN,FAIL,END
#! Where to send email messages
#SBATCH --mail-user=realtimcornwell@gmail.com
#! Uncomment this to prevent the job from being requeued (e.g. if
#! interrupted by node failure or system downtime):
##SBATCH --no-requeue
#! Do not change:
#SBATCH -p skylake
#SBATCH --exclusive
#! Same switch
#SBATCH --switches=1
#! Uncomment this to prevent the job from being requeued (e.g. if
#! interrupted by node failure or system downtime):
##SBATCH --no-requeue

#! Optionally modify the environment seen by the application
#! (note that SLURM reproduces the environment at submission irrespective of ~/.bashrc):
module purge                               # Removes all modules still loaded
module load rhel7/default-peta4            # REQUIRED - loads the basic environment
module load gcc
module load slurm

#! Set up directories (tentative for CSD3, reset according to user)
if [ -z "$SSMRESULTS" ]
then
    export SSMRESULTS=${HOME}/data/ska_mid_simulations/results
fi

if [ -z "$SSMROOT" ]
then
    export SSMROOT=${HOME}/Code/ska-sim-mid
fi

if [ -z "$SSMRESOURCES" ]
then
    export SSMRESOURCES=${HOME}/resources/ska-sim-mid/
fi

echo "SSMROOT : $SSMROOT"
echo "SSMRESULTS : $SSMRESULTS"
echo "SSMRESOURCES: $SSMRESOURCES"

export RASCIL=${HOME}/Code/rascil
export PYTHONPATH=$RASCIL:$PYTHONPATH:$SSMROOT
echo "PYTHONPATH: $PYTHONPATH"

echo -e "Running python: `which python`"
echo -e "Running dask-scheduler: `which dask-scheduler`"

cd $SLURM_SUBMIT_DIR
echo -e "Changed directory to `pwd`.\n"

JOBID=${SLURM_JOB_ID}
echo ${SLURM_JOB_NODELIST}

#! Create a hostfile:
scontrol show hostnames $SLURM_JOB_NODELIST | uniq > hostfile.$JOBID
scheduler=$(head -1 hostfile.$JOBID)

echo "run dask-scheduler"
ssh $scheduler dask-scheduler --port=8786 &
sleep 5

for host in `cat hostfile.$JOBID`; do
    echo "Working on $host ...."
    echo "run dask-worker"
    ssh $host dask-worker --host ${host} --nprocs 1 --nthreads 4 \
    --local-directory /tmp \
    --memory-limit 150GiB $scheduler:8786 &
    sleep 1
done
echo "Scheduler and workers now running"
echo "Scheduler is running at ${scheduler}:8786"

CMD="RASCIL_DASK_SCHEDULER=${scheduler}:8786 sh make_B2_5km.sh  $1"
echo "About to execute $CMD"

eval $CMD
