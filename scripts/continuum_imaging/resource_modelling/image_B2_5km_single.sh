#!/usr/bin/env bash
# Run a single test case, file locations for P3 or CSD3
#
echo `pwd`

int_time=$1
npixel=$2
mshome=${SSMRESULTS}/5km_resource_modelling/int_time${int_time}
results_dir=${SSMRESULTS}/5km_resource_modelling/int_time${int_time}_npixel${npixel}
mkdir -p ${results_dir}
cp -r ${mshome}/SKA_MID_SIM_custom_B2_dec_-45.0_nominal_nchan100.ms ${results_dir}
python3 ${RASCIL}/rascil/apps/rascil_imager.py --mode cip \
  --clean_nmoment 3 --clean_facets 4 --clean_nmajor 10 \
  --clean_threshold 3e-5 --clean_restore_facets 2 --clean_restore_overlap 32 \
  --use_dask True \
  --imaging_context ng --imaging_npixel ${npixel} --imaging_pol stokesI \
  --clean_restored_output list \
  --imaging_cellsize 5e-6 --imaging_weighting uniform --imaging_nchan 1 \
  --ingest_vis_nchan 100 --ingest_chan_per_blockvis 6 \
  --ingest_msname ${results_dir}/SKA_MID_SIM_custom_B2_dec_-45.0_nominal_nchan100.ms \
  --performance_file ${results_dir}/performance_rascil_imager_${int_time}_${npixel}.json
