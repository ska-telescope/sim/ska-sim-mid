#!/bin/sh
# Create and submit SLURM files to make a sweep of int_time, npixel space

for int_time in 2880 1440 720 360 180
  do
    for npixel in 512 1024 2048 4096 8192 16384
      do
        sbatch image_B2_5km_csd3_single.slurm ${int_time} ${npixel}
      done
  done


