#!/usr/bin/env bash
# Upload images to GCP

set -x
mkdir -p low_continuum_sims_SP-1331_SIM-930
lowdata=${HOME}/data/ska-sim-low/continuum_imaging/sp-1331/nmoment3_no_spc_edged_psf_support256_fractional0.1
cp -r ${lowdata} low_continuum_sims_SP-1331_SIM-930
gsutil rsync -r low_continuum_sims_SP-1331_SIM-930/ gs://ska1-simulation-data/ska1-low/continuum_sims_SP-1331_SIM-930

mkdir -p mid_continuum_sims_SP-1331_SIM-930
middata=${HOME}/data/ska-sim-mid/continuum_imaging/sp-1331/nmoment3_no_spc_edged_psf_support256
cp -r ${middata} mid_continuum_sims_SP-1331_SIM-930
cp README.md mid_continuum_sims_SP-1331_SIM-930

gsutil rsync -r mid_continuum_sims_SP-1331_SIM-930/ gs://ska1-simulation-data/ska1-mid/continuum_sims_SP-1331_SIM-930

gsutil -m acl -r ch -g AllUsers:R gs://ska1-simulation-data/ska1-low/continuum_sims_SP-1331_SIM-930/*.log
gsutil -m acl -r ch -g AllUsers:R gs://ska1-simulation-data/ska1-low/continuum_sims_SP-1331_SIM-930/*.fits
gsutil -m acl -r ch -g AllUsers:R gs://ska1-simulation-data/ska1-low/continuum_sims_SP-1331_SIM-930/*.sh
gsutil -m acl -r ch -g AllUsers:R gs://ska1-simulation-data/ska1-low/continuum_sims_SP-1331_SIM-930/*.slurm

gsutil -m acl -r ch -g AllUsers:R gs://ska1-simulation-data/ska1-mid/continuum_sims_SP-1331_SIM-930/*.fits
gsutil -m acl -r ch -g AllUsers:R gs://ska1-simulation-data/ska1-mid/continuum_sims_SP-1331_SIM-930/*.ms
gsutil -m acl -r ch -g AllUsers:R gs://ska1-simulation-data/ska1-mid/continuum_sims_SP-1331_SIM-930/*.sh
gsutil -m acl -r ch -g AllUsers:R gs://ska1-simulation-data/ska1-mid/continuum_sims_SP-1331_SIM-930/*.slurm
