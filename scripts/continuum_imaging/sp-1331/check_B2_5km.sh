#!/usr/bin/env bash
#
# Define SSMROOT and SSMRESULTS externally to override the defaults below
#

if [ -z "$SSMROOT" ]
then
    SSMROOT=${HOME}/Code/ska-sim-mid
fi
echo "SSMROOT : $SSMROOT"
export PYTHONPATH=$SSMROOT:$PYTHONPATH

if [ -z "$SSMRESULTS" ]
then
    SSMRESULTS=${HOME}/data/ska-sim-mid/continuum_imaging/sp-1331/
fi
echo "SSMRESULTS : $SSMRESULTS"

# Change this for each experiment
tag=nmoment3_no_spc_edged_psf_support256

results_dir=${SSMRESULTS}/${tag}
mkdir -p ${results_dir}
cp -r ${SSMRESULTS}/SKA_MID_SIM_custom_B2_dec_-45.0_nominal_nchan100.ms ${results_dir}/${tag}.ms

ls -lt ${results_dir}
cp $0 ${results_dir}
cd ${results_dir} || exit
python3 ${RASCIL}/rascil/apps/imaging_qa_main.py  \
  --ingest_fitsname_restored \
  ${results_dir}/${tag}_nmoment3_cip.taylor.0.restored.fits \
  --ingest_fitsname_residual \
  ${results_dir}/${tag}_nmoment3_cip.taylor.0.residual.fits \
  --input_source_filename \
  ${results_dir}/../SKA_MID_SIM_custom_B2_dec_-45.0_nominal_nchan100_pbcomponents.hdf \
  --telescope_model MID --check_source True --use_frequency_moment True --plot_source True \
  --perform_diagnostics True
