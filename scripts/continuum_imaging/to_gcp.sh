#!/usr/bin/env bash
# Upload images to GCP

set -x
mkdir -p low_continuum_sims_SP-1331

#Here the results should be pointed to the LOW results folder
export SSMRESULTS=${HOME}/data/ska_low_simulations/results

cp ${SSMRESULTS}/12288/*_beams_nmoment2_*.fits low_continuum_sims_SP-1331
cp ${SSMRESULTS}/12288/*_beams_nmoment2.log low_continuum_sims_SP-1331
cp ${SSMRESULTS}/10240/*_beams_nmoment3_*.fits low_continuum_sims_SP-1331
cp ${SSMRESULTS}/10240/*_beams_nmoment3.log low_continuum_sims_SP-1331
cp scripts/image_low_p3.slurm scripts/image_low.sh low_continuum_sims_SP-1331
gsutil rsync -r low_continuum_sims_SP-1331 gs://ska1-simulation-data/ska1-low/continuum_sims_SP-1331

mkdir -p mid_continuum_sims_SP-1331
export SSMRESULTS=${HOME}/data/ska_mid_simulations/results

cp ${SSMRESULTS}/5km/8192/SKA_MID_SIM_custom_B2_dec_-45.0_nominal_nchan100_nominal_nmoment2_*.fits \
mid_continuum_sims_SP-1331
cp ${SSMRESULTS}/5km/8192/SKA_MID_SIM_custom_B2_dec_-45.0_nominal_nchan100_nominal_nmoment2.log \
mid_continuum_sims_SP-1331
cp ${SSMRESULTS}/5km/10240/SKA_MID_SIM_custom_B2_dec_-45.0_nominal_nchan100_nominal_nmoment3_*.fits \
mid_continuum_sims_SP-1331
cp ${SSMRESULTS}/5km/10240/SKA_MID_SIM_custom_B2_dec_-45.0_nominal_nchan100_nominal_nmoment3.log \
mid_continuum_sims_SP-1331
cp -r ${SSMRESULTS}/5km/SKA_MID_SIM_custom_B2_dec_-45.0_nominal_nchan100_nominal.ms \
mid_continuum_sims_SP-1331/SKA_MID_SIM_custom_B2_dec_-45.0_nominal_nchan100_nominal_5km.ms
cp -r ${SSMRESULTS}/full_array/SKA_MID_SIM_custom_B2_dec_-45.0_nominal_nchan100_nominal.ms \
mid_continuum_sims_SP-1331/SKA_MID_SIM_custom_B2_dec_-45.0_nominal_nchan100_nominal_full_array.ms
cp scripts/image_B2_5km_p3.slurm scripts/image_B2_5km.sh mid_continuum_sims_SP-1331
cp README.md mid_continuum_sims_SP-1331

gsutil rsync -r mid_continuum_sims_SP-1331 gs://ska1-simulation-data/ska1-mid/continuum_sims_SP-1331

gsutil -m acl -r ch -g AllUsers:R gs://ska1-simulation-data/ska1-low/continuum_sims_SP-1331/*.log
gsutil -m acl -r ch -g AllUsers:R gs://ska1-simulation-data/ska1-low/continuum_sims_SP-1331/*.fits
gsutil -m acl -r ch -g AllUsers:R gs://ska1-simulation-data/ska1-low/continuum_sims_SP-1331/*.sh
gsutil -m acl -r ch -g AllUsers:R gs://ska1-simulation-data/ska1-low/continuum_sims_SP-1331/*.slurm

gsutil -m acl -r ch -g AllUsers:R gs://ska1-simulation-data/ska1-mid/continuum_sims_SP-1331/*.fits
gsutil -m acl -r ch -g AllUsers:R gs://ska1-simulation-data/ska1-mid/continuum_sims_SP-1331/*.ms
gsutil -m acl -r ch -g AllUsers:R gs://ska1-simulation-data/ska1-mid/continuum_sims_SP-1331/*.sh
gsutil -m acl -r ch -g AllUsers:R gs://ska1-simulation-data/ska1-mid/continuum_sims_SP-1331/*.slurm
