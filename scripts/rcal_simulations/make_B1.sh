#!/usr/bin/env bash
if [ -z "$SSMROOT" ]
then
    SSMROOT=${HOME}/Code/ska-sim-mid
fi
echo "SSMROOT : $SSMROOT"
export PYTHONPATH=$SSMROOT:$PYTHONPATH

if [ -z "$SSMRESULTS" ]
then
    SSMRESULTS=${HOME}/Code/ska-sim-mid/scripts/rcal_simulations/
fi
echo "SSMRESULTS : $SSMRESULTS"

if [ -z "$SSMRESOURCES" ]
then
    SSMRESOURCES=${HOME}/Code/ska-sim-mid/resources
fi
echo "SSMRESOURCES : $SSMRESOURCES"

source=point # Change here for other source models: "double" or "s3sky"
results_dir=${SSMRESULTS}/${source}
mkdir -p ${results_dir}

vp_directory=${SSMRESOURCES}/beam_models
screens=${SSMRESOURCES}/screens

rm -r ${results_dir}/rcal.ms
rm -r ${results_dir}/SKA_MID-AA0.5_SIM_custom_B1LOW_dec_-45.0_ionosphere_nchan*_{actual,nominal,difference}.ms
rm -r ${results_dir}/SKA_MID-AA0.5_SIM_custom_B1LOW_dec_-45.0_ionosphere_nchan*_{actual,nominal,difference}_*.hdf

python3 ${SSMROOT}/src/mid_simulation.py --mode ionosphere  --flux_limit 0.1 \
  --declination -45 --band B1LOW --pbtype MID_B1 --results ${results_dir} \
  --image_sampling 6.0 --source ${source}  --time_chunk 900.0\
  --duration custom --time_range -1 1 --integration_time 60.0  \
  --screen ${screens}/iono_screen_long.fits --height 300000 \
  --configuration MID-AA0.5 --vp_directory ${vp_directory} --nchan 200 \
  --make_images False --write_gt True --apply_pb True

mv ${results_dir}/SKA_MID-AA0.5_SIM_custom_B1LOW_dec_-45.0_ionosphere_nchan*_actual.ms ${results_dir}/rcal_${source}.ms
mv ${results_dir}/SKA_MID-AA0.5_SIM_custom_B1LOW_dec_-45.0_ionosphere_nchan*_pbcomponents.hdf rcal_${source}_pbcomponents.hdf
rm -r ${results_dir}/SKA_MID-AA0.5_SIM_custom_B1LOW_dec_-45.0_ionosphere_nchan*_{nominal,difference}.ms
rm -r ${results_dir}/SKA_MID-AA0.5_SIM_custom_B1LOW_dec_-45.0_ionosphere_nchan*_{actual,nominal,difference}_*.hdf
